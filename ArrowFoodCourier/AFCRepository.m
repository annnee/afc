

#import "AFCRepository.h"

#define kRequestStringUser @"/user"
#define kRequestStringUserPassword @"/user/password"
#define kRequestStringUserPasswordReset @"/user/password/reset"
#define kRequestStringLogin @"/login"
#define kRequestStringLogout @"/logout"
#define kRequestStringMenus @"/menus"
#define kRequestStringCart @"/cart"
#define kRequestStringGeoTag @"/geotag"
#define kRequestStringOrders @"/orders"
#define kRequestStringCartOrder @"/cart/order"
#define kRequestStringCartPrice @"/cart/price"
#define kRequestStringRestaurants @"/restaurants"

@implementation AFCRepository

#pragma mark - User Account

+ (void)createAccountWithUsername:(NSString *)username password:(NSString *)password
                            email:(NSString *)email
                             name:(NSString *)name
                        addresses:(NSArray *)addresses
                           phones:(NSArray *)phones
                  responseHandler:(void (^)(NSString *))callback
{
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    username?   [body setObject:username forKey:@"username"]: nil;
    password?   [body setObject:password forKey:@"password"] : nil;
    email?      [body setObject:email forKey:@"email"] : nil;
    name?       [body setObject:name forKey:@"name"]: nil;
    addresses?  [body setObject:addresses forKey:@"addresses"] : nil;
    phones?     [body setObject:phones forKey:@"phones"]: nil;
    
    [RestfulAPI makeRequest:RequestTypePOST withService:kRequestStringUser body:body andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:@"Could not create account, please try again later", @"default", nil];
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        
        callback(errorString);
    }];
}

+ (void)loginWithUsername:(NSString *)username password:(NSString *)password responseHandler:(void (^)(NSString *))callback
{
    NSDictionary *body = [NSDictionary dictionaryWithObjectsAndKeys:username, @"username", password, @"password", nil];
    
    [RestfulAPI makeRequest:RequestTypePOST withService:kRequestStringLogin body:body andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not login, please try again later", @"default",
                           @"Invalid username or password", @"404", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        
        callback(errorString);
    }];
}

+ (void)logoutWithResponseHandler:(void (^)(NSString *))callback
{
    [RestfulAPI makeRequest:RequestTypePOST withService:kRequestStringLogout body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Error", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString);
    }];
}

+(void)getProfileWithResponseHandler:(void (^)(NSString *, NSDictionary *))callback
{
    [RestfulAPI makeRequest:RequestTypeGET withService:kRequestStringUser body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not get profile information, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString, response);
    }];
}

+(void)changePasswordWithOldPassword:(NSString *)old newPassword:(NSString *)newPass responseHandler:(void (^)(NSString *))callback
{
    NSDictionary *body = [NSDictionary dictionaryWithObjectsAndKeys:old, @"oldPassword", newPass, @"password", nil];
    
    [RestfulAPI makeRequest:RequestTypePOST withService:kRequestStringUserPassword body:body andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
     {
         NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"Could not change password, please try again later", @"default", nil];
         
         NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
         callback(errorString);
     }];
}

+(void)sendResetPasswordEmailWithUsername:(NSString*)username responseHandler:(void (^)(NSString *))callback
{
    NSString *appendedService = [NSString stringWithFormat:@"%@/%@/reset", kRequestStringUser, username];
    
    [RestfulAPI makeRequest:RequestTypePOST withService:appendedService body:nil andHandler:^(int statusCode,NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not send reset email, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString);
    }];
}

+(void)resetPasswordWithToken:(NSString *)token newPassword:(NSString *)password responseHandler:(void (^)(NSString *))callback
{
    NSString *appendedService = [NSString stringWithFormat:@"%@/%@", kRequestStringUserPasswordReset, token];
    NSDictionary *body = [NSDictionary dictionaryWithObjectsAndKeys:password, @"password", nil];
    
    [RestfulAPI makeRequest:RequestTypePOST withService:appendedService body:body andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not reset password, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString);
    }];
}

+(void)getOrderHistoryWithResponseHandler:(void (^)(NSString *, NSArray *))callback
{
    [RestfulAPI makeRequest:RequestTypeGET withService:kRequestStringOrders body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not get order history, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString, responseArray);
    }];
}

#pragma mark - Menu

+(void)getRestaurantsWithResponseHandler:(void (^)(NSString *, NSArray *))callback
{
    [RestfulAPI makeRequest:RequestTypeGET withService:kRequestStringRestaurants body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not get restaurant information, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString, responseArray);
    }];
}

+ (void)getMenusWithResponseHandler:(void (^)(NSString *, NSArray *))callback
{
    [RestfulAPI makeRequest:RequestTypeGET withService:kRequestStringMenus body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not get menu information, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString, responseArray);
    }];
}

#pragma mark - Cart

+ (void)getCartWithResponseHandler:(void (^)(NSString *, NSDictionary *))callback
{
    [RestfulAPI makeRequest:RequestTypeGET withService:kRequestStringCart body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
     {
         NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"Could not get cart, please try again later", @"default", nil];
         
         NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
         callback(errorString, response);
     }];
}

+ (void)addItemToCartWithRestaurant:(NSString *)restaurant menuCategory:(NSString *)menuCategory item:(NSString *)item options:(NSArray *)options
                           quantity:(int)quantity responseHandler:(void (^)(NSString *))callback
{
    NSString *appendedService = [NSString stringWithFormat:@"%@/%@/%@/%@/%d", kRequestStringCart, restaurant, menuCategory, item, quantity];
    
    [RestfulAPI makeRequest:RequestTypePOST withService:appendedService body:options andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not add item to cart, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString);
    }];
}

+ (void)deleteItemFromCartWithRestaurant:(NSString *)restaurant menu:(NSString *)menu item:(NSString *)item quantity:(int)quantity responseHandler:(void (^)(NSString *))callback
{
    NSString *appendedService = [NSString stringWithFormat:@"%@/%@/%@/%@/%d", kRequestStringCart, restaurant, menu, item, quantity];
    
    [RestfulAPI makeRequest:RequestTypeDELETE withService:appendedService body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not delete item from cart, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString);
    }];
}

+(void)makeOrderWithDeliveryAddress:(NSDictionary *)shipping billingAddress:(NSDictionary *)billing responseHandler:(void (^)(NSString *))callback
{
    NSDictionary *body = [NSDictionary dictionaryWithObjectsAndKeys:shipping, @"shipping", billing, @"billing", nil];
    
    [RestfulAPI makeRequest:RequestTypePOST withService:kRequestStringCartOrder body:body andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not create your order, please try again later", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString);
    }];
}

+(void)getTotalCostWithResponseHandler:(void (^)(NSString *, NSDictionary *))callback
{
    [RestfulAPI makeRequest:RequestTypePOST withService:kRequestStringCartPrice body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
     {
         NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"Could not get cart price, please try again later", @"default", nil];
         
         NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
         callback(errorString, response);
     }];
}

#pragma mark - Location

+(void)updateLocationWithCoordinate:(CLLocationCoordinate2D)location responseHandler:(void (^)(NSString *))callback
{
    NSString *appendedService = [NSString stringWithFormat:@"%@/%f/%f", kRequestStringGeoTag,  location.latitude, location.longitude];
    
    [RestfulAPI makeRequest:RequestTypePOST withService:appendedService body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
    {
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Could not update location", @"default", nil];
        
        NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
        callback(errorString);
    }];
    
}

+(void)getDriverLocationWithResponseHandler:(void (^)(NSString *, NSDictionary *))callback
{
    NSString *appendedService = [NSString stringWithFormat:@"%@/%@", kRequestStringGeoTag,  [[[AFCListing sharedStore] user] username]];
 
    [RestfulAPI makeRequest:RequestTypeGET withService:appendedService body:nil andHandler:^(int statusCode, NSDictionary *response, NSArray *responseArray, NSError *error)
     {
         NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"Could not get driver location, please try again later", @"default",
                            @"Currently not tracking any orders", @"404", nil];
         
         NSString *errorString = [self errorString:error response:response statusCode:statusCode withMessagesForStatusCodes:d];
         callback(errorString, response);
     }];
}

#pragma mark - Parse Error

+ (NSString*)errorString:(NSError*)error response:(NSDictionary*)response statusCode:(int)statusCode withMessagesForStatusCodes:(NSDictionary*)messages
{
    NSString *errorString;
    NSString *statusCodeString = [NSString stringWithFormat:@"%d", statusCode];
    
    if(error)
    {
        errorString = [error localizedDescription];
    }
    else if([messages objectForKey:statusCodeString])
    {
        errorString = [messages objectForKey:statusCodeString];
    }
    else if([response respondsToSelector:@selector(objectForKey:)] && [response objectForKey:@"error"])
    {
        errorString = [response objectForKey:@"error"];
    }
    else if([response respondsToSelector:@selector(objectForKey:)] && [response objectForKey:@"success"])
    {
        errorString = nil;
    }
    else if(statusCode >= 400 && [messages objectForKey:@"default"])
    {
        errorString = [messages objectForKey:@"default"];
    }
    
    // Safety check in case the response object is returning a boolean, this
    // will prevent the app from crashing
    if(((BOOL) errorString) == false) errorString = nil;
    
    return errorString;
}


@end
