
#import "AFCView.h"

@interface CartViewFooter : AFCView

- (id)initWithoutCheckoutWithFrame:(CGRect)frame;

- (void)update;

@end
