

#import "AFCCart.h"
typedef enum
{
    SENT,
    RECEIVED,
    COOKING,
    EN_ROUTE,
    DELIVERED
} Status;

@interface AFCOrder : AFCCart

@property (nonatomic) Status orderStatus;

- (NSArray *)orderItems;
- (float)orderTotal;
- (NSString *)orderStatusString;

@end
