

#import <Foundation/Foundation.h>
#import "RestfulAPI.h"
#import <MapKit/MapKit.h>

@interface AFCRepository : NSObject

#pragma mark - Account


+ (void)createAccountWithUsername:(NSString*)username
                         password:(NSString*)password
                            email:(NSString*)email
                             name:(NSString*)name
                        addresses:(NSArray*)addresses
                            phones:(NSArray*)phones
                  responseHandler:(void (^)(NSString *error)) callback;

+ (void)loginWithUsername:(NSString*)username
                 password:(NSString*)password
          responseHandler:(void (^)(NSString *error)) callback;


+ (void)logoutWithResponseHandler:(void (^)(NSString *error)) callback;

+ (void)getProfileWithResponseHandler:(void (^)(NSString *error, NSDictionary *info)) callback;

+ (void)changePasswordWithOldPassword:(NSString*)old newPassword:(NSString*)newPass responseHandler:(void (^)(NSString *error)) callback;

+ (void)sendResetPasswordEmailWithUsername:(NSString*)username responseHandler:(void (^)(NSString *error))callback;

+ (void)resetPasswordWithToken:(NSString*)token newPassword:(NSString*)password responseHandler:(void (^)(NSString *error))callback;

+ (void)makeOrderWithDeliveryAddress:(NSDictionary*)shipping billingAddress:(NSDictionary*)billing responseHandler:(void (^)(NSString *error)) callback;

+ (void)getOrderHistoryWithResponseHandler:(void (^)(NSString *error, NSArray *info)) callback;

#pragma mark - Menus/Restaurants/Menu Items

+ (void)getRestaurantsWithResponseHandler:(void (^)(NSString *error, NSArray *info)) callback;

+ (void)getMenusWithResponseHandler:(void (^)(NSString *error, NSArray *info)) callback;

#pragma mark - Cart

+ (void)getCartWithResponseHandler:(void (^)(NSString *error, NSDictionary *info)) callback;

+ (void)addItemToCartWithRestaurant:(NSString*)restaraunt
                        menuCategory:(NSString*)menuCategory
                                item:(NSString*)item
                             options:(NSArray*)options
                            quantity:(int)quantity
                     responseHandler:(void (^)(NSString *error)) callback;


+ (void)deleteItemFromCartWithRestaurant:(NSString*)restaurant
                                    menu:(NSString*)menu
                                    item:(NSString*)item
                                quantity:(int)quantity
                         responseHandler:(void (^)(NSString *error)) callback;

+ (void)getTotalCostWithResponseHandler:(void (^)(NSString *error, NSDictionary *info)) callback;

#pragma mark - Tracking

+ (void)updateLocationWithCoordinate:(CLLocationCoordinate2D)location responseHandler:(void (^)(NSString *error)) callback;

+ (void)getDriverLocationWithResponseHandler:(void (^)(NSString *error, NSDictionary *info)) callback;

@end
