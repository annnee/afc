
#import <UIKit/UIKit.h>

// List of colors and their hex values for the apps common colors used.
enum
{
    AFCRed = 0xE0504B,      // The red provided in the design document.
    AFCGray = 0xA2A4A6,     // The gray provided in the design document.
    DarkRed = 0xB5504B,     // The red for the separators among other things.
    DrawerRed = 0xC5504B    // The red in the left drawer.
} Color;

@interface UIColor (ColorUtil)

// Creates a UIColor object with the given hex color value.
+ (UIColor*)colorFromHexValue:(UInt32)hexValue;

@end

@implementation UIColor (ColorUtil)

+ (UIColor*)colorFromHexValue:(UInt32)hexValue
{
    return [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0
                           green:((float)((hexValue & 0xFF00) >> 8))/255.0
                            blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
}

@end
