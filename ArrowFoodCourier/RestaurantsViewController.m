 

#import "RestaurantsViewController.h"
#import "RestaurantsView.h"
#import "MenuCategoriesViewController.h"
#import "AFCRestaurantsTableViewCell.h"
#import "AFCMenuCategory.h"
#import "AFCRestaurant.h"
#import "AFCListing.h"
#import "AFCUser.h"

@interface RestaurantsViewController ()

@end

@implementation RestaurantsViewController
{
    //Private variables
    
    RestaurantsSecondaryNavBar *_bar2;
    
    UITableView *_tableView;
    NSArray *_tableData;
    
    UIView *_activeView;
}

#pragma mark - View Controller Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self addTableData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View Setup

- (void)setupView
{
    // Reset the selected restaurant to nil
    [AFCListing sharedStore].selectedRestaurant = nil;
    
    [[self mainNavigationController] setNavigationBarHidden:NO animated:YES];
    
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] setDrawerGesturesEnabled:YES];
    
    // Set the view to HomeView
    self.view = [[RestaurantsView alloc] initWithFrame:self.view.frame];
    
    // Define the navigation title for the AFCNavigationController so that it will
    // be displayed in the top bar.
    self.navigationItem.title = @"RESTAURANTS";
    
    // Add icon items to the top bar
    [self addHomeIcon];
    [self addCartButtonIcon];
    
    // Add table view controller
    [self createTableView];
    
    // Hide the nav bar's drop shadow because of the secondary nav bar
    //[[self navigationController] setDropShadowHidden:YES];
    
    // Create the secondary navigation bar
    //_bar2 = [[RestaurantsSecondaryNavBar alloc] initWithPosition:CGPointZero];
    //[self.view addSubview:_bar2];
    //_bar2.delegate = self;
    
    // Create the views that slide down when a button is pressed in
    // secondary nav bar
    //[self addDropDownViews];
    //[self.view bringSubviewToFront:_bar2];
    
}

- (void)createTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    // Push the table view to be below the secondary nav bar
    [self.view sendSubviewToBack:_tableView];
    
    // Remove the extra whitespace at the left of the tableview
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)addDropDownViews
{
    [self.view addSubview:_bar2.filterView];
    [self.view addSubview:_bar2.sortView];
    [self.view addSubview:_bar2.searchView];
    
    // Add extra view on top of these to hide them from being visible underneath the status bar (iOS 7).
    UIView *coverView = [[UIView alloc] initWithFrame:CGRectMake(0, -100, self.view.frame.size.width, 100)];
    coverView.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [self.view addSubview:coverView];
}

#pragma mark - Table Delegate & Datasource

// To add a restaurant, create an AFCRestaurant object and add it to the AFCRestaurantListing sharedStore object
// this will make sure that each table cell has an associated AFCRestaurant object which it will use to create a menu view.
- (void)addTableData
{
    _tableData = [[AFCListing sharedStore] allRestaurants];
    
    // If restaurant data is empty, we need to refresh it
    if ([[[AFCListing sharedStore] allRestaurants] count] == 0)
    {
        [self startLoading];

        // Add all restaurants
        [AFCRepository getRestaurantsWithResponseHandler:^(NSString *error, NSArray *info)
        {
            if(error)
            {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Restaurants Error" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [av show];
            }
            else
            {
                for (NSDictionary *dict in info)
                {
                    [[AFCListing sharedStore] addRestaurant:[[AFCRestaurant alloc] initWithDictionary:dict]];
                }
           
                // Iterate through each restaurant and add whatever menu categories belong to it
                [AFCRepository getMenusWithResponseHandler:^(NSString *error, NSArray *info)
                {
                    if(error)
                    {
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Restaurants Error" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [av show];
                    }
                    {
                        for (AFCRestaurant *restaurant in [[AFCListing sharedStore] allRestaurants])
                        {
                            for (NSDictionary *category in info) {
                                if ([(NSString *)[category objectForKey:@"restaurant"] isEqualToString:[restaurant name]])
                                {
                                    [restaurant addMenuCategory:[[AFCMenuCategory alloc] initWithDictionary:category]];
                                }
                            }
                        }
                        _tableData = [[AFCListing sharedStore] allRestaurants];
                        if(_tableView)[_tableView reloadData];
                    }
                    [self stopLoading];
                }];
            }
            
            
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"tableItem";
    
    AFCRestaurantsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[AFCRestaurantsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.cellRestaurant = [_tableData objectAtIndex:indexPath.row];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[self mainNavigationController] pushViewController:[[MenuCategoriesViewController alloc] initWithRestaurant:[_tableData objectAtIndex:indexPath.row]] animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Secondary Navigation Delegates

- (void)filterButtonPressed:(UIButton*)sender
{
    if(_activeView)
    {
        [self hideDropDown:_activeView];
    }
    
    if(sender.selected)
    {
        _activeView = _bar2.filterView;
        _bar2.filterView.userInteractionEnabled = YES;
        _bar2.sortView.userInteractionEnabled = NO;
        _bar2.searchView.userInteractionEnabled = NO;
        [self showDropDown:_activeView];
    }
    else
    {
        _activeView = nil;
    }
    
}

-(void)sortButtonPressed:(UIButton*)sender
{
    if(_activeView)
    {
        [self hideDropDown:_activeView];
    }
    
    if(sender.selected)
    {
        _activeView = _bar2.sortView;
        _bar2.sortView.userInteractionEnabled = YES;
        _bar2.filterView.userInteractionEnabled = NO;
        _bar2.searchView.userInteractionEnabled = NO;
        [self showDropDown:_activeView];
    }
    else
    {
        _activeView = nil;
    }
}

-(void)searchButtonPressed:(UIButton*)sender
{
    if(_activeView)
    {
        [self hideDropDown:_activeView];
    }
    
    if(sender.selected)
    {
        _activeView = _bar2.searchView;
        _bar2.searchView.userInteractionEnabled = YES;
        _bar2.sortView.userInteractionEnabled = NO;
        _bar2.filterView.userInteractionEnabled = NO;
        [self showDropDown:_activeView];
        [_bar2 makeSearchBarFirstResponder];
    }
    else
    {
        [_bar2 resignSearchBarResponder];
        _activeView = nil;
    }
}

-(void)filterOptionSelected:(NSString *)filter
{

}

-(void)sortOptionSelected:(NSString *)sort
{

}

-(void)searchTextChanged:(NSString *)search
{
    
}

#pragma mark - Animations

- (void)hideDropDown:(UIView*)view
{
    // Disable user interaction
    _bar2.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:0.2 animations:^{
        [view setFrame:CGRectMake(view.frame.origin.x,
                                         -view.frame.size.height+_bar2.frame.size.height,
                                         view.frame.size.width,
                                         view.frame.size.height)];
        [_tableView setFrame:CGRectMake(_tableView.frame.origin.x,
                                        _bar2.frame.size.height,
                                        _tableView.frame.size.width,
                                        _tableView.frame.size.height)];
    } completion:^(BOOL finished) {
        // Enable user interaction
        _bar2.userInteractionEnabled = YES;
        
    }];
}

- (void)showDropDown:(UIView*)view
{
    // Disable user interation
    _bar2.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [view setFrame:CGRectMake(view.frame.origin.x,
                                         _bar2.frame.size.height,
                                         view.frame.size.width,
                                         view.frame.size.height)];
        [_tableView setFrame:CGRectMake(_tableView.frame.origin.x,
                                        view.frame.size.height+_bar2.frame.size.height,
                                        _tableView.frame.size.width,
                                        _tableView.frame.size.height)];
        
    } completion:^(BOOL finished) {
        // Enable user interaction
        _bar2.userInteractionEnabled = YES;
        
    }];
}


@end
