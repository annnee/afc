

#import <Foundation/Foundation.h>
#import "AFCMenuItem.h"

@interface AFCMenuCategory : NSObject
{
    NSMutableArray *menuItems;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;

- (id)initWithName:(NSString *)itemName description:(NSString *)itemDescription;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (void)addMenuItem:(AFCMenuItem *)item;
- (NSArray *)allMenuItems;

@end
