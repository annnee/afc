

#import "AFCMenuCategory.h"

@implementation AFCMenuCategory

@synthesize name = _name;
@synthesize description = _description;

- (id)initWithName:(NSString *)categoryName description:(NSString *)categoryDescription
{
    self = [super init];
    if (self)
    {
        _name = categoryName;
        _description = categoryDescription;
        
        menuItems = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        menuItems = [[NSMutableArray alloc] init];
        _name = [dictionary objectForKey:@"name"];
        _description = [(NSArray *)[dictionary objectForKey:@"tags"] componentsJoinedByString:@", "];
        
        // Add menu items
        NSArray *items = [dictionary objectForKey:@"items"];
        for (NSDictionary *item in items) {
            [self addMenuItem:[[AFCMenuItem alloc] initWithDictionary:item]];
        }
    }
    return self;
}

- (void)addMenuItem:(AFCMenuItem *)item
{
    [menuItems addObject:item];
}

- (NSArray *)allMenuItems
{
    return menuItems;
}

@end
