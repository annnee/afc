 

#import "AFCViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AFCOrder.h"

@interface TrackingViewController : AFCViewController<MKMapViewDelegate, CLLocationManagerDelegate>

- (id)initWithOrder:(AFCOrder *)order;

@end
