
#import "AFCAddress.h"

@implementation AFCAddress

@synthesize line1 = _line1;
@synthesize line2 = _line2;
@synthesize city = _city;
@synthesize state = _state;
@synthesize zip = _zip;
@synthesize defaultBilling = _defaultBilling;
@synthesize defaultShipping = _defaultShipping;

-(id)initWithLine1:(NSString *)line1
               line2:(NSString *)line2
                city:(NSString *)city
               state:(NSString *)state
                 zip:(NSString *)zip
      defaultBilling:(BOOL)defaultBilling
     defaultShipping:(BOOL)defaultShipping
{
    self = [super init];
    if(self)
    {
        _line1 = line1;
        _line2 = line2;
        _city = city;
        _state = state;
        _zip = zip;
        _defaultShipping = defaultShipping;
        _defaultBilling = defaultBilling;
    }
    return self;
}

-(id)initFromDictionary:(NSDictionary *)d
{
    self = [super init];
    if(self)
    {
        _line1 = [d objectForKey:@"line1"];
        _line2 = [d objectForKey:@"line2"];
        _city = [d objectForKey:@"city"];
        _state = [d objectForKey:@"state"];
        _zip = [d objectForKey:@"zip"];
        _defaultBilling = [[d objectForKey:@"defaultBilling"] boolValue];
        _defaultShipping = [[d objectForKey:@"defaultShipping"] boolValue];
    }
    return self;
}

-(NSDictionary *)dictionary
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
    if(_line1) [d setObject:_line1 forKey:@"line1"];
    if(_line2) [d setObject:_line2 forKey:@"line2"];
    if(_city) [d setObject:_city forKey:@"city"];
    if(_state) [d setObject:_state forKey:@"state"];
    if(_zip) [d setObject:_zip forKey:@"zip"];
    [d setObject:[NSNumber numberWithBool:_defaultBilling] forKey:@"defaultBilling"];
    [d setObject:[NSNumber numberWithBool:_defaultShipping] forKey:@"defaultShipping"];
    return d;
}

-(NSString *)text
{
    return [NSString stringWithFormat:@"%@ %@, %@, %@", _line1, _line2, _city, _state];
}

@end
