

#import <Foundation/Foundation.h>
#import "AFCAddress.h"
#import "AFCPhone.h"

@interface AFCUser : NSObject

@property (nonatomic) BOOL isDriver;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSArray *addresses;
@property (nonatomic, strong) NSArray *phoneNumbers;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *username;

- (id)initWithFirstName:(NSString *)userFirstName
               lastName:(NSString *)userLastName
                addresses:(NSArray *)userAddresses
            phoneNumbers:(NSArray *)userPhoneNumbers
                  email:(NSString *)userEmail
               username:(NSString *)userUsername
               isDriver:(BOOL)isDriver;

- (id)initFromDictionary:(NSDictionary*)dictionary;

- (NSDictionary*)dictionary;

@end
