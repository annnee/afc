

#import <UIKit/UIKit.h>
#import "AFCView.h"

@interface LogOnView : AFCView

@property (nonatomic, strong) UIScrollView *setUp;
@property (nonatomic, strong) UITextField *username;
@property (nonatomic, strong) UITextField *password;

@end
