

#import <UIKit/UIKit.h>
#import "AFCRestaurant.h"

@interface MenuHeaderView : UIView

@property AFCRestaurant *restaurant;

- (id)initWithPosition:(CGPoint)position restaurant:(AFCRestaurant *)restaurant;

@end
