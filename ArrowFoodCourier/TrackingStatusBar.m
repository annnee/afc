 

#import "TrackingStatusBar.h"
#import <QuartzCore/QuartzCore.h>

@implementation TrackingStatusBar
{
    NSArray *_statuses;
    NSMutableArray *_circles;
    NSMutableArray *_labels;
}

@synthesize  enabled = _enabled;
@synthesize status = _status;

static int labelPadding = 25;
static int lineHeight = 8;
static float circleRatio = 0.7;

- (id)initWithPosition:(CGPoint)point andWidth:(int)width andCircleDiameter:(int)diameter andStatuses:(NSArray *)statuses
{
    self = [super initWithFrame:CGRectMake(point.x, point.y, width, diameter + labelPadding)];
    if(self)
    {
        _statuses = [NSArray arrayWithArray:statuses];
        
        lineHeight = roundf((diameter - roundf(circleRatio*diameter)) / 2.0) + 2;
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, lineHeight)];
        line.backgroundColor = [UIColor blackColor];
        line.layer.cornerRadius = 5;
        [line setCenter:CGPointMake(line.center.x, self.frame.size.height / 2 - labelPadding / 2)];
        [self addSubview:line];
        
        // Circles and labels
        _circles = [[NSMutableArray alloc] init];
        _labels = [[NSMutableArray alloc] init];
        
        float totalCircleWidth = diameter*statuses.count;
        float paddingBetweenCircles = (width - totalCircleWidth) / (statuses.count - 1);
        for(int i = 0; i < statuses.count; i++)
        {
            UIView *circle = [[UIView alloc] initWithFrame:CGRectMake(i*diameter + i*paddingBetweenCircles, 0, diameter, diameter)];
            [circle setCenter:CGPointMake(circle.center.x, line.center.y)];
            circle.backgroundColor = [UIColor blackColor];
            circle.layer.cornerRadius = roundf(circle.frame.size.width / 2);
            circle.frame = CGRectIntegral(circle.frame);
            [self addSubview:circle];
            
            UIView *smallCircle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, roundf(circleRatio*diameter), roundf(circleRatio*diameter))];
            smallCircle.backgroundColor = [UIColor whiteColor];
            smallCircle.layer.cornerRadius = roundf(smallCircle.frame.size.width / 2);
            smallCircle.frame = CGRectIntegral(smallCircle.frame);
            [smallCircle setCenter:circle.center];
            [self addSubview:smallCircle];
            [_circles addObject:smallCircle];
            
            NSString *s = [statuses[i] uppercaseString];
            UIFont *font = [UIFont fontWithName:HEADER_FONT size:13];
            CGSize textSize = [s sizeWithFont:font];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textSize.width, textSize.height)];
            label.text = s;
            label.font = font;
            label.textColor = [UIColor colorFromHexValue:AFCGray];
            [label setCenter:CGPointMake(circle.center.x, self.frame.size.height / 2 + labelPadding / 2)];
            label.backgroundColor = [UIColor clearColor];
            label.frame = CGRectIntegral(label.frame);
            [self addSubview:label];
        
            [_labels addObject:label];
        }
        self.clipsToBounds = NO;
        
    }
    return self;
}

-(void)setEnabled:(BOOL)enabled
{

}

-(void)setStatus:(NSString *)status
{
    int index = -1;
    if(status)
    {
        index = (int)[_statuses indexOfObject:status];
    }
    
    for(int i = 0; i < _circles.count; i++)
    {
        if(!status || i > index)
        {
            [_circles[i] setBackgroundColor:[UIColor whiteColor]];
            [_labels[i] setTextColor:[UIColor colorFromHexValue:AFCGray]];
        }
        else if(i <= index)
        {
            [_circles[i] setBackgroundColor:[UIColor colorFromHexValue:DarkRed]];
            [_labels[i] setTextColor:[UIColor blackColor]];
        }
    }
}

@end
