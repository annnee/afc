
#import "MenuView.h"

@implementation MenuView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        // Background for status bar
        UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, -100, self.frame.size.width, 100)];
        bg.backgroundColor = [UIColor colorFromHexValue:AFCGray];
        [self addSubview:bg];
    }
    return self;
}

@end
