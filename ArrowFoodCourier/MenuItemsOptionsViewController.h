 
#import "AFCViewController.h"
#import "AFCMenuItem.h"

@interface MenuItemsOptionsViewController : AFCViewController<UITableViewDataSource, UITableViewDelegate>
{
    AFCMenuItem *menuItem;
}

- (id)initWithMenuItem:(AFCMenuItem *)item;

@end
