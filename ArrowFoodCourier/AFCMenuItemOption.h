

#import <Foundation/Foundation.h>
typedef enum
{
    CHECK_ONE,
    CHECKBOX,
    FREEFORM
} FormType;

@interface AFCMenuItemOption : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic) FormType type;
@property (nonatomic, strong) NSString *typeString;
@property (nonatomic, strong) NSString *selectedParam;

- (id)initWithName:(NSString *)optionName
       description:(NSString *)optionDescription
              type:(NSString *)optionType
            params:(NSString *)optionParams;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSArray *)allParameters;

-(NSDictionary*)dictionary;

@end
