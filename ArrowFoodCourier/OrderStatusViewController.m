 

#import "OrderStatusViewController.h"
#import "AFCRepository.h"
#import "AFCOrder.h"
#import "OrderStatusTableViewCell.h"
#import "TrackingViewController.h"

@interface OrderStatusViewController ()
{
    UITableView *orderStatusTable;
    NSMutableArray *orderData;
    UIRefreshControl *_refreshControl;
}

@end

@implementation OrderStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        orderData = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupView
{
    // Add buttons and set navigation bar title
    [self addHomeIcon];
    [self addCartButtonIcon];
    
    // Set up table
    CGRect tableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    orderStatusTable = [[UITableView alloc] initWithFrame:tableFrame];
    orderStatusTable.delegate = self;
    orderStatusTable.dataSource = self;
    orderStatusTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [[self view] addSubview:orderStatusTable];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = orderStatusTable;
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(loadTableData) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = _refreshControl;
    [_refreshControl beginRefreshing];
    
    // Remove the extra whitespace at the left of the tableview
    if ([orderStatusTable respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [orderStatusTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    [self loadTableData];
    
    self.navigationItem.title = @"ORDER STATUS";
}

- (void)loadTableData
{
    orderData = [[NSMutableArray alloc] init];
    [AFCRepository getOrderHistoryWithResponseHandler:^(NSString *error, NSArray *info)
     {
         if(error)
         {
             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Order History Error" message:error delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [av show];
         }
         else
         {
             for (NSDictionary *o in info)
             {
                 AFCOrder *order = [[AFCOrder alloc] initFromDictionary:o];
                 if (order) [orderData addObject:order];
             }
             
             [orderStatusTable reloadData];
             [_refreshControl endRefreshing];
         }
     }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"orderStatusCell";
    
    OrderStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"OrderStatusTableViewCell" bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    
    cell.orderTitle.text = [NSString stringWithFormat:@"Number of Items: %i",[[(AFCOrder *)[orderData objectAtIndex:indexPath.row] orderItems] count]];
    cell.orderPrice.text = [NSString stringWithFormat:@"%.2f", [(AFCOrder *)[orderData objectAtIndex:indexPath.row] orderTotal]];
    cell.orderStatus.text = [NSString stringWithFormat:@"Status: %@", [[orderData objectAtIndex:indexPath.row] orderStatusString]];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [orderData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 62;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TrackingViewController *tvc = [[TrackingViewController alloc] initWithOrder:[orderData objectAtIndex:indexPath.row]];
    [[self navigationController] pushViewController:tvc animated:YES];
}

@end
