

#import <Foundation/Foundation.h>
#import "AFCRestaurant.h"
#import "AFCMenuItem.h"
#import "AFCUser.h"
#import "AFCAddress.h"
#import "AFCPhone.h"
#import "AFCMenuCategory.h"
#import "AFCCartItem.h"
#import "AFCCart.h"

@interface AFCListing : NSObject
{
    NSMutableArray *allRestaurants;
}

@property (nonatomic, strong) AFCUser *user;
@property (nonatomic, strong) AFCRestaurant *selectedRestaurant;
@property (nonatomic, strong) AFCMenuCategory *selectedCategory;
@property (nonatomic, strong) AFCCart *cart;

@property BOOL isParsing;

+ (AFCListing *)sharedStore;

- (NSArray *)allRestaurants;
- (void)addRestaurant:(AFCRestaurant *)p;
- (void)removeRestaurant:(AFCRestaurant *)p;

- (void)createCartItemsFromDictionary:info;
- (NSArray*)allCartItems;
- (void)addCartItem:(AFCCartItem*)cartItem;
- (BOOL)removeCartItem:(AFCCartItem *)p;

- (float)totalTaxCost;
- (float)totalItemsWithoutTaxCost;
- (float)totalItemsWithTaxCost;

@end
