

#import "AFCView.h"
#import "AFCMenuItem.h"
#import "AFCMenuCategory.h"

typedef enum
{
    CartViewCellPropertyRestaurant,
    CartViewCellPropertyCategory,
    CartViewCellPropertyMenuItem
}CartViewCellProperty;

@interface CartViewCell : UITableViewCell

@property (nonatomic, strong) NSString *cellRestaurant;
@property (nonatomic, strong) NSString *cellMenuCategory;
@property (nonatomic, strong) AFCMenuItem *cellMenuItem;

- (void)setupView;

- (void)toggleVisibiltyOfDeleteView;
- (void)showDeleteView;
- (void)hideDeleteView;
- (BOOL)locationHitsDeleteView:(CGPoint)point;

@end
