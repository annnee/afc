

#import "AFCCart.h"
#import "AFCCartItem.h"

@implementation AFCCart

@synthesize cartItems = _cartItems;

-(id)initFromDictionary:(NSDictionary *)info
{
    self = [super init];
    if(self)
    {
        _allCartItems = [[NSMutableArray alloc] init];
        NSArray *items = [info objectForKey:@"items"];
        for(NSDictionary *item in items)
        {
            AFCCartItem *i = [[AFCCartItem alloc] initWithDictionary:item];
            [_allCartItems addObject:i];
        }
    }
    return self;
}

-(void)addCartItem:(AFCCartItem *)cartItem
{
    [_allCartItems addObject:cartItem];
}

- (BOOL)removeCartItem:(AFCCartItem *)p
{
    int index = -1;
    for(int i = 0; i < _allCartItems.count; i++)
    {
        AFCCartItem *m = (AFCCartItem*)[_allCartItems objectAtIndex:i];
        if([m.menuItem isEqual:p.menuItem])
        {
            index = i;
            break;
        }
    }
    if(index > -1)
    {
        [_allCartItems removeObjectAtIndex:index];
        return YES;
    }
    return NO;
}

-(NSArray *)cartItems
{
    return _allCartItems;
}

@end
