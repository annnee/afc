 

#import "MenuItemOptionsViewCell.h"

@implementation MenuItemOptionsViewCell
{
    int selectedRow;
    NSArray *tableData;
}

@synthesize cellHeight = _cellHeight;
@synthesize cellMenuItemOption = _cellMenuItemOption;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Set default height to 80 pixels
        _cellHeight = 80;
        
        selectedRow = 0;
        
        // Hide the original content to instead use custom content
        self.textLabel.alpha = 0;
        self.imageView.alpha = 0;
    }
    return self;
}

- (void)layoutView
{
    if (_cellMenuItemOption.type == CHECK_ONE)
    {
        tableData = [_cellMenuItemOption allParameters];
        UILabel *_titleLabel = [[UILabel alloc] init];
        UILabel *_descriptionLabel = [[UILabel alloc] init];
        
        // Adjust table height for number of items in the table
        UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, OPTION_CELL_FOOTER_HEIGHT, self.frame.size.width, [tableData count]*DEFAULT_TABLE_CELL_HEIGHT)];
        table.delegate = self;
        table.dataSource = self;
        table.scrollEnabled = NO;
        
        // Remove the extra whitespace at the left of the tableview
        if ([table respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
        {
            [table setSeparatorInset:UIEdgeInsetsZero];
        }
        
        [self addSubview:table];
        
        // Create the label for the menu item name
        [_titleLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
        [_titleLabel setTextColor:[UIColor blackColor]];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        [_titleLabel setFrame:CGRectMake(MenuCellMargin, 0, self.frame.size.width - MenuCellMargin, MenuCellHeight - 11)];
        [_titleLabel setText:[_cellMenuItemOption name]];
        [self.contentView addSubview:_titleLabel];
        
        // For the description
        [_descriptionLabel setFont:[UIFont fontWithName:BODY_FONT size:12]];
        [_descriptionLabel setTextColor:[UIColor colorFromHexValue:AFCGray]];
        [_descriptionLabel setFrame:CGRectMake(MenuCellMargin, 11, self.frame.size.width - MenuCellMargin, MenuCellHeight)];
        [_descriptionLabel setBackgroundColor:[UIColor clearColor]];
        [_descriptionLabel setText:[_cellMenuItemOption description]];
        [self.contentView addSubview:_descriptionLabel];
    }
    else if(_cellMenuItemOption.type == CHECKBOX)
    {
        tableData = [_cellMenuItemOption allParameters];
        UILabel *_titleLabel = [[UILabel alloc] init];
        UILabel *_descriptionLabel = [[UILabel alloc] init];
        
        // Create the label for the menu item name
        [_titleLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
        [_titleLabel setTextColor:[UIColor blackColor]];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        [_titleLabel setFrame:CGRectMake(MenuCellMargin, 10, self.frame.size.width - MenuCellMargin, CGRectGetHeight(self.frame) / 2.0 - 10)];
        [_titleLabel setText:[_cellMenuItemOption name]];
    
        [self.contentView addSubview:_titleLabel];
        
        // For the description
        [_descriptionLabel setFont:[UIFont fontWithName:BODY_FONT size:12]];
        [_descriptionLabel setTextColor:[UIColor colorFromHexValue:AFCGray]];
        [_descriptionLabel setFrame:CGRectMake(MenuCellMargin, CGRectGetMaxY(_titleLabel.frame), self.frame.size.width - MenuCellMargin, CGRectGetHeight(self.frame) / 2.0)];
        [_descriptionLabel setBackgroundColor:[UIColor clearColor]];
        [_descriptionLabel setText:[_cellMenuItemOption description]];
        [self.contentView addSubview:_descriptionLabel];
        
        self.accessoryView = [[UISwitch alloc] init];
        [(UISwitch*)self.accessoryView addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [(UISwitch*)self.accessoryView setOn:[_cellMenuItemOption.selectedParam boolValue]];
        self.accessoryView.exclusiveTouch = YES;
    }
    
    //TODO: add other labels
}

- (void)switchChanged:(UISwitch*)s
{
    _cellMenuItemOption.selectedParam = [NSString stringWithFormat:@"%@", [NSNumber numberWithBool:s.isOn]];
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellMenuItemOption:(AFCMenuItemOption *)cellMenuItemOption
{
    _cellMenuItemOption = cellMenuItemOption;
    [self layoutView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = _cellMenuItemOption.name;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
	}
	
    if(_cellMenuItemOption.type == CHECK_ONE)
    {
        if (indexPath.row == selectedRow)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType  = UITableViewCellAccessoryNone;
        }
    }
    else if(_cellMenuItemOption.type == CHECKBOX)
    {
        cell.accessoryView = [[UISwitch alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:HEADER_FONT size:18];
    cell.indentationLevel = 0;
    cell.indentationWidth = 0;
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedRow = (int)indexPath.row;
    _cellMenuItemOption.selectedParam = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    [tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cellMenuItemOption.type == CHECK_ONE? [tableData count] : 0;
}

@end
