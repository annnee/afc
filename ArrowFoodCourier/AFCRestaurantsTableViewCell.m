

#import "AFCRestaurantsTableViewCell.h"
#import "AFCRestaurant.h"

@implementation AFCRestaurantsTableViewCell
{
    UILabel *_titleLabel;
    UILabel *_descriptionLabel;
    UILabel *_detailsLabel;
    
    UIImageView *_iconImageView;
}

@synthesize cellRestaurant = _cellRestaurant;

#pragma mark - Lifecycle -

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        
        // Initialize private variables
        _titleLabel = [[UILabel alloc] init];
        _detailsLabel = [[UILabel alloc] init];
        _descriptionLabel = [[UILabel alloc] init];
        
        // Hide the original content to instead use custom content
        self.textLabel.alpha = 0;
        self.imageView.alpha = 0;
        
        // Add the custom labels
        [self addLabels];
        
        // Add the custom left icon image
        [self addIconImage];
    
    }
    return self;
}

-(void)dealloc
{

}

#pragma mark - Customization -

- (void)addLabels
{
    int inset = 7;
    int yPos = 0 + inset + 1;
    
    // Create the label for the name of the restaurant
    [_titleLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
    [_titleLabel setTextColor:[UIColor colorFromHexValue:AFCRed]];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [_titleLabel setFrame:CGRectMake(IMAGE_WIDTH, yPos, self.frame.size.width - IMAGE_WIDTH, CELL_HEIGHT/3)];
    [self.contentView addSubview:_titleLabel];
    
    yPos += CELL_HEIGHT / 3 - inset;
    
    // For the description
    [_descriptionLabel setFrame:CGRectMake(IMAGE_WIDTH, yPos, self.frame.size.width - IMAGE_WIDTH, CELL_HEIGHT/3)];
    [_descriptionLabel setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_descriptionLabel];
    [_descriptionLabel setFont:[UIFont fontWithName:BODY_FONT_LIGHT size:12]];
    _descriptionLabel.textColor = [UIColor darkTextColor];
    
    yPos += CELL_HEIGHT / 3 - inset;
    
    // For the details (phone and distance)
    [_detailsLabel setFont:[UIFont fontWithName:BODY_FONT_LIGHT size:12]];
    [_detailsLabel setTextColor:[UIColor colorFromHexValue:AFCGray]];
    [_detailsLabel setBackgroundColor:[UIColor clearColor]];
    [_detailsLabel setFrame:CGRectMake(IMAGE_WIDTH, yPos, self.frame.size.width - IMAGE_WIDTH, CELL_HEIGHT/3)];
    [self.contentView addSubview:_detailsLabel];
    
    //TODO: add other labels
}

- (void)addIconImage
{
    // This is just an empty view that will act as a container for all of the images
    UIView *emptyBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, IMAGE_WIDTH, CELL_HEIGHT)];
    
    // The icon background image
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconBackground"]];
    
    // Placeholder image until a different image is set
    _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PlaceholderIcon"]];
    
    // Add the images
    [self.contentView addSubview:emptyBackground];
    [emptyBackground addSubview:background];
    [emptyBackground addSubview:_iconImageView];
    
    // Center the images
    [background setCenter:emptyBackground.center];
    [_iconImageView setCenter:emptyBackground.center];
}

#pragma mark - Overriden Setters -

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellRestaurant:(AFCRestaurant *)restaurant
{
    [_titleLabel setText:[restaurant.name capitalizedString]];
    [_descriptionLabel setText:[restaurant.shortDescription capitalizedString]];
    [_detailsLabel setText:[NSString stringWithFormat:@"%@ | %@", restaurant.address, restaurant.phoneNumber]];
}

@end
