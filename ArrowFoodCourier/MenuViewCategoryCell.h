 

#import <UIKit/UIKit.h>
#import "AFCMenuCategory.h"

@interface MenuViewCategoryCell : UITableViewCell

@property (nonatomic, strong) AFCMenuCategory *cellMenuCategory;

@end
