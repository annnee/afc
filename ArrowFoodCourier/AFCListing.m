

#import "AFCListing.h"
#import "LeftDrawer.h"
#import "AFCRepository.h"
#import "CartViewCell.h"

@implementation AFCListing

@synthesize user = _user;
@synthesize isParsing = _isParsing;

+ (AFCListing *)sharedStore
{
    static AFCListing *sharedStore = nil;
    if (!sharedStore)
        sharedStore = [[super allocWithZone:nil] init];
    
    return sharedStore;
}

- (id)init
{
    self = [super init];
    if (self) {
        allRestaurants = [[NSMutableArray alloc] init];
        
        _isParsing = false;
    }
    
    return self;
}

- (NSArray *)allRestaurants
{
    return allRestaurants;
}

- (void)addRestaurant:(AFCRestaurant *)p
{
    [allRestaurants addObject:p];
}

- (void)removeRestaurant:(AFCRestaurant *)p
{
    [allRestaurants removeObjectIdenticalTo:p];
}

-(void)createCartItemsFromDictionary:(id)info
{
    self.cart = [[AFCCart alloc] initFromDictionary:info];
}

- (NSArray *)allCartItems
{
    return self.cart.cartItems;
}

-(void)addCartItem:(AFCCartItem *)cartItem
{
    [self.cart addCartItem:cartItem];
}

-(BOOL)removeCartItem:(AFCCartItem *)p
{
    return [self.cart removeCartItem:p];
}

- (float)totalItemsWithoutTaxCost
{
    float cost = 0;
    for (AFCCartItem *item in self.cart.cartItems) {
        AFCMenuItem *m = item.menuItem;
        cost += [m price];
    }
    return cost;
}

- (float)totalTaxCost
{
    float cost = TAX_RATE * [self totalItemsWithoutTaxCost];
    return cost;
}

- (float)totalItemsWithTaxCost
{
    float cost = [self totalItemsWithoutTaxCost] + [self totalTaxCost];
    return cost;
}

- (void)setUser:(AFCUser *)user
{
    _user = user;
    [[LeftDrawer sharedInstance] updateUserInformation];
}

@end
