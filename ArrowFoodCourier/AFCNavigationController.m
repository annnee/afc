 

#import "AFCNavigationController.h"
#import <QuartzCore/QuartzCore.h>

@interface AFCNavigationController ()

@end

@implementation AFCNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        
        // Modify the navigation bar view. The modifications are different depending on if the version is iOS 6 or 7
        if([self.navigationBar respondsToSelector:@selector(setBarTintColor:)])
        {
            [self.navigationBar setBarTintColor:[UIColor colorFromHexValue:AFCRed]];
            
            [self.navigationBar setTranslucent:NO];
            
            // Change the font type and color for the title on the navigation bar
            [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,
                                                        [UIFont fontWithName:HEADER_FONT size:20], NSFontAttributeName, nil]];
        }
        else
        {
            self.navigationBar.tintColor = [UIColor colorFromHexValue:AFCRed];
            [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
            [self.navigationBar setBackgroundColor:[UIColor colorFromHexValue:AFCRed]];
            
            [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            
            // Change the font type and color for the title on the navigation bar
            [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor,
                                                        [UIFont fontWithName:HEADER_FONT size:20], UITextAttributeFont,
                                                        [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0], UITextAttributeTextShadowColor,
                                                        [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                                        nil]];
            
        }
        
        // Rounded corners
//        CALayer *navLayer = self.navigationBar.layer;
//        
//        CGRect bounds = navLayer.bounds;
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bounds
//                                                       byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
//                                                             cornerRadii:CGSizeMake(5.0, 5.0)];
//        
//        CAShapeLayer *maskLayer = [CAShapeLayer layer];
//        maskLayer.frame = bounds;
//        maskLayer.path = maskPath.CGPath;
//        [navLayer addSublayer:maskLayer];
//        navLayer.mask = maskLayer;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create the status bar red background
    UIView *statusBar = [[UIView alloc] initWithFrame:CGRectMake(-300, 0, 700, 20)];
//    [statusBar setBackgroundColor:[UIColor colorFromHexValue:DarkRed]];
    [[self view] addSubview:statusBar];
	// Do any additional setup after loading the view.
    //[self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// This is to hide the shadow while on the home page
// (because there is a secondary navigation bar that attaches to it)
-(void)setDropShadowHidden:(BOOL)hidden
{
    if(hidden)
    {
        self.navigationBar.clipsToBounds = YES;
    }
    else
    {
        self.navigationBar.clipsToBounds = NO;
    }
}

-(void)moveToHomeController:(UIViewController *)viewController
{
    [self popToRootViewControllerAnimated:NO];
    [self pushViewController:viewController animated:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



@end
