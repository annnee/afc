

#import "AFCOrder.h"

@implementation AFCOrder

@synthesize orderStatus = _orderStatus;

- (id)initFromDictionary:(NSDictionary *)info
{
    NSArray *cart = [info objectForKey:@"cart"];
    if (!([cart count] > 0)) return nil;
    self = [super initFromDictionary:[cart objectAtIndex:0]];
    if (self)
    {
        [self parseOrderStatus:[info objectForKey:@"updates"]];
    }
    return self;
}

-(NSArray *)orderItems;
{
    return _allCartItems;
}

- (float)orderTotal;
{
    float total = 0;
    for (AFCCartItem *item in _allCartItems) {
        total += [[item menuItem] price];
    }
    return total;
}

- (void)parseOrderStatus:(NSArray *)updates
{
    if ([updates count] == 0) _orderStatus = SENT;
    for (NSDictionary *update in updates) {
        NSString *state = [update objectForKey:@"state"];
        if ([[state uppercaseString] isEqualToString:@"RECEIVED"] && _orderStatus <= RECEIVED) _orderStatus = RECEIVED;
        else if ([[state uppercaseString] isEqualToString:@"COOKING"] && _orderStatus <= COOKING) _orderStatus = COOKING;
        else if ([[state uppercaseString] isEqualToString:@"EN ROUTE"] && _orderStatus <= EN_ROUTE) _orderStatus = EN_ROUTE;
        else if ([[state uppercaseString] isEqualToString:@"DELIVERED"] && _orderStatus <= DELIVERED) _orderStatus = DELIVERED;
    }
}

- (NSString *)orderStatusString
{
    if (_orderStatus == RECEIVED) return @"Received";
    if (_orderStatus == COOKING) return @"Cooking";
    if (_orderStatus == EN_ROUTE) return @"En Route";
    if (_orderStatus == DELIVERED) return @"Delivered";
    else return @"Sent";
}
@end
