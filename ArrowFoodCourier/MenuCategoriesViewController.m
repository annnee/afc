 

#import "WYPopoverController.h"
#import "MenuCategoriesViewController.h"
#import "MenuView.h"
#import "MenuHeaderView.h"
#import "AFCRestaurant.h"
#import "MenuViewCategoryCell.h"
#import "CartViewController.h"
#import "AFCListing.h"
#import "MenuItemsViewController.h"

@interface MenuCategoriesViewController ()

@end

@implementation MenuCategoriesViewController
{
    AFCRestaurant *_restaurant;
    MenuHeaderView *_header;
    UITableView *_menuCategoryTableView;
    
    NSArray *_tableCategoryData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (id)initWithRestaurant:(AFCRestaurant *)restaurant
{
    self = [super init];
    if (self)
    {
        _restaurant = restaurant;
        [AFCListing sharedStore].selectedRestaurant = _restaurant;
        //[self setupView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"%f,%f",self.view.frame.origin.y,_menuCategoryTableView.frame.origin.y);
}

- (void)setupView
{
    // Reset the category to be nil
    [AFCListing sharedStore].selectedCategory = nil;
    
    // Set the view to HomeView
    self.view = [[MenuView alloc] initWithFrame:self.view.frame];
    
    // Define the navigation title for the AFCNavigationController so that it will
    // be displayed in the top bar.
    self.navigationItem.title = _restaurant.name;
    
    // Create header view
    _header = [[MenuHeaderView alloc] initWithPosition:CGPointZero restaurant:_restaurant];
    
    //[[self view] addSubview:_header];
    
    // Add icon items to the top bar
    [self addCartButtonIcon];
    [self addBackButtonIcon];
    
    // Add table view controller
    [self createTableView];
}

- (void)createTableView
{
    _tableCategoryData = [_restaurant allMenuCategories];
    _menuCategoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    _menuCategoryTableView.delegate = self;
    _menuCategoryTableView.dataSource = self;
    [self.view addSubview:_menuCategoryTableView];
    
    // Push the table view to be below the secondary nav bar
    //[self.view sendSubviewToBack:_menuTableView];
    
    // Remove the extra whitespace at the left of the tableview
    if ([_menuCategoryTableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [_menuCategoryTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    _menuCategoryTableView.tableHeaderView = _header;
    _menuCategoryTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableCategoryData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"menuTableCategoryItem";
    
    MenuViewCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[MenuViewCategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.cellMenuCategory = [_tableCategoryData objectAtIndex:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return MenuCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AFCMenuCategory *category = [_tableCategoryData objectAtIndex:indexPath.row];
    MenuItemsViewController *ivc = [[MenuItemsViewController alloc] initWithCategory:category];
    [[self mainNavigationController] pushViewController:ivc animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
