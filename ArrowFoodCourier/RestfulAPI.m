

#import "RestfulAPI.h"

#define kRequestStringServer @"http://rest-arrow.herokuapp.com"

@implementation RestfulAPI

static BOOL debug = NO;

#pragma mark Request Parameters

+(void)makeRequest:(RequestType)type withService:(NSString *)service body:(id)body andHandler:(void (^)(int statusCode, NSDictionary *responseDictionary, NSArray* responseArray, NSError *error))callback
{
    // Make sure it is URL encoded in case weird characters or spaces
    service = [self websafeString:service];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kRequestStringServer, service]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSError *error;
    
    switch (type)
    {
        case RequestTypePOST:
        {
            NSData *postData = body? [NSJSONSerialization dataWithJSONObject:body options:kNilOptions error:&error] : nil;
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            break;
        }
        case RequestTypeGET:
        {
            break;
        }
        case RequestTypeDELETE:
        {
            [request setHTTPMethod:@"DELETE"];
            break;
        }
        default:
            break;
    }
    [self beginAsynchronousRequest:request WithCallback:callback];
}

#pragma mark Send Request

+ (void)beginAsynchronousRequest:(NSURLRequest*)request WithCallback:(void (^)(int, NSDictionary *, NSArray *, NSError *))callback
{
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         // It could be a dictionary or array
         NSDictionary *responseJSONDictionary = nil;
         NSArray *responseJSONArray = nil;
         
         NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
         NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         int statusCode = (int)[HTTPResponse statusCode];
         
         // This is dumb, server really should just stick to JSON ,but whatever,
         // nothing I can do about it. If the status isnt 200 its a string, otherwise its
         // JSON.
         if(statusCode != 200 && statusCode != 503)
         {
             responseJSONDictionary = [NSDictionary dictionaryWithObject:responseString forKey:@"error"];
         }
         else if (data.length > 0 && connectionError == nil)
         {
             NSError *error;
             id jsonData = [NSJSONSerialization JSONObjectWithData:data
                                                           options:kNilOptions
                                                             error:&error];
             
             if([jsonData isKindOfClass:[NSArray class]])
             {
                 responseJSONArray = jsonData;
             }
             else if([jsonData isKindOfClass:[NSDictionary class]])
             {
                 responseJSONDictionary = jsonData;
             }
             else
             {
                 NSLog(@"ERROR. Return type not supported");
             }
             
         }
         if(debug)
         {
             NSLog(@"Response: %@ - %@", response, responseString);
             NSLog(@"Error: %@", connectionError);
         }
         
         callback(statusCode, responseJSONDictionary, responseJSONArray, connectionError);
     }];
}

+ (NSString *)websafeString:(NSString *)string
{
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes
                             (kCFAllocatorDefault,
                             (CFStringRef)string, NULL,
                             (CFStringRef)@":@!$'()*+,; ",
                             kCFStringEncodingUTF8));
}


@end
