// Pricint constants
#define TAX_RATE 0.06

// Global font types
#define HEADER_FONT @"Rokkitt"
#define BODY_FONT @"Helvetica"
#define SUBHEADER_FONT @"Intro"
#define BODY_FONT_BOLD @"Helvetica-Bold"
#define BODY_FONT_LIGHT @"HelveticaNeue-Light"

// Global constants
#define DEFAULT_TABLE_CELL_HEIGHT 44

// For the SecondaryNavigationBar
#define SecondaryNavBarHeight 38            // Height of the nav bar
#define SecondaryNavBarSeparatorThickness 1 // Thickness of line separators
#define NumberOfSecondaryBarItems 3         // Number of buttons in the nav bar
#define SecondaryNavBarSeparatorHeight 24   // Height of the vertical separator between buttons

// For the Menu
#define MenuHeaderHeight 250
#define MenuHeaderMargin 10
#define MenuStoreFrontHeight 200
#define MenuStoreFrontDescriptionHeight 60
#define MenuCellMargin 10
#define MenuCellHeight 60

// For the Cart
#define CartPopoverHeight 400
#define CartPopoverWidth 250
#define CartCellHeight 60
#define CartCellMargin 10
#define CartFooterHeight 145

// For the Options View
#define OptionsFooterHeight 60
#define OPTION_CELL_FOOTER_HEIGHT 50
#define CELL_RADIO_HEIGHT 200

// For the AFCHomeTableViewCell
#define CELL_HEIGHT 75                      // Height of each cell.
#define IMAGE_WIDTH 76                      // Width of the left icon image in each cell.

// For the left drawer
#define TOP_VIEW_UNCOVERED 55               // The amount that is showing of the top view when the drawer is open
#define DRAWER_BUTTON_HEIGHT 44             // The height of each button option in the drawer
#define DRAWER_SEPARATOR_HEIGHT 32          // Height of the gray separator between the button options
#define LEFT_CONTENT_PADDING 15             // The amount of space to the left of all the content
#define SWIPE_THRESHOLD 10
