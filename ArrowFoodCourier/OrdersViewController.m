//
//  OrdersViewController.m
//  ArrowFoodCourier
//
//  Created by Annie Neel on 3/20/14.
//  Copyright (c) 2014 Ashleigh Chape. All rights reserved.
//

#import "OrdersViewController.h"
#import "OrdersView.h"

@interface OrdersViewController ()

@end

@implementation OrdersViewController
{
    UIView *_activeView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupView
{
    // Set the view to LogOn View
    self.view = [[OrdersView alloc] initWithFrame:self.view.frame];
    
    // Define the navigation title for the AFCNavigationController so that it will
    // be displayed in the top bar.
    self.navigationItem.title = @"ORDERS";
    
    // Add icon items to the top bar
    [self addHomeIcon];
}

@end
