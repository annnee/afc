
#import "CartViewFooter.h"
#import "AFCListing.h"

@implementation CartViewFooter
{
    // Empty cart UI items
    UILabel *_emptyCartLabel;
    
    // Non-empty cart UI items
    UILabel *_taxLabel;
    UILabel *_totalLabel;
    UILabel *_subtotalLabel;
    
    UILabel *_taxCostLabel;
    UILabel *_totalCostLabel;
    UILabel *_subtotalCostLabel;
    
    UIView *_separatorLine;
    UIButton *_checkoutButton;
    
    BOOL _includeCheckoutButton;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        _includeCheckoutButton = YES;
        if ([(AFCCart*)[[AFCListing sharedStore] cart] cartItems] == 0) [self setupEmptyCartElementsWithFade:NO];
        else [self setupCheckoutCartElements];
    }
    return self;
}

-(id)initWithoutCheckoutWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        _includeCheckoutButton = NO;
        if ([[(AFCCart*)[[AFCListing sharedStore] cart] cartItems] count] == 0) [self setupEmptyCartElementsWithFade:NO];
        else [self setupCheckoutCartElements];
    }
    return self;
}

- (void)setupEmptyCartElementsWithFade:(BOOL)fade
{
    self.backgroundColor = [UIColor clearColor];
    _emptyCartLabel = [[UILabel alloc] init];
    [_emptyCartLabel setFrame:CGRectMake(0, 0, self.frame.size.width, CartFooterHeight)];
    [_emptyCartLabel setTextAlignment:NSTextAlignmentCenter];
    [_emptyCartLabel setBackgroundColor:[UIColor clearColor]];
    _emptyCartLabel.font = [UIFont fontWithName:HEADER_FONT size:16];
    _emptyCartLabel.text = @"No items in cart!";
    if (fade)
    {
        _emptyCartLabel.alpha = 0;
        [_emptyCartLabel setFrame:CGRectMake(0, 20, self.frame.size.width, CartFooterHeight)];
    }
    
    [self addSubview:_emptyCartLabel];
    
    if (fade) [UIView animateWithDuration:.5 animations:^{ _emptyCartLabel.alpha = 1; [_emptyCartLabel setFrame:CGRectMake(0, 0, self.frame.size.width, CartFooterHeight)] ;}];
}

- (void)setupCheckoutCartElements
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect taxRect = CGRectMake(CartCellMargin, 2, self.frame.size.width - 2*CartCellMargin, 30);
    CGRect subtotalRect = CGRectMake(CartCellMargin, 25, self.frame.size.width - 2*CartCellMargin, 30);
    CGRect totalRect = CGRectMake(CartCellMargin, 50, self.frame.size.width - 2*CartCellMargin, 30);
    CGRect checkoutButtonRect = CGRectMake(0, 85, self.frame.size.width, 60);
    
    _taxLabel = [[UILabel alloc] init];
    [_taxLabel setFrame:taxRect];
    [_taxLabel setTextAlignment:NSTextAlignmentLeft];
    [_taxLabel setBackgroundColor:[UIColor clearColor]];
    _taxLabel.font = [UIFont fontWithName:HEADER_FONT size:16];
    _taxLabel.text = @"Tax:";
    
    _taxCostLabel = [[UILabel alloc] init];
    [_taxCostLabel setFrame:taxRect];
    [_taxCostLabel setTextAlignment:NSTextAlignmentRight];
    [_taxCostLabel setBackgroundColor:[UIColor clearColor]];
    _taxCostLabel.font = [UIFont fontWithName:HEADER_FONT size:16];
    _taxCostLabel.text = [NSString stringWithFormat:@"$%.2f", [[AFCListing sharedStore] totalTaxCost]];
    
    _subtotalLabel = [[UILabel alloc] init];
    [_subtotalLabel setFrame:subtotalRect];
    [_subtotalLabel setTextAlignment:NSTextAlignmentLeft];
    [_subtotalLabel setBackgroundColor:[UIColor clearColor]];
    _subtotalLabel.font = [UIFont fontWithName:HEADER_FONT size:16];
    _subtotalLabel.text = @"Subtotal:";
    
    _subtotalCostLabel = [[UILabel alloc] init];
    [_subtotalCostLabel setFrame:subtotalRect];
    [_subtotalCostLabel setTextAlignment:NSTextAlignmentRight];
    [_subtotalCostLabel setBackgroundColor:[UIColor clearColor]];
    _subtotalCostLabel.font = [UIFont fontWithName:HEADER_FONT size:16];
    _subtotalCostLabel.text = [NSString stringWithFormat:@"$%.2f", [[AFCListing sharedStore] totalItemsWithoutTaxCost]];
    
    _separatorLine = [[UIView alloc] initWithFrame:CGRectMake(CartCellMargin - 3, 52, self.frame.size.width - 2*CartCellMargin + 6, 1)];
    [_separatorLine setBackgroundColor:[UIColor colorFromHexValue:0xECECEC]];
    
    _totalLabel = [[UILabel alloc] init];
    [_totalLabel setFrame:totalRect];
    [_totalLabel setTextAlignment:NSTextAlignmentLeft];
    [_totalLabel setBackgroundColor:[UIColor clearColor]];
    _totalLabel.font = [UIFont fontWithName:HEADER_FONT size:16];
    _totalLabel.text = @"Total:";
    
    _totalCostLabel = [[UILabel alloc] init];
    [_totalCostLabel setFrame:totalRect];
    [_totalCostLabel setTextAlignment:NSTextAlignmentRight];
    [_totalCostLabel setBackgroundColor:[UIColor clearColor]];
    _totalCostLabel.font = [UIFont fontWithName:HEADER_FONT size:16];
    _totalCostLabel.text = [NSString stringWithFormat:@"$%.2f", [[AFCListing sharedStore] totalItemsWithTaxCost]];
    
    [self addSubview:_taxLabel];
    [self addSubview:_totalLabel];
    [self addSubview:_taxCostLabel];
    [self addSubview:_totalCostLabel];
    [self addSubview:_separatorLine];
    [self addSubview:_subtotalLabel];
    [self addSubview:_subtotalCostLabel];
    
    if(_includeCheckoutButton)
    {
        _checkoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkoutButton setFrame:checkoutButtonRect];
        _checkoutButton.backgroundColor = [UIColor colorFromHexValue:0xF8F8F8];
        _checkoutButton.titleLabel.font = [UIFont fontWithName:HEADER_FONT size:18];
        [_checkoutButton setTitleColor:[UIColor colorFromHexValue:AFCRed] forState:UIControlStateNormal];
        [_checkoutButton setTitleColor:[UIColor colorFromHexValue:AFCGray] forState:UIControlStateHighlighted];
        [_checkoutButton setTitle:@"Check Out" forState:UIControlStateNormal];
        [_checkoutButton addTarget:self action:@selector(checkoutButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_checkoutButton];
    }
    
}

- (void)update
{
    if([[(AFCCart*)[[AFCListing sharedStore] cart] cartItems] count] > 0)
    {
        _taxCostLabel.text = [NSString stringWithFormat:@"$%.2f", [[AFCListing sharedStore] totalTaxCost]];
        _subtotalCostLabel.text = [NSString stringWithFormat:@"$%.2f", [[AFCListing sharedStore] totalItemsWithoutTaxCost]];
        _totalCostLabel.text = [NSString stringWithFormat:@"$%.2f", [[AFCListing sharedStore] totalItemsWithTaxCost]];
    }
    else
    {
        for(UIView* subview in self.subviews)
            [subview removeFromSuperview];
        [self setupEmptyCartElementsWithFade:YES];
    }
}

- (IBAction)checkoutButtonPressed:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckoutButtonPressed" object:nil];
}

@end
