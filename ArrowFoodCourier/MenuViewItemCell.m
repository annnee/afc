 

#import "MenuViewItemCell.h"

@implementation MenuViewItemCell
{
    UILabel *_nameLabel;
    UILabel *_descriptionLabel;
    UILabel *_priceLabel;
    
    UIImageView *_iconImageView;
}

@synthesize cellMenuItem = _cellMenuItem;

#pragma mark - Lifecycle -

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        
        // Initialize private variables
        _nameLabel = [[UILabel alloc] init];
        _priceLabel = [[UILabel alloc] init];
        _descriptionLabel = [[UILabel alloc] init];
        
        // Hide the original content to instead use custom content
        self.textLabel.alpha = 0;
        self.imageView.alpha = 0;
        
        // Add the custom labels
        [self addLabels];
        
    }
    return self;
}

-(void)dealloc
{
    
}

#pragma mark - Customization -

- (void)addLabels
{
    // Create the label for the menu item name
    [_nameLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
    [_nameLabel setTextColor:[UIColor blackColor]];
    [_nameLabel setBackgroundColor:[UIColor clearColor]];
    [_nameLabel setFrame:CGRectMake(MenuCellMargin, 0, self.frame.size.width - MenuCellMargin, MenuCellHeight - 11)];
    [self.contentView addSubview:_nameLabel];
    
    // For the description
    [_descriptionLabel setFont:[UIFont fontWithName:BODY_FONT size:12]];
    [_descriptionLabel setTextColor:[UIColor colorFromHexValue:AFCGray]];
    [_descriptionLabel setFrame:CGRectMake(MenuCellMargin, 11, self.frame.size.width - MenuCellMargin, MenuCellHeight)];
    [_descriptionLabel setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_descriptionLabel];
    
    // For the price
    [_priceLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
    [_priceLabel setTextColor:[UIColor colorFromHexValue:AFCGray]];
    [_priceLabel setBackgroundColor:[UIColor clearColor]];
    [_priceLabel setFrame:CGRectMake(0, 0, self.frame.size.width - MenuCellMargin, MenuCellHeight)];
    [_priceLabel setTextAlignment:NSTextAlignmentRight];
    [self.contentView addSubview:_priceLabel];
    
    //TODO: add other labels
}

#pragma mark - Overriden Setters -

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setCellMenuItem:(AFCMenuItem *)cellMenuItem
{
    _cellMenuItem = cellMenuItem;
    [_nameLabel setText:_cellMenuItem.name];
    [_descriptionLabel setText:_cellMenuItem.description];
    [_priceLabel setText:[NSString stringWithFormat:@"$%.2f", _cellMenuItem.price]];
}

@end