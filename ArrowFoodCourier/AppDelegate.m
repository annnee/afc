
#import "AppDelegate.h"
#import "RestaurantsViewController.h"
#import "LogOnViewController.h"
#import "DriverModeViewController.h"
#import "PayPalMobile.h"

#import "AFCRepository.h"

@implementation AppDelegate
{
    UISwipeGestureRecognizer *_swipeLeft;
    UISwipeGestureRecognizer *_swipeRight;
}

@synthesize driverMode = _driverMode;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentSandbox : @"Adpj4hDKKuu1DfP2TKatcCL4-_enPGi9YndWgoZ2Gp7t_cMR7aJXM-2iVr09"}];
    
    // Create the root view controller (which is the stack navigation object)
    self.rootViewController = [[AFCNavigationController alloc] init];
    [self.window setRootViewController:self.rootViewController];
    [self.rootViewController.view setFrame:self.window.frame];
    
    // Root controller is just an empty controller, since all the items in the left drawer will essentially be
    // used as a root controller
    AFCViewController* viewController = [[AFCViewController alloc] initWithNibName:nil bundle:nil];
    [self.rootViewController pushViewController:viewController animated:NO];

    [self.window makeKeyAndVisible];
    
    _driverMode = NO;
    
    [self setupLoginView];
    [self setupLeftDrawer];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Login

- (void)setupLoginView
{

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:[[LogOnViewController alloc] initWithNibName:nil bundle:nil]];
    [navController setNavigationBarHidden:YES animated:NO];
    [self.rootViewController presentViewController:navController animated:NO completion:nil];
    
    //code to deal with first load or not - this is where we can insert the stuff for the intro.
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"IntroRun"];
    if([savedValue isEqualToString:@"Yes"])
    {
        // code has already run once...
    } else
    {
        // code has not been run
        NSString *valueToSave = @"Yes";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"IntroRun"];
    }
}

#pragma mark - Drawer

- (void)setupLeftDrawer
{
    // Initialize the drawer view now so that it does not lazy load when the user tries to open it
    [LeftDrawer sharedInstance];
    
    // Add the swipe gesture that will handle left/right swipes to open and close the drawers
    _swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedLeft:)];
    _swipeLeft.cancelsTouchesInView = YES;
    _swipeLeft.delegate = self;
    _swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    
    _swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedRight:)];
    _swipeRight.cancelsTouchesInView = YES;
    _swipeRight.delegate = self;
    _swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.window addGestureRecognizer:_swipeLeft];
    [self.window addGestureRecognizer:_swipeRight];
}

-(void)setDrawerGesturesEnabled:(BOOL)enabled
{
    _swipeLeft.enabled = enabled;
    _swipeRight.enabled = enabled;
}

#pragma mark - Gesture Recognizer

// Only recieve swipes at the edges of the screen
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint touchLocation = [touch locationInView:[self window]];
    if(((UISwipeGestureRecognizer*)gestureRecognizer).direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if(touchLocation.x < [[UIScreen mainScreen] bounds].size.width - SWIPE_THRESHOLD)
        {
            return NO;
        }
    }
    if(((UISwipeGestureRecognizer*)gestureRecognizer).direction == UISwipeGestureRecognizerDirectionRight)
    {
        if(touchLocation.x > SWIPE_THRESHOLD)
        {
            return NO;
        }
    }
    
    return YES;
}

- (void)swipedLeft:(UIGestureRecognizer*)gestureRec
{
    // Only work if it was on the edge of the screen
    
    
    // Dismiss the keyboard if it's open
    [[self window] endEditing:YES];
    
    // Hide the drawer
    if([[LeftDrawer sharedInstance] isOpen])
        [[LeftDrawer sharedInstance] hide];
    
}

- (void)swipedRight:(UIGestureRecognizer*)gestureRec
{
    // Dismiss the keyboard if it's open
    [[self window] endEditing:YES];
    
    // Open the drawer
    if(![[LeftDrawer sharedInstance] isOpen])
        [[LeftDrawer sharedInstance] show];
}

@end
