 

#import <UIKit/UIKit.h>
#import "AFCMenuItem.h"

@interface MenuViewItemCell : UITableViewCell

@property (nonatomic, strong) AFCMenuItem *cellMenuItem;

@end
