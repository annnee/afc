

#import <Foundation/Foundation.h>
#import "AFCMenuItem.h"

@interface AFCCartItem : NSObject

@property (nonatomic, strong) AFCMenuItem *menuItem;
@property (nonatomic, strong) NSString *menuCategory;
@property (nonatomic, strong) NSString *restaurant;

- (id)initWithDictionary:(NSDictionary*)info;

- (id)initWithMenuItem:(AFCMenuItem*)item category:(NSString*)category andRestaurant:(NSString*)restaurant;

@end
