 
#import "CreateAccountViewController.h"
#import "CreateAccountView.h"
#import "RestaurantsViewController.h"
#import "ProfileDetailHeader.h"

@interface CreateAccountViewController ()

@end

enum
{
    fname = 1000,
    lname,
    username,
    email,
    password,
    confirmpassword,
    line1 = 2000,
    line2,
    city,
    state,
    zip,
    defaultBill,
    defaultShip,
    phoneNumber = 3000,
    phoneType,
    defaultPhone
    
} Tags;

@implementation CreateAccountViewController
{
    NSMutableArray *_allFields;
    
    UIButton* signInInstead;
    UIButton* createAccount;
    
    UITableView *_createAccountTableView;
    int _numberOfAddresses;
    int _numberOfPhones;
    int _totalNumberOfRows;
    int _cellHeight;
    
    UITextField *_activeField;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupView
{
    self.view = [[CreateAccountView alloc] initWithFrame:self.view.frame];
    
    [self registerForKeyboardNotifications];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] setDrawerGesturesEnabled:NO];
    
    CreateAccountView* selfView = (CreateAccountView*)self.view;
    selfView.backgroundColor = [UIColor colorWithRed:0.871 green:0.871 blue:0.871 alpha:1];

    signInInstead = [UIButton buttonWithType:UIButtonTypeCustom];
    CALayer *btnLayer = [signInInstead layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
    [signInInstead setTitle:@"Sign In" forState:UIControlStateNormal];
    signInInstead.frame = CGRectMake(0, 0, 130, 30);
    signInInstead.backgroundColor = [UIColor whiteColor];
    [signInInstead addTarget:self action:@selector(signingIn:) forControlEvents:UIControlEventTouchUpInside];
    [[signInInstead titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
    [signInInstead setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [selfView addSubview:signInInstead];
    
    createAccount = [UIButton buttonWithType:UIButtonTypeCustom];
    CALayer *cabtnlayer = [createAccount layer];
    [cabtnlayer setMasksToBounds:YES];
    [cabtnlayer setCornerRadius:5.0f];
    [createAccount setTitle:@"Create Account" forState:UIControlStateNormal];
    createAccount.frame = CGRectMake(0, 0, 130, 30);
    createAccount.backgroundColor = [UIColor colorFromHexValue:AFCRed];
    [createAccount addTarget:self action:@selector(creatingAccount:) forControlEvents:UIControlEventTouchUpInside];
    [[createAccount titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
    [createAccount setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selfView addSubview:createAccount];
    
    _allFields = [[NSMutableArray alloc] init];
    _numberOfAddresses = 1;
    _numberOfPhones = 1;
    _cellHeight = 35;
    _createAccountTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))
                                                           style:UITableViewStyleGrouped];
    _createAccountTableView.delegate = self;
    _createAccountTableView.dataSource = self;
    _createAccountTableView.backgroundColor = [UIColor colorWithRed:0.871 green:0.871 blue:0.871 alpha:1];
    _createAccountTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_createAccountTableView.frame),
                                                                                       CGRectGetHeight(createAccount.frame) + CGRectGetHeight(signInInstead.frame) + 10 + 20)];
    [_createAccountTableView.tableFooterView addSubview:signInInstead];
    [_createAccountTableView.tableFooterView addSubview:createAccount];
    
    float padding = (CGRectGetWidth(_createAccountTableView.frame) - CGRectGetWidth(signInInstead.frame)*2) / 3.0;
    [signInInstead setCenter:CGPointMake(padding + CGRectGetWidth(signInInstead.frame) / 2.0,
                                         CGRectGetHeight(_createAccountTableView.tableFooterView.frame) / 2.0)];
    
    [createAccount setCenter:CGPointMake(CGRectGetMaxX(signInInstead.frame) + padding + CGRectGetWidth(createAccount.frame) / 2.0,
                                         CGRectGetHeight(_createAccountTableView.tableFooterView.frame) / 2.0)];
    // Remove the extra whitespace at the left of the tableview
    if ([_createAccountTableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [_createAccountTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    [selfView addSubview:_createAccountTableView];
    
}

- (void)signingIn:(UIButton*)button
{
    NSLog(@"Sign in");
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark - Keyboard

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)notification
{
    UIScrollView *setUp = _createAccountTableView;
    
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height - (CGRectGetHeight(self.view.frame) -
                                                                             CGRectGetMaxY(_createAccountTableView.frame)), 0.0);
    setUp.contentInset = contentInsets;
    setUp.scrollIndicatorInsets = contentInsets;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIScrollView *setUp = _createAccountTableView;
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        setUp.contentInset = contentInsets;
        setUp.scrollIndicatorInsets = contentInsets;
    }completion:nil];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark - Text Field

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeField = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self dismissKeyboard];
    return YES;
}

#pragma mark - TableView Datasource/Delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *headerText = section == 0? @"Login Information" : section == 1? @"Delivery Address" : section == 2? @"Phone Number" : 0;
    ProfileDetailHeader *header = [[ProfileDetailHeader alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, DEFAULT_TABLE_CELL_HEIGHT)];
    [[header headerLabel] setText:headerText];
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return DEFAULT_TABLE_CELL_HEIGHT;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _cellHeight;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0? 6 : section == 1? 5*_numberOfAddresses : section == 2? 2*_numberOfPhones : 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    NSMutableArray *subViews = [[NSMutableArray alloc] init];
    int padding = 10;
    
    if(indexPath.section == 0)
    {
        UITextField *field;
        switch (indexPath.row)
        {
            case 0:
            {
                identifier = @"firstname";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"First Name";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = fname;
                break;
            }
            case 1:
            {
                identifier = @"lastname";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Last Name";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = lname;
                break;
            }
            case 2:
            {
                identifier = @"username";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Username";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = username;
                field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                field.autocorrectionType = UITextAutocorrectionTypeNo;
                field.keyboardType = UIKeyboardTypeEmailAddress;
                break;
            }
            case 3:
            {
                identifier = @"emailaddress";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Email Address";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = email;
                field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                field.autocorrectionType = UITextAutocorrectionTypeNo;
                field.keyboardType = UIKeyboardTypeEmailAddress;
                break;
            }
            case 4:
            {
                identifier = @"password";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Password";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.secureTextEntry = YES;
                field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                field.autocorrectionType = UITextAutocorrectionTypeNo;
                field.tag = password;
                break;
            }
            case 5:
            {
                identifier = @"confirmpassword";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Confirm Password";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.secureTextEntry = YES;
                field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                field.autocorrectionType = UITextAutocorrectionTypeNo;
                field.tag = confirmpassword;
                break;
            }
            default:
                break;
        }
        field.delegate = self;
        [subViews addObject:field];
    }
    else if(indexPath.section == 1)
    {
        int row = indexPath.row % 6;
        switch (row)
        {
            case 0:
            {
                identifier = [NSString stringWithFormat:@"line1%d", _numberOfAddresses];
                UITextField *field;
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Address Line 1";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = line1;
                [subViews addObject:field];
                break;
            }
            case 1:
            {
                identifier = [NSString stringWithFormat:@"line2%d", _numberOfAddresses];
                UITextField *field;
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Address Line 2";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = line2;
                [subViews addObject:field];
                break;
            }
            case 2:
            {
                identifier = [NSString stringWithFormat:@"citystate%d", _numberOfAddresses];
                UITextField *field;
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width / 2 - padding / 2, _cellHeight)];
                field.placeholder = @"City";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.keyboardType = UIKeyboardTypeAlphabet;
                field.tag = city;
                
                UITextField *field2;
                field2 = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(field.frame) + padding / 2, 0, _createAccountTableView.frame.size.width / 2 - padding / 2, _cellHeight)];
                field2.placeholder = @"State";
                field2.backgroundColor = [UIColor whiteColor];
                field2.borderStyle = UITextBorderStyleNone;
                field2.tag = state;
                field2.keyboardType = UIKeyboardTypeAlphabet;
                
                [subViews addObject:field];
                [subViews addObject:field2];
                break;
            }
            case 3:
            {
                identifier = [NSString stringWithFormat:@"zip%d", _numberOfAddresses];
                UITextField *field;
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Zip";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = zip;
                field.keyboardType = UIKeyboardTypeNumberPad;
                [subViews addObject:field];
                break;
            }
            case 4:
            {
                identifier = [NSString stringWithFormat:@"defaultBilling%d", _numberOfAddresses];
                UILabel *label;
                label = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                label.text = @"Use address for billing?";
                label.backgroundColor = [UIColor whiteColor];
                [subViews addObject:label];
                
                UISwitch *s = [[UISwitch alloc] init];
                [s setFrame:CGRectMake(CGRectGetWidth(_createAccountTableView.frame) - CGRectGetWidth(s.frame) - padding, 0, 0, 0)];
                s.center = CGPointMake(s.center.x, _cellHeight / 2.0);
                s.tag = defaultBill;
                [s setOn:NO];
                [subViews addObject:s];
                break;
            }
            default:
                break;
        }
    }
    else if(indexPath.section == 2)
    {
        int row = indexPath.row % 3;
        switch (row)
        {
            case 0:
            {
                identifier = [NSString stringWithFormat:@"number%d", _numberOfPhones];
                UITextField *field;
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Phone Number";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.tag = phoneNumber;
                field.keyboardType = UIKeyboardTypePhonePad;
                [subViews addObject:field];
                break;
            }
            case 1:
            {
                identifier = [NSString stringWithFormat:@"name%d", _numberOfPhones];
                UILabel *label;
                label = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, _createAccountTableView.frame.size.width - padding, _cellHeight)];
                label.text = @"Mobile phone?";
                label.backgroundColor = [UIColor whiteColor];
                [subViews addObject:label];
                
                UISwitch *s = [[UISwitch alloc] init];
                [s setFrame:CGRectMake(CGRectGetWidth(_createAccountTableView.frame) - CGRectGetWidth(s.frame) - padding, 0, 0, 0)];
                s.center = CGPointMake(s.center.x, _cellHeight / 2.0);
                s.tag = phoneType;
                [s setOn:YES];
                [subViews addObject:s];
                break;
            }
            case 2:
            {
                
                
                break;
            }
            default:
                break;
        }
        
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if( cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        for(UIView *v in subViews)
        {
            if([v isKindOfClass:[UITextField class]])
            {
                ((UITextField*)v).delegate = self;
                [_allFields addObject:v];
            }
            if([v isKindOfClass:[UISwitch class]])
            {
                [_allFields addObject:v];
            }
            
            [cell.contentView addSubview:v];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


#pragma mark - Create Account
#define emailRegex @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"

- (BOOL)errorCheck
{
    NSString *errorString;
    
    for(UIView *view in _allFields)
    {
        if([view isKindOfClass:[UITextField class]] && view.tag != line2)
        {
            UITextField *f = (UITextField*)view;
            if(f.text.length == 0)
            {
                errorString = @"Please fill out all fields";
                
            }
        }
    }
    if(!errorString)
    {
        UITextField *emailField = (UITextField*)[_createAccountTableView viewWithTag:email];
        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:emailRegex options:NSRegularExpressionCaseInsensitive error:NULL];
        NSUInteger emailMatches = [expression numberOfMatchesInString:[self trimmedString:emailField.text] options:0 range:NSMakeRange(0, [[self trimmedString:emailField.text] length])];
        
        if(emailMatches == 0)
        {
            errorString = @"Please enter a valid email address";
        }
    }
    if(!errorString)
    {
        UITextField *passwordField = (UITextField*)[_createAccountTableView viewWithTag:password];
        UITextField *confirmPasswordField = (UITextField*)[_createAccountTableView viewWithTag:confirmpassword];
        
        if(![[self trimmedString:passwordField.text] isEqualToString:[self trimmedString:confirmPasswordField.text]])
        {
            errorString = @"Passwords do not match";
        }
    }
    
    if(errorString)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Create account error"
                                                         message:errorString
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        [alert show];
        return NO;
    }
    return YES;
}

// Trim leading and trailing whitespace
- (NSString*)trimmedString:(NSString*)string
{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (void)creatingAccount:(UIButton*)button
{
    NSLog(@"Create Account");
    
    [self startLoading];
    
    [self.view endEditing:YES];
    [self.view setUserInteractionEnabled:NO];
    
    createAccount.enabled = NO;
    signInInstead.enabled = NO;
    
    if([self errorCheck])
    {
        // Make request
        NSString *firstname = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:fname]).text];
        NSString *lastname = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:lname]).text];
        NSString *name = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
        NSString *usernameString = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:username]).text];
        NSString *emailAddress = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:email]).text];
        NSString *pword = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:password]).text];
        
        NSString *l1 = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:line1]).text];
        NSString *l2 = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:line2]).text];
        NSString *cityString = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:city]).text];
        NSString *stateString = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:state]).text];
        NSString *zipString = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:zip]).text];
        BOOL defaultShip = YES;
        BOOL defaultBilling = ((UISwitch*)[_createAccountTableView viewWithTag:defaultBill]).isOn;
        
        NSString *phoneNumberString = [self trimmedString:((UITextField*)[_createAccountTableView viewWithTag:phoneNumber]).text];
        BOOL defaultNum = YES;
        NSString *type = ((UISwitch*)[_createAccountTableView viewWithTag:phoneType]).isOn? @"Mobile" : @"Home";
        
        
        AFCAddress *address = [[AFCAddress alloc] initWithLine1:l1 line2:l2 city:cityString state:stateString zip:zipString
                                                 defaultBilling:defaultBilling defaultShipping:defaultShip];
        AFCPhone *phone = [[AFCPhone alloc] initWithType:type number:phoneNumberString defaultNumber:defaultNum];
        
        [AFCRepository createAccountWithUsername:usernameString password:pword email:emailAddress name:name
                                       addresses:[NSArray arrayWithObject:address.dictionary]
                                          phones:[NSArray arrayWithObject:phone.dictionary] responseHandler:^(NSString *error)
        {
            if(!error)
            {
                createAccount.enabled = YES;
                signInInstead.enabled = YES;
                [self.view setUserInteractionEnabled:YES];
                [self stopLoading];
                
                // Go back so they can now log in with their account
                [[self navigationController] popViewControllerAnimated:YES];
            }
            else
            {
                UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Create account error"
                                                                 message:error
                                                                delegate:self
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles: nil];
                [alert show];
                createAccount.enabled = YES;
                signInInstead.enabled = YES;
                [self.view setUserInteractionEnabled:YES];
                [self stopLoading];
                NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
                [_createAccountTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
        }];
        
    }
    else
    {
        createAccount.enabled = YES;
        signInInstead.enabled = YES;
        [self.view setUserInteractionEnabled:YES];
        [self stopLoading];
        NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
        [_createAccountTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}


@end
