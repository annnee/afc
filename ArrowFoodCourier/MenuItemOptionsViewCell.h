 

#import <UIKit/UIKit.h>
#import "AFCMenuItemOption.h"

@interface MenuItemOptionsViewCell : UITableViewCell <UITableViewDataSource, UITableViewDelegate>

@property (readonly) float cellHeight;
@property (nonatomic, strong) AFCMenuItemOption *cellMenuItemOption;

@end
