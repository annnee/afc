 

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "WYPopoverController.h"

@interface AFCViewController : UIViewController<WYPopoverControllerDelegate>

- (AFCNavigationController*) mainNavigationController;

- (void) addHomeIcon;

- (void) addBackButtonIcon;
- (void) addCartButtonIcon;
- (void) updateCartBadge;
- (void) presentCartNotification;

- (int) heightVisibleOnScreen;

// Method that should be overriden by child class
- (void)setupView;

- (void)startLoading;
- (void)stopLoading;

@end
