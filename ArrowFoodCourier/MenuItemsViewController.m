 

#import "MenuItemsViewController.h"
#import "MenuView.h"
#import "MenuViewItemCell.h"
#import "AFCListing.h"
#import "MenuItemsOptionsViewController.h"

@interface MenuItemsViewController ()

@end

@implementation MenuItemsViewController
{
    AFCMenuCategory *_category;
    UITableView *_menuItemsTableView;
    
    NSArray *_tableItemsData;
}

- (id)initWithCategory:(AFCMenuCategory *)category
{
    self = [super init];
    if (self)
    {
        _category = category;
        [AFCListing sharedStore].selectedCategory = _category;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [self setupView];
    [super viewWillAppear:animated];
}

- (void)setupView
{
    // Define the navigation title for the AFCNavigationController so that it will
    // be displayed in the top bar.
    self.navigationItem.title = _category.name.uppercaseString;
    
    // Add icon items to the top bar
    [self addCartButtonIcon];
    [self addBackButtonIcon];
    
    // Add table view controller
    [self createTableView];
}

- (void)createTableView
{
    _tableItemsData = [_category allMenuItems];
    _menuItemsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    _menuItemsTableView.delegate = self;
    _menuItemsTableView.dataSource = self;
    [self.view addSubview:_menuItemsTableView];
    
    // Push the table view to be below the secondary nav bar
    //[self.view sendSubviewToBack:_menuTableView];
    
    // Remove the extra whitespace at the left of the tableview
    if ([_menuItemsTableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [_menuItemsTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    _menuItemsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableItemsData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"menuTableMenuItem";
    
    MenuViewItemCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[MenuViewItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.cellMenuItem = [_tableItemsData objectAtIndex:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return MenuCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [[AFCListing sharedStore] addCartItem:[_tableItemsData objectAtIndex:indexPath.row]];
//    [self presentCartNotification];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MenuItemsOptionsViewController *miovc = [[MenuItemsOptionsViewController alloc] initWithMenuItem:[_tableItemsData objectAtIndex:indexPath.row]];
    [[self mainNavigationController] pushViewController:miovc animated:YES];
}

@end
