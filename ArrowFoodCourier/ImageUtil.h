

#import <Foundation/Foundation.h>

@interface ImageUtil : NSObject

+ (UIImageView*) viewWithImageName:(NSString*)name andPos:(CGPoint)position;
+ (UIImageView*) viewWithImageName:(NSString*)name andCenter:(CGPoint)position;

@end
