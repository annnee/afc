

#import "AFCMenuItemOption.h"

@implementation AFCMenuItemOption
{
    NSMutableArray *_parameters;
}

@synthesize name = _name;
@synthesize description = _description;
@synthesize type = _type;
@synthesize selectedParam = _selectedParam;

- (id)initWithName:(NSString *)optionName description:(NSString *)optionDescription type:(NSString *)optionType params:(NSString *)optionParams
{
    self = [super init];
    if (self)
    {
        _name = optionName;
        _description = optionDescription;
        _type = [self parseOptionType:optionType];
        _typeString = optionType;
        _parameters = [self parseFormParams:optionParams];
        _selectedParam = [_parameters objectAtIndex:0];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        _name = [dictionary objectForKey:@"name"];
        _description = [dictionary objectForKey:@"description"];
        _type = [self parseOptionType:[dictionary objectForKey:@"type"]];
        _typeString = [dictionary objectForKey:@"type"];
        _parameters = [self parseFormParams:[dictionary objectForKey:@"param"]];
        _selectedParam = [_parameters objectAtIndex:0];
    }
    return self;
}

- (NSArray *)allParameters
{
    return _parameters;
}

- (FormType)parseOptionType:(NSString *)type
{
    if ([type.uppercaseString isEqualToString:@"CHECK_ONE"] || [type.uppercaseString isEqualToString:@"SELECT"])
    {
        return CHECK_ONE;
    }
    else if([type.uppercaseString isEqualToString:@"CHECKBOX"])
    {
        return CHECKBOX;
    }
    else
    {
        return FREEFORM;
    }
}

- (NSMutableArray *)parseFormParams:(NSString *)params
{
    NSArray *a = (NSArray *)[params componentsSeparatedByString:@","];
    NSMutableArray *mA = [NSMutableArray arrayWithArray:a];
    
    for(int i = 0; i < a.count; i++)
    {
        if([[a[i] uppercaseString] isEqualToString:@"TRUE"])
            [mA setObject:@"1" atIndexedSubscript:i];
        if([[a[i] uppercaseString] isEqualToString:@"FALSE"])
            [mA setObject:@"0" atIndexedSubscript:i];
    }
    return mA;
}

-(NSDictionary *)dictionary
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
    [d setObject:_name forKey:@"name"];
    [d setObject:_description forKey:@"description"];
    [d setObject:_typeString forKey:@"type"];
    [d setObject:_selectedParam forKey:@"param"];
    
    return d;
}

@end
