

#import <Foundation/Foundation.h>

@interface AFCCart : NSObject
{
    NSMutableArray *_allCartItems;
}

@property (nonatomic, strong) NSArray *cartItems;

- (id)initFromDictionary:(NSDictionary*)info;

- (void)addCartItem:(AFCCartItem*)cartItem;
- (BOOL)removeCartItem:(AFCCartItem *)p;

@end
