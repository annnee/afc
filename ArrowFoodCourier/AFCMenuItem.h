

#import <Foundation/Foundation.h>
#import "AFCMenuItemOption.h"

@interface AFCMenuItem : NSObject
{
    NSMutableArray *options;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) UIImage *image;
@property float price;

- (id)initWithName:(NSString *)itemName description:(NSString *)itemDescription price:(float)itemPrice;
- (id)initWithName:(NSString *)itemName description:(NSString *)itemDescription price:(float)itemPrice image:(UIImage *)itemImage;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (void)addOption:(AFCMenuItemOption *)option;

- (NSArray *)allOptions;
- (NSArray*)allOptionsDictionary;

@end
