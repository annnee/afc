 

#import "OrderStatusTableViewCell.h"

@implementation OrderStatusTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [[self orderTitle] setFont:[UIFont fontWithName:HEADER_FONT size:18]];
    [[self orderStatus] setFont:[UIFont fontWithName:BODY_FONT size:12]];
    [[self orderPrice] setFont:[UIFont fontWithName:HEADER_FONT size:18]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
