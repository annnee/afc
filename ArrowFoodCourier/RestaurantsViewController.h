 

#import <UIKit/UIKit.h>
#import "AFCViewController.h"
#import "RestaurantsSecondaryNavBar.h"

@interface RestaurantsViewController : AFCViewController<UITableViewDataSource, UITableViewDelegate, RestaurantsSecondaryNavBarDelegate>

@end
