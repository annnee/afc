
#import <Foundation/Foundation.h>
#import "AFCMenuCAtegory.h"

@interface AFCRestaurant : NSObject
{
    NSMutableArray *menuCategories;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *detailedDescription;
@property (nonatomic, strong) UIImage *storefrontImage;
@property float rating;

- (id)initWithName:(NSString *)restaurantName
           address:(NSString *)restaurantAddress
       phoneNumber:(NSString *)restaurantPhoneNumber
  shortDescription:(NSString *)restaurantShortDescription
detailedDescription:(NSString *)restaurantDetailedDescription
   storefrontImage:(UIImage *)restaurantStorefrontImage
            rating:(float)restaurantRating;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (void)addMenuCategory:(AFCMenuCategory *)category;
- (NSArray *)allMenuCategories;

@end
