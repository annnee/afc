

#import "ProfileView.h"
#import "AFCListing.h"
#import "AFCUser.h"

@implementation ProfileView {
    
    UILabel *userName;
}


@synthesize username = _username;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setUpView];
    }
    return self;
}

- (void)setUpView
{
    
//    UIImageView* circle = [ImageUtil viewWithImageName:@"UserIconBackground" andPos:CGPointMake(self.frame.size.width/2-35, 20)];
//    [circle setCenter:CGPointMake(self.frame.size.width/2, circle.center.y)];
//    [self addSubview:circle];
//    
//    // The AFC icon
//    UIImageView *icon = [ImageUtil viewWithImageName:@"UserIcon" andCenter:circle.center];
//    [self addSubview:icon];
//    
    self.backgroundColor = [UIColor colorWithRed:0.871 green:0.871 blue:0.871 alpha:1.0];
    
    userName = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, self.frame.size.width, 50)];
    userName.font = [UIFont fontWithName:HEADER_FONT size:20];
    userName.text = [[[AFCListing sharedStore]user]username];
    userName.textAlignment = NSTextAlignmentCenter;
    [userName setCenter:CGPointMake(self.frame.size.width / 2, 110)];

}

@end