 

#import "CartViewController.h"
#import "AFCListing.h"
#import "AFCMenuItem.h"
#import "CartViewCell.h"
#import "CartViewFooter.h"

@interface CartViewController ()

@end

@implementation CartViewController
{
    UITableView *_cartTableView;    
    NSArray *_tableData;
    
    int viewAppearedCount;
    
    BOOL _cellDeleteShowing;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super init];
    if (self)
    {
        self.view.frame = frame;
        _cellDeleteShowing = NO;
    }
    return self;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        viewAppearedCount = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupView];
}

- (void)setupView
{
    // Add table view controller
    if(viewAppearedCount == 1)
    {
        [self createTableView];
    }
    viewAppearedCount++;
}

- (void)createTableView
{
    _tableData = [(AFCCart*)[[AFCListing sharedStore] cart] cartItems];
    _cartTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    _cartTableView.delegate = self;
    _cartTableView.dataSource = self;
    _cartTableView.autoresizesSubviews = YES;
    [_cartTableView setBackgroundColor:[UIColor colorFromHexValue:0xF0F0F0]];
    
    [self.view addSubview:_cartTableView];
    
    // Push the table view to be below the secondary nav bar
    //[self.view sendSubviewToBack:_cartTableView];
    
    // Remove the extra whitespace at the left of the tableview
    if ([_cartTableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [_cartTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    _cartTableView.tableFooterView = [[CartViewFooter alloc] initWithFrame:(CGRectMake(0, 0, self.view.frame.size.width, CartFooterHeight))];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"cartTableItem";
    
    CartViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[CartViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellDeleteShow:)];
        swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellDeleteHide:)];
        swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellDelete:)];
        
        [cell addGestureRecognizer:swipeLeft];
        [cell addGestureRecognizer:swipeRight];
        [cell addGestureRecognizer:tap];
    }
    [cell setFrame:CGRectMake(0, 0, _cartTableView.frame.size.width, cell.frame.size.height)];

    cell.cellRestaurant = [(AFCCartItem*)[_tableData objectAtIndex:indexPath.row] restaurant];
    cell.cellMenuCategory = [(AFCCartItem*)[_tableData objectAtIndex:indexPath.row] menuCategory];
    cell.cellMenuItem = [(AFCCartItem*)[_tableData objectAtIndex:indexPath.row] menuItem];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CartCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Gesture Recognizers

- (void)cellDeleteShow:(UIGestureRecognizer*)recognizer
{
    CartViewCell *cell = (CartViewCell*)recognizer.view;
    [cell showDeleteView];
}

- (void)cellDeleteHide:(UIGestureRecognizer*)recognizer
{
    CartViewCell *cell = (CartViewCell*)recognizer.view;
    [cell hideDeleteView];
}

- (void)cellDelete:(UIGestureRecognizer*)recognizer
{
    CartViewCell *cell = (CartViewCell*)recognizer.view;
    CGPoint point = [recognizer locationInView:cell];
    if([cell locationHitsDeleteView:point])
    {
        [self deleteCell:cell];
    }
    else
    {
        [cell toggleVisibiltyOfDeleteView];
    }
}

- (void)deleteCell:(CartViewCell*)cell
{
    [self startLoading];
    [AFCRepository deleteItemFromCartWithRestaurant:cell.cellRestaurant menu:cell.cellMenuCategory item:cell.cellMenuItem.name quantity:1 responseHandler:^(NSString *error)
    {
        if(!error)
        {
            NSIndexPath *path = [_cartTableView indexPathForCell:cell];
            if(path)
            {
                AFCMenuItem *item = cell.cellMenuItem;
                AFCCartItem *cartItem = [[AFCCartItem alloc] initWithMenuItem:item category:cell.cellMenuCategory andRestaurant:cell.cellRestaurant];
                if([[[AFCListing sharedStore] cart] removeCartItem:cartItem])
                {
                    _tableData = [[[AFCListing sharedStore] cart] cartItems];
                    [_cartTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:path] withRowAnimation:UITableViewRowAnimationLeft];
                    CartViewFooter *footer = (CartViewFooter*)_cartTableView.tableFooterView;
                    [footer update];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"CartItemDeleted" object:nil];
                }
            }
        }
        else
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Cart Error"
                                                         message:error
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
            [av show];
        }
        [self stopLoading];
    }];
    
    
}

@end
