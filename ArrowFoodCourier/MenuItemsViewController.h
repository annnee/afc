 

#import "AFCViewController.h"
#import "WYPopoverController.h"
#import "AFCMenuCategory.h"

@interface MenuItemsViewController : AFCViewController<UITableViewDataSource, UITableViewDelegate>

- (id)initWithCategory:(AFCMenuCategory *)category;

@end
