

#import "RestaurantsSecondaryNavBar.h"
#import <QuartzCore/QuartzCore.h>

@implementation RestaurantsSecondaryNavBar
{
    NSArray *_buttons;
    NSArray *_filterOptions;
    NSArray *_sortOptions;
    
    NSMutableArray *_filterButtons;
    NSMutableArray *_sortButtons;
    
    UITextField *_searchTextView;
    
    UIButton *_searchButton;
}

#pragma mark - Properties

@synthesize delegate = _delegate;
@synthesize filterView = _filterView;
@synthesize sortView = _sortView;
@synthesize searchView = _searchView;

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialize private variables
        _filterOptions = [NSArray arrayWithObjects:@"All", @"Near Me", nil];
        _sortOptions = [NSArray arrayWithObjects:@"Distance", @"Name", nil];
        
        // Initialization code
        [self setupView];
        [self createDropDownViews];
        
    }
    return self;
}

- (id)initWithPosition:(CGPoint)position
{
    return [self initWithFrame:CGRectMake(position.x, position.y, [[UIScreen mainScreen] bounds].size.width, SecondaryNavBarHeight)];
}

-(void)dealloc
{
    self.delegate = nil;
    _searchTextView.delegate = nil;
    
    NSLog(@"%@ dealloc.", [self class]);
}

#pragma mark - View Creation

- (void)setupView
{
    // View
    [self setBackgroundColor:[UIColor colorFromHexValue:AFCRed]];
    
    // Separators
    UIView *s1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, SecondaryNavBarSeparatorThickness)];
    [s1 setBackgroundColor:[UIColor colorFromHexValue:DarkRed]];
    
    int xLoc = self.frame.size.width / NumberOfSecondaryBarItems;
    int yLoc = (self.frame.size.height - SecondaryNavBarSeparatorHeight) / 2;
    
    UIView *s2 = [[UIView alloc] initWithFrame:CGRectMake(xLoc, yLoc, SecondaryNavBarSeparatorThickness, SecondaryNavBarSeparatorHeight)];
    [s2 setBackgroundColor:[UIColor colorFromHexValue:DarkRed]];
    UIView *s3 = [[UIView alloc] initWithFrame:CGRectMake(xLoc*2, yLoc, SecondaryNavBarSeparatorThickness, SecondaryNavBarSeparatorHeight)];
    [s3 setBackgroundColor:[UIColor colorFromHexValue:DarkRed]];
    
    [self addSubview:s1];
    [self addSubview:s2];
    [self addSubview:s3];
    
    // Add buttons
    UIButton *button1 = [[UIButton alloc] initWithFrame:CGRectMake(-2, -1, xLoc+5,
                                                                   self.frame.size.height+2.5)];
    [button1 setBackgroundColor:[UIColor clearColor]];
    [button1 setTitle:@"FILTER" forState:UIControlStateNormal];
    [button1.titleLabel setFont:[UIFont fontWithName:BODY_FONT_BOLD size:12]];
    [button1 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateHighlighted | UIControlStateSelected)];
    [button1 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateSelected)];
    [button1 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateHighlighted)];
    [button1 addTarget:self action:@selector(filterPressed:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:button1];
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(xLoc-2, -1, xLoc+5,
                                                                   self.frame.size.height+2.5)];
    [button2 setBackgroundColor:[UIColor clearColor]];
    [button2 setTitle:@"SORT" forState:UIControlStateNormal];
    [button2.titleLabel setFont:[UIFont fontWithName:BODY_FONT_BOLD size:12]];
    [button2 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateHighlighted | UIControlStateSelected)];
    [button2 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateSelected)];
    [button2 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateHighlighted)];
    [button2 addTarget:self action:@selector(sortPressed:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:button2];
    
    UIButton *button3 = [[UIButton alloc] initWithFrame:CGRectMake(xLoc*2-2, -1, xLoc+5,
                                                                   self.frame.size.height+2.5)];
    [button3 setBackgroundColor:[UIColor clearColor]];
    [button3 setTitle:@"SEARCH" forState:UIControlStateNormal];
    [button3.titleLabel setFont:[UIFont fontWithName:BODY_FONT_BOLD size:12]];
    [button3 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateHighlighted | UIControlStateSelected)];
    [button3 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateSelected)];
    [button3 setBackgroundImage:[UIImage imageNamed:@"ButtonPressed"] forState:(UIControlStateHighlighted)];
    [button3 addTarget:self action:@selector(searchPressed:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:button3];
    _searchButton = button3;
    
    _buttons = [NSArray arrayWithObjects:button1, button2, button3, nil];
    
    // Set drop shadow
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0, 2);
    self.layer.shadowRadius = 1;
    self.layer.shadowOpacity = 0.7;
    
}

- (void)createDropDownViews
{
    // Arrays that will hold the selectable options in the filter and sort drop downs
    _filterButtons = [[NSMutableArray alloc] init];
    _sortButtons = [[NSMutableArray alloc] init];
    
    // The height each button will be
    int segmentHeight = 38;
    
    // Create the filter dropdown
    _filterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, [[self filterOptions] count] * segmentHeight)];
    _filterView.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [_filterView setCenter:CGPointMake(_filterView.center.x, -_filterView.frame.size.height/2)];
    
    int xPos = 30;
    int yPos = 0;
    for(NSString *s in [self filterOptions])
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, _filterView.frame.size.width, segmentHeight)];
        label.textColor = [UIColor whiteColor];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setFont:[UIFont fontWithName:BODY_FONT size:14]];
        label.text = s;
        [_filterView addSubview:label];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, yPos, _filterView.frame.size.width, segmentHeight)];
        [button addTarget:self action:@selector(filterOptionChosen:) forControlEvents:UIControlEventTouchDown];
        [button setImage:[UIImage imageNamed:@"SelectButton"] forState:(UIControlStateHighlighted | UIControlStateSelected)];
        [button setImage:[UIImage imageNamed:@"SelectButton"] forState:(UIControlStateSelected)];
        [button setContentMode:UIViewContentModeCenter];
        button.titleLabel.text = label.text;
        button.titleLabel.textColor = [UIColor clearColor];
        
        // Select the default value (the first one in the array).
        if(yPos == 0) button.selected = YES;
        
        [_filterView addSubview:button];
        [_filterButtons addObject:button];
        
        yPos += segmentHeight;
    }
    
    // Create the sort drop down
    _sortView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, [[self sortOptions] count] * segmentHeight)];
    _sortView.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [_sortView setCenter:CGPointMake(_sortView.center.x, -_sortView.frame.size.height/2)];
    
    yPos = 0;
    for(NSString *s in [self sortOptions])
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, _sortView.frame.size.width, segmentHeight)];
        label.textColor = [UIColor whiteColor];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setFont:[UIFont fontWithName:BODY_FONT size:14]];
        label.text = s;
        [_sortView addSubview:label];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, yPos, _sortView.frame.size.width, segmentHeight)];
        [button addTarget:self action:@selector(sortOptionChosen:) forControlEvents:UIControlEventTouchDown];
        [button setImage:[UIImage imageNamed:@"SelectButton"] forState:(UIControlStateHighlighted | UIControlStateSelected)];
        [button setImage:[UIImage imageNamed:@"SelectButton"] forState:(UIControlStateSelected)];
        [button setContentMode:UIViewContentModeCenter];
        button.titleLabel.text = label.text;
        button.titleLabel.textColor = [UIColor clearColor];
        
        // Select the default value (the first one in the array).
        if(yPos == 0) button.selected = YES;
        
        [_sortView addSubview:button];
        [_sortButtons addObject:button];
        
        yPos += segmentHeight;
    }
    
    // Create the search drop down
    _searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, segmentHeight)];
    _searchView.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [_searchView setCenter:CGPointMake(_searchView.center.x, -_searchView.frame.size.height/2)];
    
    UIImage *clearSearchImage = [UIImage imageNamed:@"QuitSearch"];
    UIButton *clearSearchButton = [[UIButton alloc] initWithFrame:CGRectMake(10, (_searchView.frame.size.height - clearSearchImage.size.height) /2,
                                                                             clearSearchImage.size.width, clearSearchImage.size.height)];
    [clearSearchButton setBackgroundImage:clearSearchImage forState:UIControlStateNormal];
    [clearSearchButton addTarget:self action:@selector(clearSearchPressed) forControlEvents:UIControlEventTouchUpInside];
    [_searchView addSubview:clearSearchButton];
    
    _searchTextView = [[UITextField alloc] initWithFrame:CGRectMake(clearSearchButton.frame.size.width+12, (_searchView.frame.size.height - 20) / 2, 226, 20)];
    [_searchTextView setBackgroundColor:[UIColor whiteColor]];
    _searchTextView.layer.cornerRadius = 2;
    _searchTextView.keyboardType = UIKeyboardTypeAlphabet;
    [_searchTextView setReturnKeyType:UIReturnKeySearch];
    _searchTextView.delegate = self;
    
    [_searchView addSubview:_searchTextView];
}

#pragma mark - Selectors

- (void)filterPressed:(UIButton*)sender
{
    // Toggle the selected button
    for(UIButton *b in _buttons)
    {
        if(b != sender) b.selected = NO;
    }
    sender.selected = !sender.selected;
    
    if(_delegate && [_delegate respondsToSelector:@selector(filterButtonPressed:)])
    {
        [_delegate filterButtonPressed:sender];
    }
}

- (void)sortPressed:(UIButton*)sender
{
    // Toggle the selected button
    for(UIButton *b in _buttons)
    {
        if(b != sender) b.selected = NO;
    }
    sender.selected = !sender.selected;
    
    if(_delegate && [_delegate respondsToSelector:@selector(sortButtonPressed:)])
    {
        [_delegate sortButtonPressed:sender];
    }
}

-(void)searchPressed:(UIButton*)sender
{
    // Toggle the selected button
    for(UIButton *b in _buttons)
    {
        if(b != sender) b.selected = NO;
    }
    sender.selected = !sender.selected;
    
    if(_delegate && [_delegate respondsToSelector:@selector(searchButtonPressed:)])
    {
        [_delegate searchButtonPressed:sender];
    }
}

- (void)filterOptionChosen:(UIButton*)sender
{
    for(UIButton* b in _filterButtons)
    {
        b.selected = NO;
    }
    sender.selected = YES;
    
    if(_delegate && [_delegate respondsToSelector:@selector(filterOptionSelected:)])
    {
        [_delegate filterOptionSelected:sender.titleLabel.text];
    }
}

- (void)sortOptionChosen:(UIButton*)sender
{
    for(UIButton* b in _sortButtons)
    {
        b.selected = NO;
    }
    sender.selected = YES;
    
    if(_delegate && [_delegate respondsToSelector:@selector(sortOptionSelected:)])
    {
        [_delegate sortOptionSelected:sender.titleLabel.text];
    }
}

- (void)clearSearchPressed
{
    // The user could be trying to close the search view, so
    // if the text field is already empty and they are still hitting the X button
    // then close the keyboard
    if(_searchTextView.text.length == 0)
    {
        [self searchPressed:_searchButton];
    }
    
    [_searchTextView setText:@""];
}

#pragma mark - UITextField Delegates

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(self.delegate && [self .delegate respondsToSelector:@selector(searchTextChanged:)])
    {
        [self.delegate searchTextChanged:string];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return NO;
}

#pragma mark - Data

-(NSArray *)filterOptions
{
    return _filterOptions;
}

-(NSArray *)sortOptions
{
    return _sortOptions;
}

#pragma mark - Other

-(void)resignSearchBarResponder
{
    [_searchTextView resignFirstResponder];
}

-(void)makeSearchBarFirstResponder
{
    [_searchTextView becomeFirstResponder];
}


@end
