 

#import "ProfileViewController.h"
#import "ProfileView.h"
#import "MenuCategoriesViewController.h"
#import "AFCRestaurantsTableViewCell.h"
#import "ProfileDetailHeader.h"

@interface ProfileViewController ()

@end

enum
{
    fname = 1000,
    lname,
    username,
    email,
    password,
    confirmpassword,
    line1 = 2000,
    line2,
    city,
    state,
    zip,
    defaultBill,
    defaultShip,
    phoneNumber = 3000,
    phoneType,
    defaultPhone
    
} Tags;

@implementation ProfileViewController
{
    NSMutableArray *_allFields;

    UIButton* _saveButton;
    
    UITableView *_profileTableView;
    int _numberOfAddresses;
    int _numberOfPhones;
    int _totalNumberOfRows;
    int _cellHeight;
    
    UITextField *_activeField;
    AFCUser *_user;
    
    NSString *_oldPassword;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupView];
    [AFCRepository getProfileWithResponseHandler:^(NSString *error, NSDictionary *info)
    {
        [AFCListing sharedStore].user = _user = [[AFCUser alloc] initFromDictionary:info];
        [self updateTableInformation];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupView
{
    self.view = [[ProfileView alloc] initWithFrame:self.view.frame];
    
    // Define the navigation title for the AFCNavigationController so that it will
    // be displayed in the top bar.
    self.navigationItem.title = @"PROFILE";
    
    // Add icon items to the top bar
    [self addHomeIcon];
    [self addCartButtonIcon];
    
    [self registerForKeyboardNotifications];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] setDrawerGesturesEnabled:NO];
    
    ProfileView* selfView = (ProfileView*)self.view;
    
    _user = [[AFCListing sharedStore] user];
    
    _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _saveButton.frame = CGRectMake(0, 0, 150, 30);
    CALayer *btnLayer = [_saveButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
    [_saveButton setTitle:@"Save" forState:UIControlStateNormal];
    _saveButton.backgroundColor = [UIColor whiteColor];
    [_saveButton addTarget:self action:@selector(saveProfile:) forControlEvents:UIControlEventTouchUpInside];
    [[_saveButton titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
    [_saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [selfView addSubview:_saveButton];
    
    _allFields = [[NSMutableArray alloc] init];
    _numberOfAddresses = 1;
    _numberOfPhones = 1;
    _cellHeight = 35;
    _profileTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))
                                                           style:UITableViewStyleGrouped];
    _profileTableView.delegate = self;
    _profileTableView.dataSource = self;
    _profileTableView.backgroundColor = [UIColor colorWithRed:0.871 green:0.871 blue:0.871 alpha:1];
    _profileTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_profileTableView.frame), CGRectGetHeight(_saveButton.frame) + 20*2)];
    [_profileTableView.tableFooterView addSubview:_saveButton];
    [_saveButton setCenter:CGPointMake(CGRectGetWidth(_profileTableView.frame) / 2.0, CGRectGetHeight(_profileTableView.tableFooterView.frame) / 2.0)];
    // Remove the extra whitespace at the left of the tableview
    if ([_profileTableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [_profileTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    [selfView addSubview:_profileTableView];
    
}

#pragma mark - Keyboard

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)notification
{
    UIScrollView *setUp = _profileTableView;
    
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height - (CGRectGetHeight(self.view.frame) -
                                                                             CGRectGetMaxY(_profileTableView.frame)), 0.0);
    setUp.contentInset = contentInsets;
    setUp.scrollIndicatorInsets = contentInsets;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIScrollView *setUp = _profileTableView;
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        setUp.contentInset = contentInsets;
        setUp.scrollIndicatorInsets = contentInsets;
    } completion:nil];
    
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark - Text Field

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeField = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self dismissKeyboard];
    return YES;
}

#pragma mark - TableView Datasource/Delegate

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return section == 0? @"Login Information" : section == 1? @"Delivery Address" : section == 2? @"Phone Number" : 0;
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *headerText = section == 0? @"Login Information" : section == 1? @"Delivery Address" : section == 2? @"Phone Number" : 0;
    ProfileDetailHeader *header = [[ProfileDetailHeader alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, DEFAULT_TABLE_CELL_HEIGHT)];
    [[header headerLabel] setText:headerText];
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return DEFAULT_TABLE_CELL_HEIGHT;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _cellHeight;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0? 5 : section == 1? 5*_numberOfAddresses : section == 2? 2*_numberOfPhones : 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    NSMutableArray *subViews = [[NSMutableArray alloc] init];
    int padding = 10;
    
    if(indexPath.section == 0)
    {
        UITextField *field;
        switch (indexPath.row)
        {
            case 0:
            {
                identifier = @"firstname";
                field = (UITextField*)[tableView viewWithTag:fname];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                    field.placeholder = @"First Name";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.tag = fname;
                    field.text = _user.firstName;
                }
                
                
                break;
            }
            case 1:
            {
                identifier = @"lastname";
                field = (UITextField*)[tableView viewWithTag:lname];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                    field.placeholder = @"Last Name";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.tag = lname;
                    field.text = _user.lastName;
                }
                
                break;
            }
            case 2:
            {
                identifier = @"emailaddress";
                field = (UITextField*)[tableView viewWithTag:email];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                    field.placeholder = @"Email Address";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.tag = email;
                    field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    field.autocorrectionType = UITextAutocorrectionTypeNo;
                    field.keyboardType = UIKeyboardTypeEmailAddress;
                    field.text = _user.email;
                }
                
                break;
            }
            case 3:
            {
                identifier = @"password";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Password";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.secureTextEntry = YES;
                field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                field.autocorrectionType = UITextAutocorrectionTypeNo;
                field.tag = password;
                break;
            }
            case 4:
            {
                identifier = @"confirmpassword";
                field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                field.placeholder = @"Confirm Password";
                field.backgroundColor = [UIColor whiteColor];
                field.borderStyle = UITextBorderStyleNone;
                field.secureTextEntry = YES;
                field.autocapitalizationType = UITextAutocapitalizationTypeNone;
                field.autocorrectionType = UITextAutocorrectionTypeNo;
                field.tag = confirmpassword;
                break;
            }
            default:
                break;
        }
        field.delegate = self;
        [subViews addObject:field];
    }
    else if(indexPath.section == 1)
    {
        int row = indexPath.row % 6;
        switch (row)
        {
            case 0:
            {
                identifier = [NSString stringWithFormat:@"line1%d", _numberOfAddresses];
                UITextField *field = (UITextField*)[tableView viewWithTag:line1];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                    field.placeholder = @"Address Line 1";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.tag = line1;
                    [subViews addObject:field];
                    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] line1];
                }
                
                break;
            }
            case 1:
            {
                identifier = [NSString stringWithFormat:@"line2%d", _numberOfAddresses];
                UITextField *field;
                field = (UITextField*)[tableView viewWithTag:line2];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                    field.placeholder = @"Address Line 2";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.tag = line2;
                    [subViews addObject:field];
                    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] line2];
                }
                
                break;
            }
            case 2:
            {
                identifier = [NSString stringWithFormat:@"citystate%d", _numberOfAddresses];
                UITextField *field;
                field = (UITextField*)[tableView viewWithTag:city];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width / 2 - padding / 2, _cellHeight)];
                    field.placeholder = @"City";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.keyboardType = UIKeyboardTypeAlphabet;
                    field.tag = city;
                    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] city];
                }
                
                
                UITextField *field2;
                field2 = (UITextField*)[tableView viewWithTag:state];
                if(!field2)
                {
                    field2 = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(field.frame) + padding / 2, 0, _profileTableView.frame.size.width / 2 - padding / 2, _cellHeight)];
                    field2.placeholder = @"State";
                    field2.backgroundColor = [UIColor whiteColor];
                    field2.borderStyle = UITextBorderStyleNone;
                    field2.tag = state;
                    field2.keyboardType = UIKeyboardTypeAlphabet;
                    field2.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] state];
                }
                
                
                [subViews addObject:field];
                [subViews addObject:field2];
                break;
            }
            case 3:
            {
                identifier = [NSString stringWithFormat:@"zip%d", _numberOfAddresses];
                UITextField *field;
                field = (UITextField*)[tableView viewWithTag:zip];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                    field.placeholder = @"Zip";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.tag = zip;
                    field.keyboardType = UIKeyboardTypeNumberPad;
                    [subViews addObject:field];
                    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] zip];
                }
                
                break;
            }
            case 4:
            {
                identifier = [NSString stringWithFormat:@"defaultBilling%d", _numberOfAddresses];
                UILabel *label;
                label = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                label.text = @"Use address for billing?";
                label.backgroundColor = [UIColor whiteColor];
                [subViews addObject:label];
                
                UISwitch *s = (UISwitch*)[tableView viewWithTag:defaultBill];
                if(!s)
                {
                    s = [[UISwitch alloc] init];
                    [s setFrame:CGRectMake(CGRectGetWidth(_profileTableView.frame) - CGRectGetWidth(s.frame) - padding, 0, 0, 0)];
                    s.center = CGPointMake(s.center.x, _cellHeight / 2.0);
                    s.tag = defaultBill;
                    [subViews addObject:s];
                    [s setOn:[(AFCAddress*)[_user.addresses objectAtIndex:0] defaultBilling]];
                }
                
                break;
            }
            default:
                break;
        }
    }
    else if(indexPath.section == 2)
    {
        int row = indexPath.row % 3;
        switch (row)
        {
            case 0:
            {
                identifier = [NSString stringWithFormat:@"number%d", _numberOfPhones];
                UITextField *field;
                field = (UITextField*)[tableView viewWithTag:phoneNumber];
                if(!field)
                {
                    field = [[UITextField alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                    field.placeholder = @"Phone Number";
                    field.backgroundColor = [UIColor whiteColor];
                    field.borderStyle = UITextBorderStyleNone;
                    field.tag = phoneNumber;
                    field.keyboardType = UIKeyboardTypePhonePad;
                    [subViews addObject:field];
                    field.text = [(AFCPhone*)[_user.phoneNumbers objectAtIndex:0] number];
                }
                
                break;
            }
            case 1:
            {
                identifier = [NSString stringWithFormat:@"name%d", _numberOfPhones];
                UILabel *label;
                label = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, _profileTableView.frame.size.width - padding, _cellHeight)];
                label.text = @"Mobile phone?";
                label.backgroundColor = [UIColor whiteColor];
                [subViews addObject:label];
                
                UISwitch *s = (UISwitch*)[tableView viewWithTag:phoneType];
                if(!s)
                {
                    s = [[UISwitch alloc] init];
                    [s setFrame:CGRectMake(CGRectGetWidth(_profileTableView.frame) - CGRectGetWidth(s.frame) - padding, 0, 0, 0)];
                    s.center = CGPointMake(s.center.x, _cellHeight / 2.0);
                    s.tag = phoneType;
                    [subViews addObject:s];
                    
                    AFCPhone *p = (AFCPhone*)[_user.phoneNumbers objectAtIndex:0];
                    BOOL isMobile = [[p type] isEqualToString:@"Mobile"];
                    [s setOn:isMobile];
                }
                
                break;
            }
            default:
                break;
        }
        
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if( cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        for(UIView *v in subViews)
        {
            if([v isKindOfClass:[UITextField class]])
            {
                ((UITextField*)v).delegate = self;
                [_allFields addObject:v];
            }
            if([v isKindOfClass:[UISwitch class]])
            {
                [_allFields addObject:v];
            }
            
            [cell.contentView addSubview:v];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void)updateTableInformation
{
    UITextField *field;
    UISwitch *s;
    field = (UITextField*)[_profileTableView viewWithTag:fname];
    field.text = _user.firstName;
    
    field = (UITextField*)[_profileTableView viewWithTag:lname];
    field.text = _user.lastName;
    
    field = (UITextField*)[_profileTableView viewWithTag:email];
    field.text = _user.email;
    
    field = (UITextField*)[_profileTableView viewWithTag:line1];
    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] line1];
    
    field = (UITextField*)[_profileTableView viewWithTag:line2];
    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] line2];
    
    field = (UITextField*)[_profileTableView viewWithTag:city];
    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] city];
    
    field = (UITextField*)[_profileTableView viewWithTag:state];
    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] state];
    
    field = (UITextField*)[_profileTableView viewWithTag:zip];
    field.text = [(AFCAddress*)[_user.addresses objectAtIndex:0] zip];
    
    s = (UISwitch*)[_profileTableView viewWithTag:defaultBill];
    [s setOn: [[_user.addresses objectAtIndex:0] defaultBilling]];
    
    field = (UITextField*)[_profileTableView viewWithTag:phoneNumber];
    field.text = [(AFCPhone*)[_user.phoneNumbers objectAtIndex:0] number];
    
    s = (UISwitch*)[_profileTableView viewWithTag:phoneType];
    [s setOn: [[(AFCPhone*)[_user.phoneNumbers objectAtIndex:0] type] isEqualToString:@"Mobile"]];
}


#pragma mark - Create Account
#define emailRegex @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"

- (BOOL)errorCheck
{
    NSString *errorString;
    
    for(UIView *view in _allFields)
    {
        if([view isKindOfClass:[UITextField class]] && view.tag != line2 && view.tag != password && view.tag != confirmpassword)
        {
            UITextField *f = (UITextField*)view;
            if(f.text.length == 0)
            {
                errorString = @"Please fill out all fields";
                
            }
        }
    }
    if(!errorString)
    {
        UITextField *emailField = (UITextField*)[_profileTableView viewWithTag:email];
        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:emailRegex options:NSRegularExpressionCaseInsensitive error:NULL];
        NSUInteger emailMatches = [expression numberOfMatchesInString:[self trimmedString:emailField.text] options:0 range:NSMakeRange(0, [[self trimmedString:emailField.text] length])];
        
        if(emailMatches == 0)
        {
            errorString = @"Please enter a valid email address";
        }
    }
    if(!errorString)
    {
        UITextField *passwordField = (UITextField*)[_profileTableView viewWithTag:password];
        UITextField *confirmPasswordField = (UITextField*)[_profileTableView viewWithTag:confirmpassword];
        
        if(passwordField.text.length > 0 && ![[self trimmedString:passwordField.text] isEqualToString:[self trimmedString:confirmPasswordField.text]])
        {
            errorString = @"Passwords do not match";
        }
    }
    
    if(errorString)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Profile update error"
                                                         message:errorString
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        [alert show];
        return NO;
    }
    return YES;
}

// Trim leading and trailing whitespace
- (NSString*)trimmedString:(NSString*)string
{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (void)saveProfile:(UIButton*)button
{
    NSLog(@"Update Profile");
    [self.view endEditing:YES];
    [self startLoading];
    
    if([self errorCheck])
    {
        [self updateProfileInformation];
    }
    else
    {
        [self stopLoading];
    }
        
}

- (void)updatePassword
{
    // Make request
    NSString *pword = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:password]).text];
    
    // Change password first
    if(pword.length > 0)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Change Profile Password" message:@"Please enter your current password to change your profile password."
                                                   delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Update", nil];
        av.alertViewStyle = UIAlertViewStylePlainTextInput;
        av.tag = 100;
        av.delegate = self;
        [av show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex == 1)
    {
        UITextField *textField = (UITextField*)[alertView textFieldAtIndex:0];
        NSString *pword = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:password]).text];
        
        [AFCRepository changePasswordWithOldPassword:textField.text newPassword:pword responseHandler:^(NSString *error)
         {
             if(!error)
             {
                 NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
                 [_profileTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
                 [self stopLoading];
             }
             else
             {
                 UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Change password error"
                                                                  message:error
                                                                 delegate:self
                                                        cancelButtonTitle:@"Ok"
                                                        otherButtonTitles: nil];
                 [alert show];
                 [self stopLoading];
             }
         }];
    }
}

- (void)updateProfileInformation
{
    NSString *firstname = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:fname]).text];
    NSString *lastname = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:lname]).text];
    NSString *name = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
    NSString *usernameString = [_user username];
    NSString *emailAddress = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:email]).text];
    
    NSString *l1 = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:line1]).text];
    NSString *l2 = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:line2]).text];
    NSString *cityString = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:city]).text];
    NSString *stateString = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:state]).text];
    NSString *zipString = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:zip]).text];
    BOOL defaultShip = YES;
    BOOL defaultBilling = ((UISwitch*)[_profileTableView viewWithTag:defaultBill]).isOn;
    
    NSString *phoneNumberString = [self trimmedString:((UITextField*)[_profileTableView viewWithTag:phoneNumber]).text];
    BOOL defaultNum = YES;
    NSString *type = ((UISwitch*)[_profileTableView viewWithTag:phoneType]).isOn? @"Mobile" : @"Home";
    
    AFCAddress *address = [[AFCAddress alloc] initWithLine1:l1 line2:l2 city:cityString state:stateString zip:zipString
                                             defaultBilling:defaultBilling defaultShipping:defaultShip];
    AFCPhone *phone = [[AFCPhone alloc] initWithType:type number:phoneNumberString defaultNumber:defaultNum];
    
    [AFCRepository createAccountWithUsername:usernameString password:nil email:emailAddress name:name
                                   addresses:[NSArray arrayWithObject:address.dictionary]
                                      phones:[NSArray arrayWithObject:phone.dictionary] responseHandler:^(NSString *error)
     {
         if(!error)
         {
             [AFCRepository getProfileWithResponseHandler:^(NSString *error, NSDictionary *info)
              {
                  if(!error)
                  {
                      [AFCListing sharedStore].user = _user = [[AFCUser alloc] initFromDictionary:info];
                      [self updateTableInformation];
                      [self updatePassword];
                  }
                  else
                  {
                      UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Profile update error"
                                                                       message:error
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles: nil];
                      [alert show];
                  }
                  _saveButton.enabled = YES;
                  [self.view setUserInteractionEnabled:YES];
                  [self stopLoading];
              }];
         }
         else
         {
             UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Profile update error"
                                                              message:error
                                                             delegate:self
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles: nil];
             [alert show];
             _saveButton.enabled = YES;
             [self.view setUserInteractionEnabled:YES];;
             [self stopLoading];
         }
     }];
}

@end
