

#import "CreateAccountView.h"
#import "AFCNavigationController.h"
#import "AFCListing.h"
#import "AFCUser.h"
#import "RestaurantsView.h"
#import "RestaurantsViewController.h"
#import "LogOnViewController.h"

@interface CreateAccountView ()

@end

@implementation CreateAccountView
{
    
    UIScrollView *setUp;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    
    return self;
    
}

- (AFCNavigationController*) navigationController
{
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate] rootViewController];
}

@end
