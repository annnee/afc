 

#import <UIKit/UIKit.h>

@interface AFCNavigationController : UINavigationController

- (void)setDropShadowHidden:(BOOL)hidden;

- (void)moveToHomeController:(UIViewController*)viewController;

@end
