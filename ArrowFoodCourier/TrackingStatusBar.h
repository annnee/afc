 
#import <UIKit/UIKit.h>


@interface TrackingStatusBar : UIView

- (id)initWithPosition:(CGPoint)point andWidth:(int)width andCircleDiameter:(int)diameter andStatuses:(NSArray*)statuses;

@property (nonatomic, strong) NSString *status;
@property (nonatomic) BOOL enabled;

@end
