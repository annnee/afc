

#import "AFCCartItem.h"

@implementation AFCCartItem

@synthesize menuItem = _menuItem;
@synthesize menuCategory = _menuCategory;
@synthesize restaurant = _restaurant;

-(id)initWithDictionary:(NSDictionary *)info
{
    self = [super init];
    if(self)
    {
        NSMutableDictionary *mItem = [[NSMutableDictionary alloc] init];
        [info objectForKey:@"item"]? [mItem setObject:[info objectForKey:@"item"] forKey:@"name"] :nil;
        [info objectForKey:@"itemOptions"]? [mItem setObject:[info objectForKey:@"itemOptions"] forKey:@"itemOptions"] : nil;
        [info objectForKey:@"description"]? [mItem setObject:[info objectForKey:@"description"] forKey:@"description"] : nil;
        [info objectForKey:@"total"]? [mItem setObject:[info objectForKey:@"total"] forKey:@"price"] : nil;
        
        self.menuItem = [[AFCMenuItem alloc] initWithDictionary:mItem];
        self.menuCategory = [info objectForKey:@"menu"];
        self.restaurant = [info objectForKey:@"restaurant"];
    }
    return self;
}

-(id)initWithMenuItem:(AFCMenuItem *)item category:(NSString *)category andRestaurant:(NSString *)restaurant
{
    self = [super init];
    if(self)
    {
        self.menuItem = item;
        self.menuCategory = category;
        self.restaurant = restaurant;
    }
    return self;
}

@end
