 

#import "AFCViewController.h"
#import "PayPalMobile.h"

@interface CheckOutViewController : AFCViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, PayPalPaymentDelegate>

@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;

@end
