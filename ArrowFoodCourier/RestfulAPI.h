
#import <Foundation/Foundation.h>

typedef enum
{
    RequestTypeGET,
    RequestTypePOST,
    RequestTypeDELETE
} RequestType;

@interface RestfulAPI : NSObject

+ (void)makeRequest:(RequestType)type
        withService:(NSString*)service
               body:(id)body
         andHandler:(void (^)(int statusCode, NSDictionary* responseDictionary, NSArray *responseArray, NSError *error))callback;

@end
