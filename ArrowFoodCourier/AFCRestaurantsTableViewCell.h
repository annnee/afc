

#import <UIKit/UIKit.h>
#import "AFCRestaurant.h"

@interface AFCRestaurantsTableViewCell : UITableViewCell

@property (nonatomic, strong) AFCRestaurant *cellRestaurant;

@end
