 

#import <UIKit/UIKit.h>

@interface OrderStatusTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderTitle;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property (weak, nonatomic) IBOutlet UILabel *orderPrice;

@end
