

#import "LogOnView.h"
#import "AFCNavigationController.h"
#import "CreateAccountView.h"
#import "CreateAccountViewController.h"

@interface LogOnView ()

@end

@implementation LogOnView {

    UITextField *username;
    UITextField *password;
}

@synthesize setUp = _setUp;
@synthesize username = _username;
@synthesize password = _password;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        // Custom initialization
        _setUp = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];

        [self setUpView];
        [self registerForKeyboardNotifications];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(dismissKeyboard)];
        
        [self addGestureRecognizer:tap];
        
    }
    return self;
}

-(void)dealloc
{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setUpView
{
    self.backgroundColor = [UIColor colorWithRed:0.871 green:0.871 blue:0.871 alpha:1];
    
    // The white circle behind the red icon
    UIImageView* circle = [ImageUtil viewWithImageName:@"UserIconBackground" andPos:CGPointMake(self.frame.size.width/2-35, 40)];
    [circle setCenter:CGPointMake(self.frame.size.width/2, circle.center.y)];
    [_setUp addSubview:circle];
    
    // The AFC icon
    UIImageView *icon = [ImageUtil viewWithImageName:@"UserIcon" andCenter:circle.center];
    [_setUp addSubview:icon];
    
    //To be used to change the preview text inside the fields.
    
    //    UIFont* textPreview = [UIFont fontWithName:HEADER_FONT size:10];
    //    NSDictionary* style = @{
    //                            NSForegroundColorAttributeName: textPreview,
    //                            };
    //
    //    NSAttributedString* usernameText = [[NSAttributedString alloc] initWithString:@"Username"
    //                                                                   attributes:style];
    //    NSAttributedString* passwordText = [[NSAttributedString alloc] initWithString:@"Username"
    //                                                                       attributes:style];
    
    
    username = [[UITextField alloc] initWithFrame:CGRectMake(20, 150, 280, 35)];
    username.Placeholder = @"Username";
    username.backgroundColor = [UIColor whiteColor];
    username.borderStyle = UITextBorderStyleRoundedRect;
    username.autocapitalizationType = UITextAutocapitalizationTypeNone;
    username.autocorrectionType = UITextAutocorrectionTypeNo;
    [_setUp addSubview:username];
    _username = username;
    
    password = [[UITextField alloc] initWithFrame:CGRectMake(20, 195, 280, 35)];
    password.Placeholder = @"Password";
    password.backgroundColor = [UIColor whiteColor];
    password.borderStyle = UITextBorderStyleRoundedRect;
    password.secureTextEntry = YES;
    password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    password.autocorrectionType = UITextAutocorrectionTypeNo;
    [_setUp addSubview:password];
    _password = password;
    
    
    
    [self addSubview:_setUp];
    _setUp.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
    
}

-(void)dismissKeyboard
{
    [self endEditing:YES];
}

- (void)registerForKeyboardNotifications
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _setUp.contentInset = contentInsets;
    _setUp.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, username.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, username.frame.origin.y-kbSize.height-50);
        [_setUp setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _setUp.contentInset = contentInsets;
    _setUp.scrollIndicatorInsets = contentInsets;
}


- (AFCNavigationController*) navigationController
{
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate] rootViewController];
}

@end
