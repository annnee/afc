 

#import <UIKit/UIKit.h>
#import "AFCViewController.h"

@interface ProfileViewController : AFCViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@end
