

#import "AFCUser.h"

@implementation AFCUser

@synthesize isDriver = _isDriver;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize addresses = _addresses;
@synthesize phoneNumbers = _phoneNumbers;
@synthesize email = _email;
@synthesize username = _username;

- (id)initWithFirstName:(NSString *)userFirstName
               lastName:(NSString *)userLastName
              addresses:(NSArray *)userAddresses
           phoneNumbers:(NSArray *)userPhoneNumbers
                  email:(NSString *)userEmail username:(NSString *)userUsername isDriver:(BOOL)isDriver
{
    self = [super init];
    if (self)
    {
        _isDriver = isDriver;
        _firstName = userFirstName;
        _lastName = userLastName;
        _addresses = userAddresses;
        _phoneNumbers = userPhoneNumbers;
        _email = userEmail;
        _username = userUsername;
        
    }
    return self;
}

-(id)initFromDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        NSString *name = [dictionary objectForKey:@"name"];
        NSArray *firstLast = [name componentsSeparatedByString:@" "];
        NSArray *addresses = [dictionary objectForKey:@"addresses"];
        NSArray *phones = [dictionary objectForKey:@"phones"];
        NSArray *roles = [dictionary objectForKey:@"roles"];
        
        _firstName = firstLast.count > 0?  firstLast[0] : nil;
        _lastName = firstLast.count > 1? firstLast[1] : nil;
        _email = [dictionary objectForKey:@"email"];
        _username = [dictionary objectForKey:@"username"];
        
        NSMutableArray *AFCAddresses = [[NSMutableArray alloc] init];
        for(NSDictionary *d in addresses)
            [AFCAddresses addObject:[[AFCAddress alloc] initFromDictionary:d]];
        
        NSMutableArray *AFCPhones = [[NSMutableArray alloc] init];
        for(NSDictionary *d in phones)
            [AFCPhones addObject:[[AFCPhone alloc] initFromDictionary:d]];
        
        _addresses = [NSArray arrayWithArray:AFCAddresses];
        _phoneNumbers = [NSArray arrayWithArray:AFCPhones];
        
        _isDriver = [roles containsObject:@"driver"];
    }
    return self;
}

-(NSDictionary *)dictionary
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *addressArray = [[NSMutableArray alloc] init];
    for(AFCAddress *a in _addresses) [addressArray addObject:a.dictionary];
    
    NSMutableArray *phoneArray = [[NSMutableArray alloc] init];
    for(AFCPhone *p in _phoneNumbers) [phoneArray addObject:p.dictionary];
    
    NSString *firstLast = [NSString stringWithFormat:@"%@ %@", _firstName, _lastName];
    [d setObject:firstLast forKey:@"name"];
    [d setObject:_username forKey:@"username"];
    [d setObject:_email forKey:@"email"];
    [d setObject:addressArray forKey:@"addresses"];
    [d setObject:phoneArray forKey:@"phones"];
    
    return d;
}

@end
