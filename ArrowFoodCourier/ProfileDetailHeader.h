

#import <UIKit/UIKit.h>

@interface ProfileDetailHeader : UITableViewHeaderFooterView

@property (nonatomic, strong) UILabel *headerLabel;

@end
