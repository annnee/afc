 

#import <UIKit/UIKit.h>

@interface LeftDrawer : UIView

+ (LeftDrawer*)sharedInstance;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *address;

@property (nonatomic, readonly) BOOL isOpen;

- (void)show;
- (void)hide;
- (void)toggle;
- (void)updateUserInformation;

@end
