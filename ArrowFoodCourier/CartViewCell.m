

#import "CartViewCell.h"

@implementation CartViewCell
{
    UILabel *_nameLabel;
    UILabel *_descriptionLabel;
    UILabel *_priceLabel;
    
    UIImageView *_iconImageView;
    
    UIView *_mainView;
    UIView *_deleteView;
    BOOL _deleteVisible;
    
    CGRect _mainViewOriginalFrame;
    CGRect _deleteViewOriginalFrame;
    CGRect _mainViewShiftedFrame;
    CGRect _deleteViewShiftedFrame;
}

@synthesize cellMenuItem = _cellMenuItem;

#pragma mark - Lifecycle -

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        //self.frame = CGRectMake(0, 0, 200, self.frame.size.height);
        
        // Initialize private variables
        _nameLabel = [[UILabel alloc] init];
        _priceLabel = [[UILabel alloc] init];
        _descriptionLabel = [[UILabel alloc] init];
        
        // Hide the original content to instead use custom content
        self.textLabel.alpha = 0;
        self.imageView.alpha = 0;
        
        _mainView = [[UIView alloc] init];
        _deleteView = [[UIView alloc] init];
        _deleteVisible = NO;
        
    }
    return self;
}

-(void)dealloc
{
    
}

#pragma mark - Customization -

- (void)setupView
{
    [[self contentView] setUserInteractionEnabled:YES];
    
    //Deletion view
    UIView *deleteView = _deleteView;
    [deleteView setFrame:CGRectMake(CGRectGetWidth(self.frame), 0, CartCellHeight, CartCellHeight)];
    _deleteViewOriginalFrame = deleteView.frame;
    deleteView.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    deleteView.userInteractionEnabled = YES;
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImageView *deleteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, delete.size.width, delete.size.height)];
    [deleteImageView setImage:delete];
    [deleteImageView setCenter:CGPointMake(CGRectGetWidth(deleteView.frame) / 2, CGRectGetHeight(deleteView.frame) / 2)];
    [deleteView addSubview:deleteImageView];
    
    [self.contentView addSubview:deleteView];
    
    UIView *cellView = _mainView;
    [cellView setFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CartCellHeight)];
    _mainViewOriginalFrame = cellView.frame;
    cellView.backgroundColor = [UIColor whiteColor];
    cellView.userInteractionEnabled = YES;
    [self.contentView addSubview:cellView];
    
    [[self contentView] setBackgroundColor:[UIColor whiteColor]];
    
    // Create the label for the menu item name
    [_nameLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
    [_nameLabel setTextColor:[UIColor blackColor]];
    [_nameLabel setBackgroundColor:[UIColor clearColor]];
    [_nameLabel setFrame:CGRectMake(CartCellMargin, 0, self.frame.size.width - 2*CartCellMargin, CartCellHeight + 2)];
    [cellView addSubview:_nameLabel];
    
    // For the price
    [_priceLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
    [_priceLabel setTextColor:[UIColor colorFromHexValue:AFCGray]];
    [_priceLabel setBackgroundColor:[UIColor clearColor]];
    [_priceLabel setFrame:CGRectMake(CartCellMargin, 0, self.frame.size.width - 2*CartCellMargin, CartCellHeight + 2)];
    [_priceLabel setTextAlignment:NSTextAlignmentRight];
    [cellView addSubview:_priceLabel];
    
    _mainViewShiftedFrame = CGRectMake(CGRectGetMinX(_mainViewOriginalFrame) - CGRectGetWidth(_deleteViewOriginalFrame), CGRectGetMinY(_mainViewOriginalFrame),
                                       CGRectGetWidth(_mainViewOriginalFrame), CGRectGetHeight(_mainViewOriginalFrame));
    _deleteViewShiftedFrame = CGRectMake(CGRectGetMinX(_deleteViewOriginalFrame) - CGRectGetWidth(_deleteViewOriginalFrame), CGRectGetMinY(_deleteViewOriginalFrame),
                                         CGRectGetWidth(_deleteViewOriginalFrame), CGRectGetHeight(_deleteViewOriginalFrame));
}

#pragma mark - Overriden Setters -

- (void)setCellMenuItem:(AFCMenuItem *)cellMenuItem
{
    _cellMenuItem = cellMenuItem;
    [_nameLabel setText:_cellMenuItem.name];
    [_descriptionLabel setText:_cellMenuItem.description];
    [_priceLabel setText:[NSString stringWithFormat:@"$%.2f", _cellMenuItem.price]];
    [self setupView];
}

#pragma mark - Delete Methods

- (BOOL)locationHitsDeleteView:(CGPoint)point;
{
    return ([[self hitTest:point withEvent:nil] isEqual:_deleteView] && _deleteVisible);
}

-(void)toggleVisibiltyOfDeleteView
{
    _deleteVisible? [self hideDeleteView] : [self showDeleteView];
}

-(void)showDeleteView
{
    self.userInteractionEnabled = NO;
    
    _deleteVisible = YES;
    
    [UIView animateWithDuration:0.2 delay:0 options:(UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState)
    animations:^
    {
        [_mainView setFrame:_mainViewShiftedFrame];
        [_deleteView setFrame:_deleteViewShiftedFrame];
        
    } completion:^(BOOL finished)
    {
        self.userInteractionEnabled = YES;
        
    }];
}

-(void)hideDeleteView
{
    self.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:0.2 delay:0 options:(UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState)
    animations:^
    {
        [_mainView setFrame:_mainViewOriginalFrame];
        [_deleteView setFrame:_deleteViewOriginalFrame];
        
    } completion:^(BOOL finished)
    {
        self.userInteractionEnabled = YES;
        _deleteVisible = NO;
        
    }];
}

@end
