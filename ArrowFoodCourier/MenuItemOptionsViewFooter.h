 

#import <UIKit/UIKit.h>
#import "AFCView.h"

@interface MenuItemOptionsViewFooter : AFCView

- (void)addTargetToButton:(id)target action:(SEL)action;

@end
