

#import <UIKit/UIKit.h>

@protocol RestaurantsSecondaryNavBarDelegate <NSObject>

- (void)filterButtonPressed:(UIButton*)sender;
- (void)sortButtonPressed:(UIButton*)sender;
- (void)searchButtonPressed:(UIButton*)sender;

- (void)filterOptionSelected:(NSString*)filter;
- (void)sortOptionSelected:(NSString*)sort;
- (void)searchTextChanged:(NSString*)search;

@end

@interface RestaurantsSecondaryNavBar : UIView<UITextFieldDelegate>

@property (nonatomic, weak) id<RestaurantsSecondaryNavBarDelegate> delegate;

@property (nonatomic, strong) UIView *filterView;
@property (nonatomic, strong) UIView *sortView;
@property (nonatomic, strong) UIView *searchView;

- (id)initWithPosition:(CGPoint)position;
- (NSArray*)filterOptions;
- (NSArray*)sortOptions;
- (void)resignSearchBarResponder;
- (void)makeSearchBarFirstResponder;

@end
