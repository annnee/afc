 

#import <UIKit/UIKit.h>
#import "WYPopoverController.h"
#import "AFCViewController.h"
#import "AFCRestaurant.h"

@interface MenuCategoriesViewController : AFCViewController<UITableViewDataSource, UITableViewDelegate>

- (id)initWithRestaurant:(AFCRestaurant *)restaurant;

@end
