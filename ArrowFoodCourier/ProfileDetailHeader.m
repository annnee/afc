

#import "ProfileDetailHeader.h"

@implementation ProfileDetailHeader

@synthesize headerLabel = _headerLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width - 20, frame.size.height)];
        [self addSubview: _headerLabel];
        [_headerLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
