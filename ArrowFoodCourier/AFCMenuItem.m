
#import "AFCMenuItem.h"

@implementation AFCMenuItem

@synthesize name = _name;
@synthesize description = _description;
@synthesize image = _image;
@synthesize price = _price;

- (id)initWithName:(NSString *)itemName description:(NSString *)itemDescription price:(float)itemPrice
{
    self = [super init];
    if (self)
    {
        _name = itemName;
        _description = itemDescription;
        _price = itemPrice;
        _image = nil;
        
        options = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithName:(NSString *)itemName description:(NSString *)itemDescription price:(float)itemPrice image:(UIImage *)itemImage
{
    self = [super init];
    if (self)
    {
        _name = itemName;
        _description = itemDescription;
        _price = itemPrice;
        _image = itemImage;
        
        options = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        options = [[NSMutableArray alloc] init];
        
        _name = [dictionary objectForKey:@"name"];
        _description = [dictionary objectForKey:@"description"];
        _price = [[dictionary objectForKey:@"price"] floatValue];
        _image = nil;
        
        // Parse options
        NSArray *itemOptions = [dictionary objectForKey:@"itemOptions"];
        for (NSDictionary *option in itemOptions) {
            [self addOption:[[AFCMenuItemOption alloc] initWithDictionary:option]];
        }
    }
    return self;
}

- (void)addOption:(AFCMenuItemOption *)option
{
    [options addObject:option];
}

- (NSArray *)allOptions
{
    return options;
}

- (NSArray*)allOptionsDictionary
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for(int i = 0; i < options.count; i++)
    {
        array[i] = ((AFCMenuItemOption*)options[i]).dictionary;
    }
    return array;
}

@end
