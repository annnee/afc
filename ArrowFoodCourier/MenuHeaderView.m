

#import "MenuHeaderView.h"
#import "AFCRestaurant.h"

@implementation MenuHeaderView

@synthesize restaurant = _restaurant;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (id)initWithPosition:(CGPoint)position restaurant:(AFCRestaurant *)restaurant
{
    _restaurant = restaurant;
    self = [self initWithFrame:CGRectMake(position.x, position.y, [[UIScreen mainScreen] bounds].size.width, MenuHeaderHeight)];
    return self;
}

- (void)setupView
{
    int originX = [self bounds].origin.x;
    int originY = [self bounds].origin.y;
    int screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    UIView *storeFrontView = [[UIImageView alloc] initWithFrame:CGRectMake(originX,
                                                                           originY,
                                                                           screenWidth,
                                                                           MenuStoreFrontHeight)];
    storeFrontView.backgroundColor = [UIColor colorFromHexValue:AFCRed];
    
    UIImageView *storeFrontImageView = [[UIImageView alloc] initWithFrame:CGRectMake(originX,
                                                                                originY,
                                                                                screenWidth,
                                                                                MenuStoreFrontHeight - 2)];
    [storeFrontImageView setImage:_restaurant.storefrontImage];
    
    UITextView *restaurantDescription = [[UITextView alloc] initWithFrame:CGRectMake(0,
                                                                                     MenuStoreFrontHeight - MenuStoreFrontDescriptionHeight - 2,
                                                                                     screenWidth,
                                                                                     MenuStoreFrontDescriptionHeight)];
    restaurantDescription.editable = NO;
    restaurantDescription.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    restaurantDescription.text = _restaurant.detailedDescription;
    restaurantDescription.textColor = [UIColor whiteColor];
    restaurantDescription.font = [UIFont fontWithName:BODY_FONT size:12];
    restaurantDescription.userInteractionEnabled = NO;
    
    [storeFrontView addSubview:storeFrontImageView];
    [storeFrontView addSubview:restaurantDescription];
    
    UIView *restaurantTitleView = [[UIImageView alloc] initWithFrame:CGRectMake(originX, originY + MenuStoreFrontHeight, screenWidth, MenuHeaderHeight - MenuStoreFrontHeight)];
    
    UILabel *restaurantTitle = [[UILabel alloc] initWithFrame:CGRectMake(MenuHeaderMargin, MenuHeaderMargin, 0, 0)];
    restaurantTitle.text = [_restaurant.name uppercaseString];
    restaurantTitle.font = [UIFont fontWithName:HEADER_FONT size:24];
    restaurantTitle.textColor = [UIColor blackColor];
    [restaurantTitle sizeToFit];
    UILabel *restaurantShortDescription = [[UILabel alloc] initWithFrame:CGRectMake(MenuHeaderMargin, restaurantTitle.bounds.size.height + 7, 0, 0)];
    restaurantShortDescription.text = _restaurant.shortDescription;
    restaurantShortDescription.font = [UIFont fontWithName:HEADER_FONT size:20];
    restaurantShortDescription.textColor = [UIColor colorFromHexValue:AFCGray];
    [restaurantShortDescription setBackgroundColor:[UIColor clearColor]];
    [restaurantShortDescription sizeToFit];
    
    CGFloat starWidth = (110 / 5) * _restaurant.rating;
    UIImageView *restaurantStarRating = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth - starWidth - MenuHeaderMargin, MenuHeaderMargin, starWidth, 22)];
    [restaurantStarRating setContentMode:UIViewContentModeLeft];
    restaurantStarRating.clipsToBounds = TRUE;
    [restaurantStarRating setImage:[UIImage imageNamed:@"Stars"]];

    [restaurantTitleView addSubview:restaurantTitle];
    [restaurantTitleView addSubview:restaurantShortDescription];
    [restaurantTitleView addSubview:restaurantStarRating];
    
    [self addSubview:storeFrontView];
    [self addSubview:restaurantTitleView];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
