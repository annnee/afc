 

#import "CheckOutViewController.h"
#import "TrackingStatusBar.h"
#import "CartViewFooter.h"
#import "OrderStatusViewController.h"

@interface CheckOutViewController ()

@end

typedef enum
{
    PaymentTypePaypal,
    PaymentTypeCreditCard,
    PaymentTypeCash
} PaymentType;

@implementation CheckOutViewController
{
    TrackingStatusBar *_statusBar;
    UIView *_containerView;
    
    NSArray *_paymentTypes;
    
    UIScrollView *_orderSummaryView;
    UIView *_paymentOptionsView;
    UIScrollView *_paymentDetailsView;
    UIView *_orderPlacedView;
    
    UITableView *_orderDetailsTableView;
    UITableView *_paymentOptionsTableView;
    
    PaymentType _selectedPaymentType;
}

#pragma mark - Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _payPalConfiguration = [[PayPalConfiguration alloc] init];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View Creation

-   (void)setupView
{
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self addHomeIcon];
    
    self.navigationItem.title = @"CHECKOUT";
    
    // Shows the checkout status
    _statusBar = [[TrackingStatusBar alloc] initWithPosition:CGPointMake(40, 20) andWidth:self.view.frame.size.width - 80 andCircleDiameter:20
                                                 andStatuses:[NSArray arrayWithObjects:@"Summary", @"Payment", @"Tracking", nil]];
    [_statusBar setStatus:@"Summary"];
    [self.view addSubview:_statusBar];
    
    UIView *grayDivider = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_statusBar.frame) + 28, CGRectGetWidth(self.view.frame), 2)];
    grayDivider.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [self.view addSubview:grayDivider];
    
    // Container for content that animates on/off screen (separate from the check out status)
    _containerView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_statusBar.frame) + 30, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetHeight(_statusBar.frame) - 30)];
    [self.view addSubview:_containerView];
    [_containerView addSubview:[self orderSummaryView]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (UIView*)orderSummaryView
{
    if(!_orderSummaryView)
    {
        _orderSummaryView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_containerView.frame), CGRectGetHeight(_containerView.frame))];
        _orderSummaryView.showsVerticalScrollIndicator = YES;
        _orderSummaryView.contentSize = CGSizeMake(CGRectGetWidth(_containerView.frame), CGRectGetHeight(_containerView.frame) * 2);
        _orderSummaryView.delaysContentTouches = NO;
        
        int yPos = 12;
        int orderItemRowHeight = 20;
        int leftPadding = 15;
        
        // Create order details
        UILabel *orderDetailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        orderDetailsLabel.font = [UIFont fontWithName:HEADER_FONT size:17];
        orderDetailsLabel.text = @"Order Details:";
        orderDetailsLabel.textColor = [UIColor colorFromHexValue:AFCRed];
        [_orderSummaryView addSubview:orderDetailsLabel];
        
        yPos += orderDetailsLabel.frame.size.height;
        
        // Order details information
        _orderDetailsTableView = [[UITableView alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2,
                                                                               orderItemRowHeight * [[[AFCListing sharedStore] allCartItems] count] + CartFooterHeight - 60)];
        _orderDetailsTableView.delegate = self;
        _orderDetailsTableView.dataSource = self;
        _orderDetailsTableView.scrollEnabled = NO;
        _orderDetailsTableView.userInteractionEnabled = NO;
        _orderDetailsTableView.backgroundColor = [UIColor colorFromHexValue:AFCGray];
        _orderDetailsTableView.tableFooterView  = [[CartViewFooter alloc] initWithoutCheckoutWithFrame:CGRectMake(0, 0, CGRectGetWidth(_orderDetailsTableView.frame), CartFooterHeight - 60)];
        [_orderSummaryView addSubview:_orderDetailsTableView];
        
        // Remove the extra whitespace at the left of the tableview
        if ([_orderDetailsTableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
        {
            [_orderDetailsTableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
         yPos += CGRectGetHeight(_orderDetailsTableView.frame);
        
        // Create delivery details
        UILabel *deliveryInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        deliveryInfoLabel.font = [UIFont fontWithName:HEADER_FONT size:17];
        deliveryInfoLabel.text = @"Delivery Information:";
        deliveryInfoLabel.textColor = [UIColor colorFromHexValue:AFCRed];
        [_orderSummaryView addSubview:deliveryInfoLabel];
        
        
        yPos += CGRectGetHeight(deliveryInfoLabel.frame);
        
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        nameLabel.font = [UIFont fontWithName:HEADER_FONT size:15];
        nameLabel.text = @"Name:";
        [_orderSummaryView addSubview:nameLabel];
        
        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        name.font = [UIFont fontWithName:HEADER_FONT size:15];
        name.text = [NSString stringWithFormat:@"%@ %@", [[[AFCListing sharedStore] user] firstName], [[[AFCListing sharedStore] user] lastName]];
        name.textAlignment = NSTextAlignmentRight;
        name.textColor = [UIColor colorFromHexValue:AFCGray];
        [_orderSummaryView addSubview:name];
        
        yPos += CGRectGetHeight(nameLabel.frame);
        
        UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        addressLabel.font = [UIFont fontWithName:HEADER_FONT size:15];
        addressLabel.text = @"Address:";
        [_orderSummaryView addSubview:addressLabel];
        
        UILabel *address = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        address.font = [UIFont fontWithName:HEADER_FONT size:15];
        address.text = [NSString stringWithFormat:@"%@", [(AFCAddress*)[[[[AFCListing sharedStore] user] addresses] objectAtIndex:0] text]];
        address.textAlignment = NSTextAlignmentRight;
        address.textColor = [UIColor colorFromHexValue:AFCGray];
        [_orderSummaryView addSubview:address];
        
        yPos += CGRectGetHeight(addressLabel.frame);
        
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        phoneLabel.font = [UIFont fontWithName:HEADER_FONT size:15];
        phoneLabel.text = @"Phone: ";
        [_orderSummaryView addSubview:phoneLabel];
        
        UILabel *phone = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        phone.font = [UIFont fontWithName:HEADER_FONT size:15];
        phone.text = [NSString stringWithFormat:@"%@", [(AFCPhone*)[[[[AFCListing sharedStore] user] phoneNumbers] objectAtIndex:0] number]];
        phone.textAlignment = NSTextAlignmentRight;
        phone.textColor = [UIColor colorFromHexValue:AFCGray];
        [_orderSummaryView addSubview:phone];
        
        yPos += CGRectGetHeight(phoneLabel.frame);
        
        UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        emailLabel.font = [UIFont fontWithName:HEADER_FONT size:15];
        emailLabel.text = @"Email:";
        [_orderSummaryView addSubview:emailLabel];
        
        UILabel *email = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, CGRectGetWidth(self.view.frame) - leftPadding*2, 20)];
        email.font = [UIFont fontWithName:HEADER_FONT size:15];
        email.text = [NSString stringWithFormat:@"%@", [[[AFCListing sharedStore] user] email]];
        email.textAlignment = NSTextAlignmentRight;
        email.textColor = [UIColor colorFromHexValue:AFCGray];
        [_orderSummaryView addSubview:email];
        
        yPos += CGRectGetHeight(emailLabel.frame) + 18;
        
        UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *btnLayer = [continueButton layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:5.0f];
        [continueButton setTitle:@"Continue" forState:UIControlStateNormal];
        continueButton.frame = CGRectMake((CGRectGetWidth(self.view.frame) - 150) / 2, yPos, 150, 30);
        continueButton.backgroundColor = [UIColor colorFromHexValue:AFCGray];
        [continueButton addTarget:self action:@selector(makePaypalPayment) forControlEvents:UIControlEventTouchUpInside];
        [[continueButton titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
        [continueButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted | UIControlStateSelected)];
        [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted)];
        [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateSelected)];
        [_orderSummaryView addSubview:continueButton];
        
        yPos += CGRectGetHeight(continueButton.frame) + 18;
        
        if(yPos <= CGRectGetHeight(_orderSummaryView.frame))
        {
            yPos = CGRectGetHeight(_orderSummaryView.frame) - (CGRectGetHeight(continueButton.frame) + 18);
            [continueButton setFrame:CGRectMake(CGRectGetMinX(continueButton.frame), yPos,
                                                CGRectGetWidth(continueButton.frame), CGRectGetHeight(continueButton.frame))];
        }
        
        _orderSummaryView.contentSize = CGSizeMake(CGRectGetWidth(_containerView.frame), yPos);
        
    
    }
    return _orderSummaryView;
}

- (UIView*)paymentOptionsView
{
    if(!_paymentOptionsView)
    {
        _paymentOptionsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_containerView.frame), CGRectGetHeight(_containerView.frame))];
        _paymentTypes = [NSArray arrayWithObjects:@"Paypal", nil];
        _paymentOptionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_containerView.frame), 44*3)];
        _paymentOptionsTableView.bounces = NO;
        _paymentOptionsTableView.delegate = self;
        _paymentOptionsTableView.dataSource = self;
        [_paymentOptionsView addSubview:_paymentOptionsTableView];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *btnLayer = [backButton layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:5.0f];
        [backButton setTitle:@"Back" forState:UIControlStateNormal];
        backButton.frame = CGRectMake((CGRectGetWidth(_paymentOptionsView.frame) - 150) / 2, CGRectGetHeight(_containerView.frame) - 48, 150, 30);
        backButton.backgroundColor = [UIColor colorFromHexValue:AFCGray];
        [backButton addTarget:self action:@selector(backToSummary) forControlEvents:UIControlEventTouchUpInside];
        [[backButton titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
        [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [backButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted | UIControlStateSelected)];
        [backButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted)];
        [backButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateSelected)];
        [_paymentOptionsView addSubview:backButton];
    }
    return _paymentOptionsView;
}

- (UIView*)paymentInformationViewForPaymentType:(PaymentType)type
{
    if(type == _selectedPaymentType && _paymentDetailsView != nil)
    {
        return _paymentDetailsView;
    }
    
    switch (type)
    {
        case PaymentTypePaypal:
        {
            break;
        }
        case PaymentTypeCreditCard:
        {
            _selectedPaymentType = PaymentTypeCreditCard;
            _paymentDetailsView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_containerView.frame), CGRectGetHeight(_containerView.frame))];
            _paymentDetailsView.contentSize = CGSizeMake(CGRectGetWidth(_paymentDetailsView.frame), CGRectGetHeight(_paymentDetailsView.frame));
            _paymentDetailsView.delaysContentTouches = NO;
            UILabel *nameLabel, *cardNumberLabel, *expiresLabel, *CVCodeLabel;
            UITextField *nameField, *cardNumberField, *expiresField, *CVCodeField;
            
            int leftPadding = 15;
            int yPos = 10;
            int height = 30;
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, self.view.frame.size.width - leftPadding*2, height)];
            nameLabel.font = [UIFont fontWithName:HEADER_FONT size:17];
            nameLabel.text = @"Cardholder:";
            
            yPos+= height;
            
            nameField = [[UITextField alloc] initWithFrame:CGRectMake(leftPadding, yPos, self.view.frame.size.width - leftPadding*2, height)];
            nameField.borderStyle = UITextBorderStyleRoundedRect;
            nameField.returnKeyType = UIReturnKeyDone;
            nameField.font = [UIFont fontWithName:BODY_FONT size:15];
            nameField.placeholder = @"Enter Name";
            
            yPos+=height;
            
            cardNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, self.view.frame.size.width - leftPadding*2, height)];
            cardNumberLabel.font = [UIFont fontWithName:HEADER_FONT size:17];
            cardNumberLabel.text = @"Card Number:";
            
            yPos+= height;
            
            cardNumberField = [[UITextField alloc] initWithFrame:CGRectMake(leftPadding, yPos, self.view.frame.size.width - leftPadding*2, height)];
            cardNumberField.borderStyle = UITextBorderStyleRoundedRect;
            cardNumberField.returnKeyType = UIReturnKeyDone;
            cardNumberField.keyboardType = UIKeyboardTypeNumberPad;
            cardNumberField.placeholder = @"Enter Card Number";
            cardNumberField.font = [UIFont fontWithName:BODY_FONT size:15];
            
            yPos+=height;
            
            expiresLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, yPos, self.view.frame.size.width/2 - leftPadding, height)];
            expiresLabel.font = [UIFont fontWithName:HEADER_FONT size:17];
            expiresLabel.text = @"Expires:";
            
            CVCodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 + leftPadding, yPos, self.view.frame.size.width/2 - leftPadding, height)];
            CVCodeLabel.font = [UIFont fontWithName:HEADER_FONT size:17];
            CVCodeLabel.text = @"CVC:";
            
            yPos+= height;
            
            expiresField = [[UITextField alloc] initWithFrame:CGRectMake(expiresLabel.frame.origin.x, yPos, 100, height)];
            expiresField.borderStyle = UITextBorderStyleRoundedRect;
            expiresField.returnKeyType = UIReturnKeyDone;
            expiresField.placeholder = @"MMYY";
            expiresField.keyboardType = UIKeyboardTypeNumberPad;
            expiresField.font = [UIFont fontWithName:BODY_FONT size:15];
            
            CVCodeField = [[UITextField alloc] initWithFrame:CGRectMake(CVCodeLabel.frame.origin.x, yPos, 100, height)];
            CVCodeField.borderStyle = UITextBorderStyleRoundedRect;
            CVCodeField.returnKeyType = UIReturnKeyDone;
            CVCodeField.keyboardType = UIKeyboardTypeNumberPad;
            CVCodeField.font = [UIFont fontWithName:BODY_FONT size:15];
            
            nameField.delegate = self;
            cardNumberField.delegate = self;
            expiresField.delegate = self;
            CVCodeField.delegate = self;
            
            [_paymentDetailsView addSubview:nameLabel];
            [_paymentDetailsView addSubview:cardNumberLabel];
            [_paymentDetailsView addSubview:expiresLabel];
            [_paymentDetailsView addSubview:CVCodeLabel];
            [_paymentDetailsView addSubview:nameField];
            [_paymentDetailsView addSubview:cardNumberField];
            [_paymentDetailsView addSubview:expiresField];
            [_paymentDetailsView addSubview:CVCodeField];
            
            UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
            CALayer *btnLayer = [backButton layer];
            [btnLayer setMasksToBounds:YES];
            [btnLayer setCornerRadius:5.0f];
            [backButton setTitle:@"Back" forState:UIControlStateNormal];
            backButton.frame = CGRectMake((CGRectGetWidth(_paymentOptionsView.frame) - 250) / 2, CGRectGetHeight(_containerView.frame) - 48, 120, 30);
            backButton.backgroundColor = [UIColor colorFromHexValue:AFCGray];
            [backButton addTarget:self action:@selector(backToPaymentOptions) forControlEvents:UIControlEventTouchUpInside];
            [[backButton titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
            [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [backButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted | UIControlStateSelected)];
            [backButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted)];
            [backButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateSelected)];
            backButton.exclusiveTouch = YES;
            [_paymentDetailsView addSubview:backButton];
            
            UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
            CALayer *btnLayer2 = [continueButton layer];
            [btnLayer2 setMasksToBounds:YES];
            [btnLayer2 setCornerRadius:5.0f];
            [continueButton setTitle:@"Place Order" forState:UIControlStateNormal];
            continueButton.frame = CGRectMake((CGRectGetWidth(_paymentOptionsView.frame) - 250) / 2 + 130, CGRectGetHeight(_containerView.frame) - 48, 120, 30);
            continueButton.backgroundColor = [UIColor colorFromHexValue:AFCRed];
            [[continueButton titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
            [continueButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted | UIControlStateSelected)];
            [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted)];
            [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateSelected)];
            continueButton.exclusiveTouch = YES;
            [_paymentDetailsView addSubview:continueButton];
            
            break;
        }
        case PaymentTypeCash:
        {
            break;
        }
        default:
            break;
    }
    return _paymentDetailsView;
}


- (UIView*)orderPlacedView
{
    if(!_orderPlacedView)
    {
        _orderPlacedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_containerView.frame), CGRectGetHeight(_containerView.frame))];
    
        UILabel *thankYou = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, CGRectGetWidth(_containerView.frame), 100)];
        thankYou.backgroundColor = [UIColor clearColor];
        thankYou.font = [UIFont fontWithName:HEADER_FONT size:45];
        thankYou.textColor = [UIColor colorFromHexValue:AFCRed];
        thankYou.textAlignment = NSTextAlignmentCenter;
        thankYou.text = @"Thank You!";
        
        [_orderPlacedView addSubview:thankYou];
        
        UITextView *description = [[UITextView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(thankYou.frame)+10, CGRectGetWidth(_containerView.frame), 100)];
        description.text = @"We have received your order and are working to get it to you as fast as possible. You can view the status of your order any time in the tracking view.";
        description.userInteractionEnabled = NO;
        description.textAlignment = NSTextAlignmentCenter;
        description.font = [UIFont fontWithName:BODY_FONT_LIGHT size:12];
        description.editable = NO;
    
        [_orderPlacedView addSubview:description];
        
        UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *btnLayer2 = [continueButton layer];
        [btnLayer2 setMasksToBounds:YES];
        [btnLayer2 setCornerRadius:5.0f];
        [continueButton setTitle:@"Start Tracking" forState:UIControlStateNormal];
        continueButton.frame = CGRectMake(CGRectGetWidth(_orderPlacedView.frame) / 2 - 60, CGRectGetHeight(_containerView.frame) - 48, 120, 30);
        continueButton.backgroundColor = [UIColor colorFromHexValue:AFCRed];
        [[continueButton titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
        [continueButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted | UIControlStateSelected)];
        [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateHighlighted)];
        [continueButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateSelected)];
        continueButton.exclusiveTouch = YES;
        [continueButton addTarget:self action:@selector(startTracking:) forControlEvents:UIControlEventTouchUpInside];
        [_orderPlacedView addSubview:continueButton];
        
    }
    return _orderPlacedView;
}

- (void)moveToView:(UIView*)view fromView:(UIView*)view2 directionForward:(BOOL)forward
{
    if(forward)
    {
        view.userInteractionEnabled = NO;
        view2.userInteractionEnabled = NO;
        [view setFrame:CGRectMake(self.view.frame.size.width, view2.frame.origin.y, view.frame.size.width, view.frame.size.height)];
        [_containerView addSubview:view];
        
    
        [UIView animateWithDuration:0.37 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
         {
             [view setFrame:CGRectMake(0, view2.frame.origin.y, view.frame.size.width, view.frame.size.height)];
             [view2 setFrame:CGRectMake(-self.view.frame.size.width, view2.frame.origin.y, view2.frame.size.width, view2.frame.size.height)];
             
         } completion:^(BOOL finished)
         {
             view.userInteractionEnabled = YES;
             [view2 removeFromSuperview];
         }];
    }
    else
    {
        view.userInteractionEnabled = NO;
        view2.userInteractionEnabled = NO;
        [view setFrame:CGRectMake(-self.view.frame.size.width, view2.frame.origin.y, view.frame.size.width, view.frame.size.height)];
        [_containerView addSubview:view];
        
        
        [UIView animateWithDuration:0.37 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
         {
             [view setFrame:CGRectMake(0, view2.frame.origin.y, view.frame.size.width, view.frame.size.height)];
             [view2 setFrame:CGRectMake(self.view.frame.size.width, view2.frame.origin.y, view2.frame.size.width, view2.frame.size.height)];
             
         } completion:^(BOOL finished)
         {
             view.userInteractionEnabled = YES;
             [view2 removeFromSuperview];
         }];
    }
}

#pragma mark - Table View Delegate/Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _paymentOptionsTableView)
    {
        return _paymentTypes.count;
    }
    else if(tableView == _orderDetailsTableView)
    {
        return [[[AFCListing sharedStore] allCartItems] count];
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _orderDetailsTableView)
    {
        return 20;
    }
    else return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _paymentOptionsTableView)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"paymentOption"];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"paymentOption"];
            cell.textLabel.font = [UIFont fontWithName:HEADER_FONT size:17];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.text = _paymentTypes[indexPath.row];
        
        return cell;
    }
    else if(tableView == _orderDetailsTableView)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderItem"];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderItem"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UILabel *nameView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 20)];
            nameView.font = [UIFont fontWithName:HEADER_FONT size:15];
            nameView.textAlignment = NSTextAlignmentLeft;
            nameView.textColor = [UIColor darkTextColor];
            nameView.tag = 4;
            [cell addSubview:nameView];
            
            UILabel *priceView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 20)];
            priceView.font = [UIFont fontWithName:HEADER_FONT size:15];
            priceView.textAlignment = NSTextAlignmentRight;
            priceView.textColor = [UIColor colorFromHexValue:AFCGray];
            priceView.tag = 5;
            [cell addSubview:priceView];
        }
        
        UILabel *nameLabel = (UILabel*)[cell viewWithTag:4];
        AFCMenuItem *menuItem = (AFCMenuItem*)[(AFCCartItem*)[[AFCListing sharedStore] allCartItems][indexPath.row] menuItem];
        [nameLabel setText:menuItem.name];
        
        NSString *price = [NSString stringWithFormat:@"$%.2f", menuItem.price];
        UILabel *priceLabel = (UILabel*)[cell viewWithTag:5];
        priceLabel.text = price;
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView == _paymentOptionsTableView)
    {
        if(indexPath.row == PaymentTypePaypal)
        {
            PayPalPayment *payment = [[PayPalPayment alloc] init];
            
            // Amount, currency, and description
            payment.amount = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%.2f",[[AFCListing sharedStore] totalItemsWithTaxCost]]];
            payment.currencyCode = @"USD";
            payment.shortDescription = @"Total";
            
            // Use the intent property to indicate that this is a "sale" payment,
            // meaning combined Authorization + Capture. To perform Authorization only,
            // and defer Capture to your server, use PayPalPaymentIntentAuthorize.
            payment.intent = PayPalPaymentIntentSale;
            
            // Check whether payment is processable.
            if (!payment.processable) {
                // If, for example, the amount was negative or the shortDescription was empty, then
                // this payment would not be processable. You would want to handle that here.
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Paypal Error"
                                                             message:@"The payment is not processable. Please try again later."
                                                            delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [av show];
                return;
            }
            
            // Create a PayPalPaymentViewController.
            PayPalPaymentViewController *paymentViewController;
            paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                           configuration:self.payPalConfiguration
                                                                                delegate:self];
            
            // Present the PayPalPaymentViewController.
            [self presentViewController:paymentViewController animated:YES completion:nil];
        }
    }
}

- (void)makePaypalPayment
{
    [self startLoading];
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    
    // Amount, currency, and description
    payment.amount = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%.2f",[[AFCListing sharedStore] totalItemsWithTaxCost]]];
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Total";
    
    // Use the intent property to indicate that this is a "sale" payment,
    // meaning combined Authorization + Capture. To perform Authorization only,
    // and defer Capture to your server, use PayPalPaymentIntentAuthorize.
    payment.intent = PayPalPaymentIntentSale;
    
    // Create a PayPalPaymentViewController.
    PayPalPaymentViewController *paymentViewController;
    paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                   configuration:self.payPalConfiguration
                                                                        delegate:self];
    // Check whether payment is processable.
    if (!payment.processable)
    {
        
        // If, for example, the amount was negative or the shortDescription was empty, then
        // this payment would not be processable. You would want to handle that here.
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Paypal Error"
                                                     message:@"The payment is not processable. Please try again later."
                                                    delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [av show];
        return;
    }
    
    [_statusBar setStatus:@"Payment"];
    // Present the PayPalPaymentViewController.
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

- (void)verifyCompletedPayment:(PayPalPayment*)payment
{
    // Send the entire confirmation dictionary
    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:payment.confirmation
                                                           options:0
                                                             error:nil];
    
    // Send confirmation to your server; your server should verify the proof of payment
    // and give the user their goods or services. If the server is not reachable, save
    // the confirmation and try again later.
    [AFCRepository makeOrderWithDeliveryAddress:((AFCAddress*)[[AFCListing sharedStore].user.addresses objectAtIndex:0]).dictionary
                                 billingAddress:((AFCAddress*)[[AFCListing sharedStore].user.addresses objectAtIndex:0]).dictionary
                                responseHandler:^(NSString *error)
     {
         if(!error)
         {
             [_statusBar setStatus:@"Tracking"];
             [self moveToView:[self orderPlacedView] fromView:[self orderSummaryView] directionForward:YES];
             [AFCListing sharedStore].cart = nil;
             
         }
         else
         {
             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Cart Error"
                                                          message:error
                                                         delegate:nil
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles: nil];
             [av show];
         }
         [self stopLoading];
     }];
    
}

#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    // Payment was processed successfully; send to server for verification and fulfillment.
    [self verifyCompletedPayment:completedPayment];
    
    // Dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
    [self stopLoading];
}

#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Target Selectors

- (void)continueToPayment:(id)sender
{
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentNoNetwork];
    [self moveToView:[self paymentOptionsView] fromView:[self orderSummaryView] directionForward:YES];
    [_statusBar setStatus:@"Payment"];
}

- (void)backToSummary
{
    [self moveToView:[self orderSummaryView] fromView:[self paymentOptionsView] directionForward:NO];
    [_statusBar setStatus:@"Summary"];
}

- (void)backToPaymentOptions
{
    for(UITextField *field in [[self paymentInformationViewForPaymentType:_selectedPaymentType] subviews])
    {
        [field resignFirstResponder];
    }
    
    [self moveToView:[self paymentOptionsView] fromView:[self paymentInformationViewForPaymentType:_selectedPaymentType] directionForward:NO];
    [_statusBar setStatus:@"Payment"];
}

- (void)startTracking:(id)sender
{
    [[self mainNavigationController] moveToHomeController:[[OrderStatusViewController alloc] init]];
}

#pragma mark - Keyboard Selectors

- (void)keyboardDidShow:(NSNotification*)notification
{
    if(_paymentDetailsView != nil)
    {
        NSDictionary* keyboardInfo = [notification userInfo];
        NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
        CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
        
        CGRect frame = _paymentDetailsView.frame;
        _paymentDetailsView.frame = CGRectMake(0, 0, CGRectGetWidth(_paymentDetailsView.frame), CGRectGetHeight(_paymentDetailsView.frame) - CGRectGetHeight(keyboardFrameBeginRect));
        _paymentDetailsView.contentSize = CGSizeMake(CGRectGetWidth(frame), CGRectGetHeight(frame));
    }
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    if(_paymentDetailsView != nil)
    {
        NSDictionary* keyboardInfo = [notification userInfo];
        NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
        CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
        
        _paymentDetailsView.frame = CGRectMake(0, 0, CGRectGetWidth(_paymentDetailsView.frame), CGRectGetHeight(_paymentDetailsView.frame) + CGRectGetHeight(keyboardFrameBeginRect));
        CGRect frame = _paymentDetailsView.frame;
        _paymentDetailsView.contentSize = CGSizeMake(CGRectGetWidth(frame), CGRectGetHeight(frame));
    }
}

@end
