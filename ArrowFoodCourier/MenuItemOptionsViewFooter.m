 

#import "MenuItemOptionsViewFooter.h"

@implementation MenuItemOptionsViewFooter
{
    UIButton *_addToCartButton;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setupElements];
    }
    return self;
}

- (void)setupElements
{
    CGRect addToCartButtonRect = CGRectMake(0, 0, self.frame.size.width, 60);
    
    _addToCartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_addToCartButton setFrame:addToCartButtonRect];
    _addToCartButton.backgroundColor = [UIColor colorFromHexValue:0xF8F8F8];
    _addToCartButton.titleLabel.font = [UIFont fontWithName:HEADER_FONT size:18];
    [_addToCartButton setTitleColor:[UIColor colorFromHexValue:AFCRed] forState:UIControlStateNormal];
    [_addToCartButton setTitleColor:[UIColor colorFromHexValue:AFCGray] forState:UIControlStateHighlighted];
    [_addToCartButton setTitle:@"Add To Cart" forState:UIControlStateNormal];
    
    [self addSubview:_addToCartButton];
}

- (void)addTargetToButton:(id)target action:(SEL)action
{
    [_addToCartButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

@end
