//
//  OrdersViewController.h
//  ArrowFoodCourier
//
//  Created by Annie Neel on 3/20/14.
//  Copyright (c) 2014 Ashleigh Chape. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFCViewController.h"

@interface OrdersViewController : AFCViewController

@end
