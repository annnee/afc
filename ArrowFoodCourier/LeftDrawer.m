 

#import "LeftDrawer.h"
#import "AFCNavigationController.h"
#import "ProfileViewController.h"
#import "RestaurantsViewController.h"
#import "TrackingViewController.h"
#import "OrderStatusViewController.h"
#import "LogOnViewController.h"
#import "CartViewController.h"
#import "OrdersViewController.h"
#import "CheckOutViewController.h"
#import "AFCListing.h"
#import "AFCUser.h"

@implementation LeftDrawer
{
    UILabel *_nameLabel;
    UILabel *_addressLabel;
    
    UIButton *_closeButton;
}

#pragma mark - Properties

@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize address = _address;

@synthesize isOpen = _isShowing;

#pragma mark - Lifecycle

// GCD to handle class constructor such that it is only initialized once and is
// thread safe.
+ (LeftDrawer*)sharedInstance
{
    static LeftDrawer *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^
    {
        _sharedInstance = [[LeftDrawer alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    });
    
    return _sharedInstance;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        //Initialization
        // Background color
        self.backgroundColor = [UIColor colorFromHexValue:AFCGray];
        
        // Create close button. This button goes over the top view to prevent any other items on the top view
        // from being pressed.
        _closeButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [_closeButton addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        // User name
        [self addUserNameUI];
        
        // Add buttons
        [self addButtons];
        
        // Default it is not showing.
        _isShowing = NO;
    }
    return self;
}

#pragma mark - View Creation

- (void) addUserNameUI
{
    // The white strip behind the label
    UIView *labelStrip = [[UIView alloc] initWithFrame:CGRectMake(0, 48, self.frame.size.width, 35)];
    labelStrip.backgroundColor = [UIColor whiteColor];
    [self addSubview:labelStrip];
    
    // The white circle behind the red icon
    UIImageView* circle = [ImageUtil viewWithImageName:@"UserIconBackground" andPos:CGPointMake(5, 8)];
    [circle setCenter:CGPointMake(circle.center.x, labelStrip.center.y)];
    [self addSubview:circle];
    
    // The AFC icon
    UIImageView *icon = [ImageUtil viewWithImageName:@"UserIcon" andCenter:circle.center];
    [self addSubview:icon];
    
    // User name label
    int x = circle.frame.origin.x+circle.frame.size.width+4;
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, labelStrip.frame.origin.y + 1, self.frame.size.width - x, labelStrip.frame.size.height)];
    [_nameLabel setFont:[UIFont fontWithName:HEADER_FONT size:17]];
    [_nameLabel setTextColor:[UIColor colorFromHexValue:AFCRed]];
    _nameLabel.backgroundColor = [UIColor clearColor];
    [_nameLabel setText:[NSString stringWithFormat:@"%@ %@", [[[AFCListing sharedStore] user] firstName], [[[AFCListing sharedStore] user] lastName]]];
    [self addSubview:_nameLabel];
}

- (void)addButtons
{
    // Add first content separator
    UIView *s1 = [[UIView alloc] initWithFrame:CGRectMake(0, 103, self.frame.size.width, 50)];
    s1.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [self addSubview:s1];
    
    UILabel *sl1 = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_CONTENT_PADDING, 10, s1.frame.size.width, s1.frame.size.height / 2)];
    sl1.textColor = [UIColor whiteColor];
    sl1.backgroundColor = [UIColor clearColor];
    sl1.text = @"DELIVERY";
    sl1.frame = CGRectIntegral(sl1.frame);
    sl1.font = [UIFont fontWithName:BODY_FONT_BOLD size:11];
    [s1 addSubview:sl1];
    
    // Delivery icon
    
    UIImage *pinImage = [UIImage imageNamed:@"location"];
    UIImageView *pinIcon = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_CONTENT_PADDING, sl1.frame.size.height + sl1.frame.origin.y - 4, pinImage.size.width, pinImage.size.height)];
    [pinIcon setImage:pinImage];
    [s1 addSubview:pinIcon];
    
    _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_CONTENT_PADDING + 15, sl1.frame.size.height + sl1.frame.origin.y - 9, s1.frame.size.width, s1.frame.size.height / 2)];
    _addressLabel.textColor = [UIColor whiteColor];
    _addressLabel.backgroundColor = [UIColor clearColor];
    _addressLabel.frame = CGRectIntegral(_addressLabel.frame);
    _addressLabel.text = [[(AFCAddress*)[[[[AFCListing sharedStore] user] addresses] objectAtIndex:0] text] uppercaseString];
    _addressLabel.font = [UIFont fontWithName:BODY_FONT_BOLD size:11];
    [s1 addSubview:_addressLabel];
    
    int yPos = 159;
    
    // Add a background view to make the rest of the view drawer red
    UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.frame.size.width, self.frame.size.height - yPos)];
    background.backgroundColor = [UIColor colorFromHexValue:DrawerRed];
    [self addSubview:background];
    
    yPos = 0;
    
    // Add the first set of buttons
    [background addSubview:[self createDrawerButtonWithTitle:@"Restaurants" andYPos:yPos andIcon:@"arrow-logo-icon" andSelector:@selector(restaurantPressed)]];
    [background addSubview:[self createDrawerButtonWithTitle:@"Tracking" andYPos:yPos+=DRAWER_BUTTON_HEIGHT andIcon:@"tracking" andSelector:@selector(trackingPressed)]];
    
    // Add next content separator
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, (yPos+=DRAWER_BUTTON_HEIGHT), self.frame.size.width, DRAWER_SEPARATOR_HEIGHT)];
    separatorView.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [background addSubview:separatorView];
    
    UILabel *separatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_CONTENT_PADDING, 0, separatorView.frame.size.width, separatorView.frame.size.height)];
    separatorLabel.textColor = [UIColor whiteColor];
    separatorLabel.backgroundColor = [UIColor clearColor];
    separatorLabel.text = @"ACCOUNT";
    separatorLabel.font = [UIFont fontWithName:BODY_FONT_BOLD size:11];
    [separatorView addSubview:separatorLabel];
    
    yPos += DRAWER_SEPARATOR_HEIGHT;
    
    // Add the next set of buttons
    [background addSubview:[self createDrawerButtonWithTitle:@"Profile" andYPos:(yPos) andIcon:@"profile" andSelector:@selector(profilePressed)]];
    [background addSubview:[self createDrawerButtonWithTitle:@"Sign Out" andYPos:(yPos+=DRAWER_BUTTON_HEIGHT) andIcon:@"signout" andSelector:@selector(signoutPressed)]];
}

- (UIButton*)createDrawerButtonWithTitle:(NSString*)title andYPos:(int)yPos andIcon:(NSString*)iconImageName andSelector:(SEL)selector
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, yPos, self.frame.size.width, DRAWER_BUTTON_HEIGHT)];
    button.titleLabel.font = [UIFont fontWithName:BODY_FONT_LIGHT size:17];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleEdgeInsets:UIEdgeInsetsMake(button.titleEdgeInsets.top, 50, button.titleEdgeInsets.bottom, button.titleEdgeInsets.right)];
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor colorFromHexValue:AFCGray] forState:UIControlStateHighlighted];
    
    if(iconImageName.length > 0)
    {
        UIImage *icon = [UIImage imageNamed:iconImageName];
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_CONTENT_PADDING, 0, icon.size.width, icon.size.height)];
        [iconView setImage:icon];
        [button addSubview:iconView];
        [iconView setCenter:CGPointMake(iconView.center.x, CGRectGetHeight(button.frame) / 2)];
    }
    
    return button;
}

#pragma mark - Showing/Hiding

- (void)show
{
    [self updateUserInformation];
    
    // Make it weak to avoid reference cycle / memory leaks
    __weak UIView *view = [(AppDelegate*)[[UIApplication sharedApplication] delegate] rootViewController].view;
    [[[[UIApplication sharedApplication] delegate] window] insertSubview:self belowSubview: view];
    [view.layer setShadowPath:[[UIBezierPath bezierPathWithRect:view.bounds] CGPath]];
    
    // Make sure it has a drop shadow
    if(view.layer.shadowOpacity == 0)
    {
        view.layer.shadowColor = [[UIColor blackColor] CGColor];
        view.layer.shadowOffset = CGSizeMake(-2, 0);
        view.layer.shadowRadius = 1;
        view.layer.shadowOpacity = 0.6;
    }
    
    // Add a button that will cover the entire view
    _closeButton.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [view addSubview:_closeButton];
    
    
    view.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:0.3 delay:0 options:(UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut) animations:^
    {
        [view setFrame:CGRectMake(self.frame.size.width - TOP_VIEW_UNCOVERED, 0, view.frame.size.width, view.frame.size.height)];
        
    } completion:^(BOOL finished)
    {
        view.userInteractionEnabled = YES;
        _isShowing = YES;
        
    }];
}

- (void)hide
{
    // Make it weak to avoid reference cycle / memory leaks
    __weak UIView *view = [(AppDelegate*)[[UIApplication sharedApplication] delegate] rootViewController].view;
    [view.layer setShadowPath:[[UIBezierPath bezierPathWithRect:view.bounds] CGPath]];
    
    view.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:0.3 delay:0 options:(UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut) animations:^
    {
        [view setFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    } completion:^(BOOL finished)
    {
        view.userInteractionEnabled = YES;
        _isShowing = NO;
    }];
}

- (void)toggle
{
    if(_isShowing)
        [self hide];
    else
        [self show];
}

- (void)updateUserInformation
{
    [_nameLabel setText:[NSString stringWithFormat:@"%@ %@", [[[AFCListing sharedStore] user] firstName], [[[AFCListing sharedStore] user] lastName]]];
    [_addressLabel setText:[[(AFCAddress*)[[[[AFCListing sharedStore] user] addresses] objectAtIndex:0] text] uppercaseString]];
}

#pragma mark  - Selectors

- (void)closeButtonPressed
{
    // Remove the close button from the top view
    [_closeButton removeFromSuperview];
    
    // Close the drawer
    [self toggle];
}

- (void)restaurantPressed
{
    [[self navigationController] popToRootViewControllerAnimated:NO];
    [[self navigationController] pushViewController:[[RestaurantsViewController alloc] init] animated:NO];
    [self closeButtonPressed];
}

- (void)trackingPressed
{
//    [[self navigationController] popToRootViewControllerAnimated:NO];
//    [[self navigationController] pushViewController:[[TrackingViewController alloc] init] animated:NO];
//    [self closeButtonPressed];
    [[self navigationController] popToRootViewControllerAnimated:NO];
    [[self navigationController] pushViewController:[[OrderStatusViewController alloc] init] animated:NO];
    [self closeButtonPressed];
}

- (void)profilePressed
{
    [[self navigationController] popToRootViewControllerAnimated:NO];
    [[self navigationController] pushViewController:[[ProfileViewController alloc] init] animated:NO];
    [self closeButtonPressed];
}

- (void)signoutPressed
{
    [AFCRepository logoutWithResponseHandler:^(NSString *error)
    {
        // clear cookies
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        for(NSHTTPCookie *cookie in cookies)
        {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:[[LogOnViewController alloc] init]];
        [navController setNavigationBarHidden:YES];
        [[self navigationController] presentViewController:navController animated:YES completion:^{
            [[self navigationController] popToRootViewControllerAnimated:NO];
            [[self navigationController] pushViewController:[[RestaurantsViewController alloc] init] animated:NO];
            [self closeButtonPressed];
        }];
    }];
}

- (AFCNavigationController*) navigationController
{
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate] rootViewController];
}

@end
