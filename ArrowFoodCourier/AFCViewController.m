 

#import "AFCViewController.h"
#import "WYPopoverController.h"
#import "CartViewController.h"
#import "CheckOutViewController.h"
#import "AFCListing.h"

@interface AFCViewController ()

@end

@implementation AFCViewController
{
    WYPopoverController *popoverController;
    UIBarButtonItem *_cartButton;
    UIView *cartItemsBadgeView;
    UILabel *cartItemsLabel;
    
    BOOL _initialized;
    UIView *_loadingView;
    UIActivityIndicatorView *_loading;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        _initialized = NO;
    }
    return self;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        _initialized = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}

-(void)viewWillAppear:(BOOL)animated
{
    if(!_initialized)
    {
        [self setupView];
        _initialized = YES;
    }
    [self updateCartBadge];
}

- (void)setupView{}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    NSLog(@"%@ dealloc.", [self class]);
}

- (AFCNavigationController*) mainNavigationController
{
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate] rootViewController];
}

-(void)addHomeIcon
{
    
    UIImage *image2 = [UIImage imageNamed:@"drawer"];
    UIBarButtonItem *bar2 = [[UIBarButtonItem alloc] initWithImage:image2 style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonPressed)];
    bar2.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = bar2;
}

-(void)menuButtonPressed
{
    // Make sure the keyboard is hidden
    [self.view endEditing:YES];
    
    [[LeftDrawer sharedInstance] toggle];
}

- (void)addCartButtonIcon
{
    
    UIImage *image = [UIImage imageNamed:@"cart"];
    UIButton *cartButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [cartButton setBackgroundImage:image forState:UIControlStateNormal];
    [cartButton addTarget:self action:@selector(cartButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *backgroundBadge = [[UIView alloc] initWithFrame:CGRectMake(cartButton.frame.size.width - 8, - 8, 16, 16)];
    [backgroundBadge setBackgroundColor:[UIColor colorFromHexValue:DarkRed]];
    backgroundBadge.layer.cornerRadius = backgroundBadge.frame.size.width/2;
    backgroundBadge.frame = CGRectIntegral(backgroundBadge.frame);
    backgroundBadge.userInteractionEnabled = NO;
    backgroundBadge.exclusiveTouch = NO;
    
    // Create cart button badge
    CGRect badgeFrame = CGRectMake(cartButton.frame.size.width - 7, -7, 14, 14);
    cartItemsBadgeView = [[UIView alloc] initWithFrame:badgeFrame];
    [cartItemsBadgeView setBackgroundColor:[UIColor whiteColor]];
    cartItemsBadgeView.layer.cornerRadius = badgeFrame.size.width/2;
    [cartItemsBadgeView setFrame:CGRectIntegral(cartItemsBadgeView.frame)];
    cartItemsBadgeView.userInteractionEnabled = NO;
    cartItemsBadgeView.exclusiveTouch = NO;
    
    cartItemsLabel = [[UILabel alloc] initWithFrame:badgeFrame];
    [cartItemsLabel setFont:[UIFont fontWithName:HEADER_FONT size:11]];
    [cartItemsLabel setTextAlignment:NSTextAlignmentCenter];
    [cartItemsLabel setTextColor:[UIColor colorFromHexValue:AFCRed]];
    [cartItemsLabel setBackgroundColor:[UIColor clearColor]];
    cartItemsLabel.userInteractionEnabled = NO;
    cartItemsLabel.exclusiveTouch = NO;
    [self updateCartBadge];
    
    [cartButton addSubview:backgroundBadge];
    [cartButton addSubview:cartItemsBadgeView];
    [cartButton addSubview:cartItemsLabel];
    
    _cartButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton];
    
    int rightPadding = 5;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
    {
        // Running in iOS6
        rightPadding = 15;
    }
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacer.width = rightPadding;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:spacer, _cartButton, nil] animated:NO];

}

- (void)cartButtonPressed
{
    if(![[AFCListing sharedStore] cart])
    {
        [AFCRepository getCartWithResponseHandler:^(NSString *error, NSDictionary *info)
         {
             if(!error)
             {
                 [[AFCListing sharedStore] createCartItemsFromDictionary:info];
                 [self updateCartBadge];
                 
                 CartViewController *cvc = [[CartViewController alloc] init];
                 popoverController = [[WYPopoverController alloc] initWithContentViewController:cvc];
                 popoverController.delegate = self;
                 [popoverController presentPopoverFromBarButtonItem:_cartButton
                                           permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES
                                                            options:WYPopoverAnimationOptionScale];
             }
             else
             {
                 UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Cart Error" message:error delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                 [av show];
             }
         }];
    }
    else
    {
        CartViewController *cvc = [[CartViewController alloc] init];
        popoverController = [[WYPopoverController alloc] initWithContentViewController:cvc];
        popoverController.delegate = self;
        [popoverController presentPopoverFromBarButtonItem:_cartButton
                                  permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES
                                                   options:WYPopoverAnimationOptionScale];
    }
}

- (void)updateCartBadge
{
    [cartItemsLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[[AFCListing sharedStore] allCartItems] count]]];
}

- (void)presentCartNotification
{
    [self updateCartBadge];
    
    // Create cart button badge
    CGRect notificationFrameStart = CGRectMake(self.view.frame.size.width - 40, 27, 24, 24);
    CGRect notificationFrameEnd = CGRectMake(notificationFrameStart.origin.x, notificationFrameStart.origin.y + 30, notificationFrameStart.size.width, notificationFrameStart.size.height);
    UIView *notificationView = [[UIView alloc] initWithFrame:notificationFrameStart];
    [notificationView setBackgroundColor:[UIColor whiteColor]];
    [notificationView layer].cornerRadius = roundf(notificationFrameStart.size.width/2);
    [notificationView setFrame:CGRectIntegral(notificationView.frame)];
    
    UILabel *notificationLabel = [[UILabel alloc] initWithFrame:notificationFrameStart];
    [notificationLabel setFont:[UIFont fontWithName:HEADER_FONT size:25]];
    [notificationLabel setTextAlignment:NSTextAlignmentCenter];
    [notificationLabel setTextColor:[UIColor colorFromHexValue:AFCRed]];
    [notificationLabel setBackgroundColor:[UIColor clearColor]];
    [notificationLabel setText:@"+"];
    
    [[[self mainNavigationController] view] addSubview:notificationView];
    [[[self mainNavigationController] view] addSubview:notificationLabel];
    
    [UIView animateWithDuration:1 animations:^{
        notificationView.alpha = 0;
        notificationView.frame = notificationFrameEnd;
        notificationLabel.alpha = 0;
        notificationLabel.frame = notificationFrameEnd;
    } completion:^(BOOL finished){
        [notificationView removeFromSuperview];
        [notificationLabel removeFromSuperview];
    }];
}

-(void)popoverControllerDidPresentPopover:(WYPopoverController *)popoverController
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkoutButtonPressed) name:@"CheckoutButtonPressed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cartItemDeleted) name:@"CartItemDeleted" object:nil];
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    popoverController.delegate = nil;
    popoverController = nil;
}

- (void)checkoutButtonPressed
{
    // Dismiss popover and unless it's already in the checkout view, pop to root view controller
    // and push the checkout view.
    [popoverController dismissPopoverAnimated:YES
    completion:^
    {
        if (![self isKindOfClass:[CheckOutViewController class]])
        {
            CheckOutViewController *covc = [[CheckOutViewController alloc] init];
            [[self mainNavigationController] moveToHomeController:covc];
        }
    }];
}

- (void)cartItemDeleted
{
    [self updateCartBadge];
}

-(void)addBackButtonIcon
{
    UIImage *image = [UIImage imageNamed:@"BackButton"];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    bar.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = bar;
}

-(void)backButtonPressed
{
    [[self mainNavigationController] popViewControllerAnimated:YES];
}

-(int)heightVisibleOnScreen
{
    return CGRectGetHeight(self.view.frame) - CGRectGetHeight([self mainNavigationController].navigationBar.frame)
                                            - CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
}

-(void)startLoading
{
    if(!_loading)
    {
        _loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
        _loadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [_loading setCenter:_loadingView.center];
        
        [_loadingView addSubview:_loading];
    }
    _loadingView.alpha = 0;
    [_loading startAnimating];
    [self.view addSubview:_loadingView];
    [self.view bringSubviewToFront:_loadingView];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        _loadingView.alpha = 1;
    } completion:nil];
}

-(void)stopLoading
{
    if(!_loading) return;
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        _loadingView.alpha = 0;
    } completion:^(BOOL finished) {
        [_loading stopAnimating];
        [_loadingView removeFromSuperview];
    }];
    
}

@end
