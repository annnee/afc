

#import <Foundation/Foundation.h>

@interface AFCAddress : NSObject

@property (nonatomic, strong) NSString *line1;
@property (nonatomic, strong) NSString *line2;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *zip;
@property (nonatomic) BOOL defaultBilling;
@property (nonatomic) BOOL defaultShipping;

- (id)initWithLine1:(NSString*)line1
                line2:(NSString*)line2
                 city:(NSString*)city
                state:(NSString*)state
                  zip:(NSString*)zip
       defaultBilling:(BOOL)defaultBilling
      defaultShipping:(BOOL)defaultShipping;

- (id)initFromDictionary:(NSDictionary*)dictionary;

- (NSDictionary*)dictionary;

- (NSString*)text;


@end
