

#import "ImageUtil.h"

@implementation ImageUtil

+ (UIImageView*)viewWithImageName:(NSString*)name andPos:(CGPoint)position
{
    UIImage* image = [UIImage imageNamed:name];
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(position.x, position.y, image.size.width, image.size.height)];
    [imageView setImage:image];
    return imageView;
}

+(UIImageView *)viewWithImageName:(NSString *)name andCenter:(CGPoint)position
{
    UIImage* image = [UIImage imageNamed:name];
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [imageView setCenter:position];
    // Make sure image values are whole pixels so that is does not appear blurry
    [imageView setFrame:CGRectIntegral(imageView.frame)];
    [imageView setImage:image];
    return imageView;
}

@end
