 
#import <UIKit/UIKit.h>
#import "AFCViewController.h"

@interface CreateAccountViewController : AFCViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@end
