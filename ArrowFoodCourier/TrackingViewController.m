 

#import "TrackingViewController.h"
#import "TrackingView.h"
#import "TrackingStatusBar.h"

@interface TrackingViewController ()
{
    AFCOrder *_order;
}

@end

@implementation TrackingViewController
{
    MKMapView *_mapView;
    CLLocationManager *_locationManager;
    UILabel *_etaTime;
    TrackingStatusBar *_trackingStatusBar;
}

static int statusDetailsHeight = 150;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (id)initWithOrder:(AFCOrder *)order
{
    self = [super init];
    if (self)
    {
        _order = order;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupView
{
    // Set the view to HomeView
    self.view = [[TrackingView alloc] initWithFrame:self.view.frame];
    
    // Top bar
    [self addBackButtonIcon];
    [self addCartButtonIcon];
    self.navigationItem.title = @"TRACKING";
    
    [self createMapView];
    [self createStatusArea];
    
}

- (void)createMapView
{
    _mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - statusDetailsHeight)];
    [self.view addSubview:_mapView];
    _mapView.delegate = self;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager setDistanceFilter:kCLDistanceFilterNone];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    [_mapView setShowsUserLocation:YES];
}

-(void) createStatusArea
{
    UIView *divider = [[UIView alloc] initWithFrame:CGRectMake(0, _mapView.frame.size.height,
                                                               self.view.frame.size.width, 2)];
    divider.backgroundColor = [UIColor colorFromHexValue:AFCGray];
    [self.view addSubview:divider];
    
    // Status
    _trackingStatusBar = [[TrackingStatusBar alloc] initWithPosition:CGPointMake(15, divider.frame.size.height+divider.frame.origin.y+15)
                                                            andWidth:self.view.frame.size.width - 35
                                                   andCircleDiameter:30
                                                         andStatuses:[NSArray arrayWithObjects:@"Sent", @"Received", @"Cooking", @"En Route", @"Delivered", nil]];
    [self.view addSubview:_trackingStatusBar];
    [_trackingStatusBar setStatus:[_order orderStatusString]];
    
    // Cover up the map if tracking is not yet available
    if ([_order orderStatus] != EN_ROUTE)
    {
        UIView *coverUpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - statusDetailsHeight)];
        [coverUpView setBackgroundColor:[UIColor whiteColor]];
        [coverUpView setAlpha:0.9];
        
        UILabel *coverUpLabel = [[UILabel alloc] initWithFrame:coverUpView.frame];
        [coverUpLabel setFont:[UIFont fontWithName:HEADER_FONT size:18]];
        [coverUpLabel setTextAlignment:NSTextAlignmentCenter];
        if ([_order orderStatus] < EN_ROUTE)
        {
            coverUpLabel.numberOfLines = 2;
            [coverUpLabel setText:@"Tracking not yet available!\nCheck back when your order is en route."];
        }
        else if ([_order orderStatus] > EN_ROUTE)
        {
            [coverUpLabel setText:@"Order has been completed!"];
        }
        
        [[self view] addSubview:coverUpView];
        [[self view] addSubview:coverUpLabel];
    }
    
    UILabel *eta = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 40, (self.view.frame.size.width / 2) - 12, 40)];
    [eta setText:@"ETA:"];
    eta.font = [UIFont fontWithName:HEADER_FONT size:15];
    eta.textColor = [UIColor blackColor];
    eta.textAlignment = NSTextAlignmentRight;
    eta.backgroundColor = [UIColor clearColor];
    [self.view addSubview:eta];
    
    _etaTime = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2, self.view.frame.size.height - 40, (self.view.frame.size.width / 2) - 12, 40)];
    _etaTime.text = @"20 mins";
    _etaTime.font = [UIFont fontWithName:HEADER_FONT size:15];
    _etaTime.textColor = [UIColor colorFromHexValue:AFCRed];
    _etaTime.textAlignment = NSTextAlignmentLeft;
    _etaTime.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_etaTime];
}

#pragma mark Delegates

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([userLocation coordinate] ,250,250);
    [mapView setRegion:region animated:YES];
}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    
}

@end
