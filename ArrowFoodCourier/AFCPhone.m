

#import "AFCPhone.h"

@implementation AFCPhone

@synthesize type = _type;
@synthesize number = _number;
@synthesize defaultNumber = _defaultNumber;

-(id)initWithType:(NSString *)type number:(NSString *)number defaultNumber:(BOOL)defaultNumber
{
    self = [super init];
    if(self)
    {
        _type = type;
        _number = number;
        _defaultNumber = defaultNumber;
    }
    return self;
}

-(id)initFromDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        _type = [dictionary objectForKey:@"name"];
        _number = [dictionary objectForKey:@"number"];
        _defaultNumber = [[dictionary objectForKey:@"default"] boolValue];
    }
    return self;
}

-(NSDictionary *)dictionary
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
    if(_type) [d setObject:_type forKey:@"name"];
    if(_number) [d setObject:_number forKey:@"number"];
    [d setObject:[NSNumber numberWithBool:_defaultNumber] forKey:@"default"];
    
    return d;
}

@end
