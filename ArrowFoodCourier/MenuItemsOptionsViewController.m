 
#import "MenuItemsOptionsViewController.h"
#import "MenuItemOptionsViewFooter.h"
#import "AFCListing.h"
#import "MenuItemOptionsViewCell.h"

@interface MenuItemsOptionsViewController ()

@end

@implementation MenuItemsOptionsViewController
{
    UITableView *_optionsTableView;
    NSArray *_tableData;
}

- (id)initWithMenuItem:(AFCMenuItem *)item
{
    self = [super init];
    if (self)
    {
        menuItem = item;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self view] setBackgroundColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupView
{
    [self createTableView];
    
    // Define the navigation title for the AFCNavigationController so that it will
    // be displayed in the top bar.
    self.navigationItem.title = menuItem.name.uppercaseString;
    
    // Add icon items to the top bar
    [self addCartButtonIcon];
    [self addBackButtonIcon];
}

- (void)createTableView
{
    _tableData = [menuItem allOptions];
    _optionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    _optionsTableView.delegate = self;
    _optionsTableView.dataSource = self;
    _optionsTableView.autoresizesSubviews = YES;
    [_optionsTableView setBackgroundColor:[UIColor colorFromHexValue:0xF0F0F0]];
    
    [self.view addSubview:_optionsTableView];
    
    // Push the table view to be below the secondary nav bar
    //[self.view sendSubviewToBack:_cartTableView];
    
    // Remove the extra whitespace at the left of the tableview
    if ([_optionsTableView respondsToSelector:@selector(setSeparatorInset:)])  // Safety check for below iOS 7
    {
        [_optionsTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    MenuItemOptionsViewFooter *miovf = [[MenuItemOptionsViewFooter alloc] initWithFrame:(CGRectMake(0, 0, self.view.frame.size.width, OptionsFooterHeight))];
    [miovf addTargetToButton:self action:@selector(addToCartButtonPressed)];
    _optionsTableView.tableFooterView = miovf;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"menuTableMenuItemOption";
    
    MenuItemOptionsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[MenuItemOptionsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.cellMenuItemOption = [_tableData objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AFCMenuItemOption *option = (AFCMenuItemOption *)[_tableData objectAtIndex:indexPath.row];
    if (option.type == CHECK_ONE)
    {
        return OPTION_CELL_FOOTER_HEIGHT + [[option allParameters] count]*DEFAULT_TABLE_CELL_HEIGHT;
    }
    else
    if(option.type == CHECKBOX)
    {
        return OPTION_CELL_FOOTER_HEIGHT;
    }
    else
    {
        return 1;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)addToCartButtonPressed
{
    [self startLoading];
    [AFCRepository addItemToCartWithRestaurant:[AFCListing sharedStore].selectedRestaurant.name
                                  menuCategory:[AFCListing sharedStore].selectedCategory.name
                                          item:menuItem.name
                                       options:[menuItem allOptionsDictionary]
                                      quantity:1 responseHandler:^(NSString *error)
     {
         if(!error)
         {
             AFCCartItem *item = [[AFCCartItem alloc] initWithMenuItem:menuItem category:[AFCListing sharedStore].selectedCategory.name andRestaurant:[AFCListing sharedStore].selectedRestaurant.name];
             [[AFCListing sharedStore] addCartItem:item];
             [self presentCartNotification];
         }
         else
         {
             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Cart Error"
                                                          message:error
                                                         delegate:nil
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles: nil];
             [av show];
         }
         [self stopLoading];
     }];
    
}
@end
