 
#import "AFCViewController.h"
#import "WYPopoverController.h"

@interface CartViewController : AFCViewController<UITableViewDataSource, UITableViewDelegate, WYPopoverControllerDelegate>

- (id)initWithFrame:(CGRect)frame;

@end
