

#import <UIKit/UIKit.h>
#import "AFCNavigationController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AFCNavigationController *rootViewController;

@property (nonatomic) BOOL driverMode;

- (void)setDrawerGesturesEnabled:(BOOL)enabled;

@end
