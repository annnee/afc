

#import <Foundation/Foundation.h>

@interface AFCPhone : NSObject

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *number;
@property (nonatomic) BOOL defaultNumber;

- (id)initWithType:(NSString*)type number:(NSString*)number defaultNumber:(BOOL)defaultNumber;

- (id)initFromDictionary:(NSDictionary*)dictionary;

- (NSDictionary*)dictionary;

@end
