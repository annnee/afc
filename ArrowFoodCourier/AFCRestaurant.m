

#import "AFCRestaurant.h"
#import "AFCAddress.h"
#import "AFCPhone.h"

@implementation AFCRestaurant
{
    NSMutableArray *addresses;
    NSMutableArray *phoneNumbers;
}

@synthesize name = _name;
@synthesize address = _address;
@synthesize phoneNumber = _phoneNumber;
@synthesize shortDescription = _shortDescription;
@synthesize detailedDescription = _detailedDescription;
@synthesize storefrontImage = _storefrontImage;
@synthesize rating = _rating;

- (id)initWithName:(NSString *)restaurantName
           address:(NSString *)restaurantAddress
       phoneNumber:(NSString *)restaurantPhoneNumber
  shortDescription:(NSString *)restaurantShortDescription
detailedDescription:(NSString *)restaurantDetailedDescription
   storefrontImage:(UIImage *)restaurantStorefrontImage
            rating:(float)restaurantRating
{
    self = [super init];
    if (self)
    {
        menuCategories = [[NSMutableArray alloc] init];
        
        _name = restaurantName;
        _address = restaurantAddress;
        _phoneNumber = restaurantPhoneNumber;
        _shortDescription = restaurantShortDescription;
        _detailedDescription = restaurantDetailedDescription;
        _storefrontImage = restaurantStorefrontImage;
        _rating = restaurantRating;
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        menuCategories = [[NSMutableArray alloc] init];
        addresses = [[NSMutableArray alloc] init];
        phoneNumbers = [[NSMutableArray alloc] init];
        
        // Parse addresses
        NSArray *tempAddresses = [dictionary objectForKey:@"addresses"];
        for (NSDictionary *address in tempAddresses) {
            [addresses addObject:[[AFCAddress alloc] initFromDictionary:address]];
        }
        
        // Parse phone numbers
        NSArray *tempPhoneNumbers = [dictionary objectForKey:@"phones"];
        for (NSDictionary *phone in tempPhoneNumbers) {
            [phoneNumbers addObject:[[AFCPhone alloc] initFromDictionary:phone]];
        }
        
        // To do: add emails
        
        // Set restaurant info
        _name = (NSString *)[dictionary objectForKey:@"name"];
        _detailedDescription = [dictionary objectForKey:@"description"];
        
        // For now just pick the first address and phone number
        _address = [[addresses objectAtIndex:0] line1];
        _phoneNumber = [[phoneNumbers objectAtIndex:0] number];
        _shortDescription = [(NSArray *)[dictionary objectForKey:@"tags"] componentsJoinedByString:@", "];
        _storefrontImage = [UIImage imageNamed:@"Diner.jpg"];
        _rating = 0;
    }
    return self;
}

- (void)addMenuCategory:(AFCMenuCategory *)category
{
    [menuCategories addObject:category];
}

- (NSArray *)allMenuCategories
{
    return menuCategories;
}

@end
