 

#import "LogOnViewController.h"
#import "LogOnView.h"
#import "CreateAccountViewController.h"
#import "RestaurantsViewController.h"
#import "DriverModeViewController.h"

@interface LogOnViewController ()

@end

@implementation LogOnViewController
{
    UIButton *signIn;
    UIButton *createAccount;
    UIButton *forgotPassword;
    UIActivityIndicatorView *_loadingIndicator;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Load the restaurants anyways, and just push over it if its a driver
    // First controller the user should see
    RestaurantsViewController *controller = [[RestaurantsViewController alloc] initWithNibName:nil bundle:nil];
    [[self mainNavigationController] pushViewController:controller animated:NO];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupView];
}

- (void)setupView
{
    // Set the view to LogOn View
    self.view = [[LogOnView alloc] initWithFrame:self.view.frame];
    
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] setDrawerGesturesEnabled:NO];
    
    signIn = [UIButton buttonWithType:UIButtonTypeCustom];
    CALayer *btnLayer = [signIn layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
    [signIn setTitle:@"Sign In" forState:UIControlStateNormal];
    signIn.frame = CGRectMake(self.view.frame.size.width/2-75, 250, 150, 30);
    signIn.backgroundColor = [UIColor whiteColor];
    [signIn addTarget:self action:@selector(signingIn:) forControlEvents:UIControlEventTouchUpInside];
    [[signIn titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
    [signIn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[(LogOnView*)self.view setUp] addSubview:signIn];
    
    createAccount = [UIButton buttonWithType:UIButtonTypeCustom];
    CALayer *cabtnlayer = [createAccount layer];
    [cabtnlayer setMasksToBounds:YES];
    [cabtnlayer setCornerRadius:5.0f];
    [createAccount setTitle:@"Create Account" forState:UIControlStateNormal];
    createAccount.frame = CGRectMake(self.view.frame.size.width/2-75, 310, 150, 30);
    createAccount.backgroundColor = [UIColor whiteColor];
    [createAccount addTarget:self action:@selector(creatingAccount:) forControlEvents:UIControlEventTouchUpInside];
    [[createAccount titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
    [createAccount setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[(LogOnView*)self.view setUp] addSubview:createAccount];
    
    forgotPassword = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgotPassword setTitle:@"Forgot Password?" forState:UIControlStateNormal];
    [forgotPassword addTarget:self action:@selector(forgotMyPassword:) forControlEvents:UIControlEventTouchUpInside];
    forgotPassword.frame = CGRectMake(self.view.frame.size.width/2-75, 280, 150, 30);
    [[forgotPassword titleLabel] setFont:[UIFont fontWithName:HEADER_FONT size:15.0f]];
    [forgotPassword setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[(LogOnView*)self.view setUp] addSubview:forgotPassword];
    
    _loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_loadingIndicator setCenter:CGPointMake(self.view.center.x , CGRectGetMaxY(createAccount.frame) + 40)];
    [_loadingIndicator setHidesWhenStopped:YES];
    [[(LogOnView*)self.view setUp] addSubview:_loadingIndicator];
    
}

- (void)forgotMyPassword:(UIButton*)button
{
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Forgot Password" message:@"Please enter your username, and we'll send you an email to reset your password."
                                               delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send", nil];
    av.alertViewStyle = UIAlertViewStylePlainTextInput;
    av.tag = 100;
    av.delegate = self;
    [av show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex == 1)
    {
        UITextField *textField = (UITextField*)[alertView textFieldAtIndex:0];
        [AFCRepository sendResetPasswordEmailWithUsername:textField.text responseHandler:^(NSString *error)
         {
             if(!error)
             {
                 UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"New Password" message:@"Within the next few minutes you will receive an email containing your reset token. Enter your token along with your new password below. You may have to check your spam folder."
                                                            delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Reset", nil];
                 [av setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
                 [[av textFieldAtIndex:0] setPlaceholder:@"Reset Token"];
                 [[av textFieldAtIndex:1] setPlaceholder:@"New Password"];
                 av.tag = 200;
                 [av show];
             }
             else
             {
                 UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Reset password error"
                                                                  message:error
                                                                 delegate:self
                                                        cancelButtonTitle:@"Ok"
                                                        otherButtonTitles: nil];
                 [alert show];
             }
         }];
    }
    else if(alertView.tag == 200 && buttonIndex == 1)
    {
        UITextField *textField1 = (UITextField*)[alertView textFieldAtIndex:0];
        UITextField *textField2 = (UITextField*)[alertView textFieldAtIndex:1];
        
        [AFCRepository resetPasswordWithToken:textField1.text newPassword:textField2.text responseHandler:^(NSString *error)
        {
            if(!error)
            {
                UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Password successfully reset!"
                                                                 message:nil
                                                                delegate:self
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"New Password" message:@"Oops, there was an error trying to reset your password. Re-enter your token along with your new password below to try again."
                                                           delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Reset", nil];
                [av setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
                [[av textFieldAtIndex:0] setPlaceholder:@"Reset Token"];
                [[av textFieldAtIndex:1] setPlaceholder:@"New Password"];
                av.tag = 200;
                [av show];
            }
        }];
    }
}

- (void)signingIn:(UIButton*)button
{
    [self.view endEditing:YES];
    [_loadingIndicator startAnimating];
    createAccount.enabled = NO;
    signIn.enabled = NO;
    
    NSLog(@"Signing in.");
    LogOnView *view = (LogOnView*)self.view;
    [AFCRepository loginWithUsername:view.username.text password:view.password.text responseHandler:^(NSString *error)
    {
        if(!error)
        {
            [AFCRepository getProfileWithResponseHandler:^(NSString *error, NSDictionary *info)
            {
                if(!error)
                {
                    [AFCListing sharedStore].user = [[AFCUser alloc] initFromDictionary:info];
                    
                    [AFCRepository getCartWithResponseHandler:^(NSString *error, NSDictionary *info)
                     {
                         if(!error)
                         {
                             [[AFCListing sharedStore] createCartItemsFromDictionary:info];
                             [self updateCartBadge];
                         }
                         
                         [_loadingIndicator stopAnimating];
                         
                         // Push driver view
                         if([[[AFCListing sharedStore] user] isDriver])
                         {
                             DriverModeViewController *controller = [[DriverModeViewController alloc] initWithNibName:nil bundle:nil];
                             [[self mainNavigationController] pushViewController:controller animated:NO];
                         }
                         
                         [[self mainNavigationController] dismissViewControllerAnimated:YES completion:nil];
                     }];
                }
                else
                {
                    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Login error"
                                                                     message:error
                                                                    delegate:self
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles: nil];
                    [alert show];
                    createAccount.enabled = YES;
                    signIn.enabled = YES;
                    [_loadingIndicator stopAnimating];
                }
            }];
        }
        else
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Login error"
                                                             message:error
                                                            delegate:self
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil];
            [alert show];
            createAccount.enabled = YES;
            signIn.enabled = YES;
            [_loadingIndicator stopAnimating];
        }
    }];
}

- (void)creatingAccount:(UIButton*)button
{
    [self.view endEditing:YES];
    
    [[self navigationController] pushViewController:[[CreateAccountViewController alloc] init] animated:YES];
    NSLog(@"Create Account");
}

@end
